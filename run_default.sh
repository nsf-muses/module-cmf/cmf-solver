#!/bin/bash

# This script runs the CMF++ project locally
# author: Nikolas Cruz-Camacho <cnc6@illinois.edu> 

# This script assumes that the 'build.sh' script has been run before it to build the necessary components and 
# that a user configuration YAML is provided as a command-line argument (config.yaml can be created with input/create_config.py)

# Set options for the script
set -euo pipefail

# Print a message indicating the start of the CMF++ run
echo "CMF++ run started"

# Determine the path to the Python executable (preferably python3)
PYTHON="$(command -v python3 2>/dev/null || echo python)"

# Default values
DEFAULT_API_FILE_PATH="./api/OpenAPI_Specifications_CMF.yaml"
DEFAULT_CONFIG_FILE_PATH="./input/config.yaml"
DEFAULT_VALIDATED_CONFIG_FILE_PATH="./input/validated_config.yaml"
DEFAULT_ROOT_OUTPUT_PATH="./output/"
DEFAULT_PDG_QUARK_TABLE_PATH="./src/PDG/PDG2021Plus_quarks.dat"
DEFAULT_PDG_TABLE_PATH="./src/PDG/PDG2021Plus_massorder.dat"

# Initialize variables with default values
API_FILE_PATH="$DEFAULT_API_FILE_PATH"
CONFIG_FILE_PATH="$DEFAULT_CONFIG_FILE_PATH"
VALIDATED_CONFIG_FILE_PATH="$DEFAULT_VALIDATED_CONFIG_FILE_PATH"
ROOT_OUTPUT_PATH="$DEFAULT_ROOT_OUTPUT_PATH"
PDG_QUARK_TABLE_PATH="$DEFAULT_PDG_QUARK_TABLE_PATH"
PDG_TABLE_PATH="$DEFAULT_PDG_TABLE_PATH"

# Display parsed values (for debugging purposes)
echo "API_FILE_PATH: $API_FILE_PATH"
echo "CONFIG_FILE_PATH: $CONFIG_FILE_PATH"
echo "VALIDATED_CONFIG_FILE_PATH: $VALIDATED_CONFIG_FILE_PATH"
echo "ROOT_OUTPUT_PATH: $ROOT_OUTPUT_PATH"
echo "PDG_QUARK_TABLE_PATH: $PDG_QUARK_TABLE_PATH"
echo "PDG_TABLE_PATH: $PDG_TABLE_PATH"

# Convert the user ths to an absolute path
API_FILE_PATH=$(realpath "$API_FILE_PATH")
CONFIG_FILE_PATH=$(realpath "$CONFIG_FILE_PATH")
VALIDATED_CONFIG_FILE_PATH=$(realpath "$VALIDATED_CONFIG_FILE_PATH")
ROOT_OUTPUT_PATH=$(realpath "$ROOT_OUTPUT_PATH")
PDG_QUARK_TABLE_PATH=$(realpath "$PDG_QUARK_TABLE_PATH")
PDG_TABLE_PATH=$(realpath "$PDG_TABLE_PATH")

# Check if the API file exists
if [ ! -f "$API_FILE_PATH" ]; then
  echo "API file not found: $API_FILE_PATH" >&2
  exit 1
fi

# Check if the PDG quark table file exists
if [ ! -f "$PDG_QUARK_TABLE_PATH" ]; then
  echo "PDG quark table file not found: $PDG_QUARK_TABLE_PATH" >&2
  exit 1
fi

# Check if the PDG table file exists
if [ ! -f "$PDG_TABLE_PATH" ]; then
  echo "PDG table file not found: $PDG_TABLE_PATH" >&2
  exit 1
fi

# Create the output directory if it doesn't exist
mkdir -p "$ROOT_OUTPUT_PATH"

# Change the current directory to 'src'
cd ./src

make run_default

# Check the exit status of the last command (CMF++)
if [ $? -eq 0 ]; then
  # Print a success message if the CMF++ run was successful
  echo -e "\n\tCMF++ run: OK\n"
else
  # Print a failure message if the CMF++ run failed and exit with status 1
  echo -e "\n\tCMF++ run: Failed\n"
  exit 1
fi

# Print a message indicating the completion of the CMF++ run
echo "CMF++ run done"