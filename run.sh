#!/bin/bash

# This script runs the CMF++ project locally
# author: Nikolas Cruz-Camacho <cnc6@illinois.edu> 

# This script assumes that the 'build.sh' script has been run before it to build the necessary components and 
# that a user configuration YAML is provided as a command-line argument (config.yaml can be created with input/create_config.py)

# Set options for the script
set -euo pipefail

# Print a message indicating the start of the CMF++ run
echo "CMF++ run started"

# Determine the path to the Python executable (preferably python3)
PYTHON="$(command -v python3 2>/dev/null || echo python)"

# Function to display help message
usage() {
  echo "Usage: $0 [options]"
  echo "Options:"
  echo "  -a API_FILE_PATH              Path to the API file (default: $DEFAULT_API_FILE_PATH)"
  echo "  -c CONFIG_FILE_PATH           Path to the config file (default: $DEFAULT_CONFIG_FILE_PATH)"
  echo "  -v VALIDATED_CONFIG_FILE_PATH Path to the validated config file (default: $DEFAULT_VALIDATED_CONFIG_FILE_PATH)"
  echo "  -r ROOT_OUTPUT_PATH           Path to the root output (default: $DEFAULT_ROOT_OUTPUT_PATH)"
  echo "  -q PDG_QUARK_TABLE_PATH       Path to the PDG quark table (default: $DEFAULT_PDG_QUARK_TABLE_PATH)"
  echo "  -t PDG_TABLE_PATH             Path to the PDG table (default: $DEFAULT_PDG_TABLE_PATH)"
  echo "  -h                            Display this help message"
  exit 1
}

# Default values
DEFAULT_API_FILE_PATH="./api/OpenAPI_Specifications_CMF.yaml"
DEFAULT_CONFIG_FILE_PATH="./input/config.yaml"
DEFAULT_VALIDATED_CONFIG_FILE_PATH="./input/validated_config.yaml"
DEFAULT_ROOT_OUTPUT_PATH="./output/"
DEFAULT_PDG_QUARK_TABLE_PATH="./src/PDG/PDG2021Plus_quarks.dat"
DEFAULT_PDG_TABLE_PATH="./src/PDG/PDG2021Plus_massorder.dat"

# Initialize variables with default values
API_FILE_PATH="$DEFAULT_API_FILE_PATH"
CONFIG_FILE_PATH="$DEFAULT_CONFIG_FILE_PATH"
VALIDATED_CONFIG_FILE_PATH="$DEFAULT_VALIDATED_CONFIG_FILE_PATH"
ROOT_OUTPUT_PATH="$DEFAULT_ROOT_OUTPUT_PATH"
PDG_QUARK_TABLE_PATH="$DEFAULT_PDG_QUARK_TABLE_PATH"
PDG_TABLE_PATH="$DEFAULT_PDG_TABLE_PATH"

# Parse options
while getopts ":a:c:v:r:q:t:h" opt; do
  case $opt in
    a) API_FILE_PATH="$OPTARG"
       ;;
    c) CONFIG_FILE_PATH="$OPTARG"
       ;;
    v) VALIDATED_CONFIG_FILE_PATH="$OPTARG"
       ;;
    r) ROOT_OUTPUT_PATH="$OPTARG"
       ;;
    q) PDG_QUARK_TABLE_PATH="$OPTARG"
       ;;
    t) PDG_TABLE_PATH="$OPTARG"
       ;;
    h) usage
       ;;
    \?) echo "Invalid option: -$OPTARG" >&2
        usage
        ;;
    :) echo "Option -$OPTARG requires an argument." >&2
       usage
       ;;
  esac
done

# Display parsed values (for debugging purposes)
echo "API_FILE_PATH: $API_FILE_PATH"
echo "CONFIG_FILE_PATH: $CONFIG_FILE_PATH"
echo "VALIDATED_CONFIG_FILE_PATH: $VALIDATED_CONFIG_FILE_PATH"
echo "ROOT_OUTPUT_PATH: $ROOT_OUTPUT_PATH"
echo "PDG_QUARK_TABLE_PATH: $PDG_QUARK_TABLE_PATH"
echo "PDG_TABLE_PATH: $PDG_TABLE_PATH"

# Convert the user ths to an absolute path
API_FILE_PATH=$(realpath "$API_FILE_PATH")
CONFIG_FILE_PATH=$(realpath "$CONFIG_FILE_PATH")
VALIDATED_CONFIG_FILE_PATH=$(realpath "$VALIDATED_CONFIG_FILE_PATH")
ROOT_OUTPUT_PATH=$(realpath "$ROOT_OUTPUT_PATH")
PDG_QUARK_TABLE_PATH=$(realpath "$PDG_QUARK_TABLE_PATH")
PDG_TABLE_PATH=$(realpath "$PDG_TABLE_PATH")

# Check if the API file exists
if [ ! -f "$API_FILE_PATH" ]; then
  echo "API file not found: $API_FILE_PATH" >&2
  exit 1
fi

# Check if the config file exists
if [ ! -f "$CONFIG_FILE_PATH" ]; then
  echo "Config file not found: $CONFIG_FILE_PATH" >&2
  exit 1
fi

# Check if the PDG quark table file exists
if [ ! -f "$PDG_QUARK_TABLE_PATH" ]; then
  echo "PDG quark table file not found: $PDG_QUARK_TABLE_PATH" >&2
  exit 1
fi

# Check if the PDG table file exists
if [ ! -f "$PDG_TABLE_PATH" ]; then
  echo "PDG table file not found: $PDG_TABLE_PATH" >&2
  exit 1
fi

# Check if the output folder exists; if not create it
if [ ! -d "$ROOT_OUTPUT_PATH" ]; then
    echo "Warning: output directory does not exist: $ROOT_OUTPUT_PATH"
    echo "Warning: creating output directory: $ROOT_OUTPUT_PATH"
    mkdir -p "$ROOT_OUTPUT_PATH"
fi

# Change the current directory to 'src'
cd src

# Preprocess the user configuration using a Python script
"$PYTHON" yaml_preprocess.py --api_file_path "$API_FILE_PATH" --config_file_path "$CONFIG_FILE_PATH" --validated_config_file_path "$VALIDATED_CONFIG_FILE_PATH"

# Run CMF++
./cmf "$VALIDATED_CONFIG_FILE_PATH" "$ROOT_OUTPUT_PATH" "$PDG_TABLE_PATH" "$PDG_QUARK_TABLE_PATH"

# Postprocess the output using Python scripts
"$PYTHON" clean_output.py --validated_config_file_path "$VALIDATED_CONFIG_FILE_PATH" --root_output_path "$ROOT_OUTPUT_PATH"
"$PYTHON" postprocess.py --validated_config_file_path "$VALIDATED_CONFIG_FILE_PATH" --root_output_path "$ROOT_OUTPUT_PATH"

# Check the exit status of the last command (CMF++)
if [ $? -eq 0 ]; then
  # Print a success message if the CMF++ run was successful
  echo -e "\n\tCMF++ run: OK\n"
else
  # Print a failure message if the CMF++ run failed and exit with status 1
  echo -e "\n\tCMF++ run: Failed\n"
  exit 1
fi

# Print a message indicating the completion of the CMF++ run
echo "CMF++ run done"

# Exit the script with status 0 (success)
exit 0
