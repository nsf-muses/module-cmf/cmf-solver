#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "PDG_particle.hpp"

// g++ doctest_particle.cpp ../particle.cpp -I ../ -o doctest_particle.exe;
// ./doctest_particle.exe

TEST_CASE("testing particle.h") {
  // Initilice test particle
  PDG_Particle P_test =
      PDG_Particle(10, "test_newname", 15., 20., 5, 3, 3, -3, -3, 5., 0, 0, 0);

  // check reassignments
  CHECK(P_test.ID == 10);
  CHECK(P_test.Name == "test_newname");
  CHECK(P_test.Mass == 15.);
  CHECK(P_test.Width == 20.);
  CHECK(P_test.Degeneracy == 5);
  CHECK(P_test.BaryonNumber == 3);
  CHECK(P_test.Strangeness == 3);
  CHECK(P_test.Charm == -3);
  CHECK(P_test.Bottomness == -3);
  CHECK(P_test.Isospin == 5.);
  CHECK(P_test.Isospin_Z == 0);
  CHECK(P_test.ElectricCharge == 0);
  CHECK(P_test.Decays == 0);
}
