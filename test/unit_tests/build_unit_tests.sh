#!/bin/bash

set -euo pipefail

# Function to compile a test
compile_test() {
    local test_name="$1"
    local source_files=("${@:2}")
    g++ "${source_files[@]}" -I ../../src/ -Wall -L/usr/local/lib/ -std=gnu++2a -o "${test_name}.exe" -lyaml-cpp
}

echo -e "Compiling unit tests\n"

# Define source file arrays
doctest_particle_sources=("doctest_particle.cpp" "../../src/utils.cpp")
doctest_PDG_reader_sources=("doctest_PDG_reader.cpp" "../../src/PDG_reader.cpp" "../../src/utils.cpp")
doctest_utils_sources=("doctest_utils.cpp" "../../src/utils.cpp")
doctest_solution_sources=("doctest_solution.cpp" "../../src/solution.cpp")

# Compile tests
compile_test "doctest_particle" "${doctest_particle_sources[@]}"
compile_test "doctest_PDG_reader" "${doctest_PDG_reader_sources[@]}"
compile_test "doctest_utils" "${doctest_utils_sources[@]}"
compile_test "doctest_solution" "${doctest_solution_sources[@]}"
