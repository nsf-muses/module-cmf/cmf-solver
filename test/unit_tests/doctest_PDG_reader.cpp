#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "PDG_reader.hpp"
#include "doctest/doctest.h"
#include "utils.hpp"
#include <cmath>

// g++ doctest_PDG_reader.cpp ../PDG_reader.cpp ../utils.cpp ../particle.cpp -I
// ../ -o doctest_PDG_reader.exe; ./doctest_PDG_reader.exe

TEST_CASE("testing PDG_reader") {

  /*
 1) create PDG_reader object with file PDG2021test.dat selecting only those
 inside desired_particles 3) assert if return is the expected one
 4) return status
*/
  string PDG_table = "./PDG2021test.dat";
  vector<size_t> desired_particles = {2112, 2212, 22};

  // Initilice AP = All Particles
  PDG_reader AP = PDG_reader(PDG_table, desired_particles);

  // check values
  CHECK(AP.read_particles[0].ID == 2112);
  CHECK(AP.read_particles[0].Name == "n");
  CHECK(fabs(AP.read_particles[0].Mass - 939.5654133) < 1e-6);
  CHECK(AP.read_particles[0].Width == 0);
  CHECK(AP.read_particles[0].Degeneracy == 2);
  CHECK(AP.read_particles[0].BaryonNumber == 1);
  CHECK(AP.read_particles[0].Strangeness == 0);
  CHECK(AP.read_particles[0].Charm == 0);
  CHECK(AP.read_particles[0].Bottomness == 0);
  CHECK(AP.read_particles[0].Isospin == 0.5);
  CHECK(AP.read_particles[0].Isospin_Z == -0.5);
  CHECK(AP.read_particles[0].ElectricCharge == 0);
  CHECK(AP.read_particles[0].Decays == 1);

  CHECK(AP.read_particles[1].ID == 2212);
  CHECK(AP.read_particles[1].Name == "p");
  CHECK(fabs(AP.read_particles[1].Mass - 938.2720813) < 1e-6);
  CHECK(AP.read_particles[1].Width == 0);
  CHECK(AP.read_particles[1].Degeneracy == 2);
  CHECK(AP.read_particles[1].BaryonNumber == 1);
  CHECK(AP.read_particles[1].Strangeness == 0);
  CHECK(AP.read_particles[1].Charm == 0);
  CHECK(AP.read_particles[1].Bottomness == 0);
  CHECK(AP.read_particles[1].Isospin == 0.5);
  CHECK(AP.read_particles[1].Isospin_Z == 0.5);
  CHECK(AP.read_particles[1].ElectricCharge == 1);
  CHECK(AP.read_particles[1].Decays == 1);
}