#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "solution.hpp"

TEST_CASE("solution class tests") {
    solution sol1;
    solution sol2;

    // Test the update_mean_fields method
    sol1.update_mean_fields(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0);
    CHECK(sol1.mean_fields.sigma == 1.0);
    CHECK(sol1.mean_fields.zeta == 2.0);
    CHECK(sol1.mean_fields.delta == 3.0);
    CHECK(sol1.mean_fields.omega == 4.0);
    CHECK(sol1.mean_fields.phi == 5.0);
    CHECK(sol1.mean_fields.rho == 6.0);
    CHECK(sol1.mean_fields.Phi_order == 7.0);

    // Test the update_residues method
    sol1.update_residues(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7);
    CHECK(sol1.residues.sigma == 0.1);
    CHECK(sol1.residues.zeta == 0.2);
    CHECK(sol1.residues.delta == 0.3);
    CHECK(sol1.residues.omega == 0.4);
    CHECK(sol1.residues.phi == 0.5);
    CHECK(sol1.residues.rho == 0.6);
    CHECK(sol1.residues.Phi_order == 0.7);

    // Test the update_chemical_potentials method
    sol1.update_chemical_potentials(10.0, 20.0, 30.0);
    CHECK(sol1.chemical_potentials.Baryon == 10.0);
    CHECK(sol1.chemical_potentials.Strange == 20.0);
    CHECK(sol1.chemical_potentials.Charge == 30.0);

    // Test the copy constructor
    sol2 = sol1;  // Test the assignment operator
    CHECK(sol2.mean_fields.sigma == 1.0);
    CHECK(sol2.mean_fields.zeta == 2.0);
    CHECK(sol2.mean_fields.delta == 3.0);
    CHECK(sol2.mean_fields.omega == 4.0);
    CHECK(sol2.mean_fields.phi == 5.0);
    CHECK(sol2.mean_fields.rho == 6.0);
    CHECK(sol2.mean_fields.Phi_order == 7.0);
    CHECK(sol2.residues.sigma == 0.1);
    CHECK(sol2.residues.zeta == 0.2);
    CHECK(sol2.residues.delta == 0.3);
    CHECK(sol2.residues.omega == 0.4);
    CHECK(sol2.residues.phi == 0.5);
    CHECK(sol2.residues.rho == 0.6);
    CHECK(sol2.residues.Phi_order == 0.7);
    CHECK(sol2.chemical_potentials.Baryon == 10.0);
    CHECK(sol2.chemical_potentials.Strange == 20.0);
    CHECK(sol2.chemical_potentials.Charge == 30.0);

    // Test the calculate_mean_squared_difference method
    double squaredDiff = sol1.calculate_mean_squared_difference(sol2);
    CHECK(squaredDiff == 0.0); // Should be zero when compared to an identical solution

    // Print the solution
    std::ostringstream oss;
    sol1.print(oss);
    CHECK(oss.str() == "10\t20\t30\t1\t2\t3\t4\t5\t6\t7\n");
}
