#!/bin/bash

set -euo pipefail

# Function to run a compiled test
run_test() {
    local test_name="$1"
    echo -e "\nRunning $test_name"
    "./${test_name}.exe" || (( RESULT += 1 ))  # Increment RESULT if test fails
}

echo -e "Running unit tests\n"
RESULT=0

# Run tests
run_test "doctest_particle"
run_test "doctest_PDG_reader"
run_test "doctest_utils"
run_test "doctest_solution"

if [ $RESULT -eq 0 ]; then
  echo -e "\n\tC++ unit tests: OK\n"
else
  echo -e "\n\tC++ unit tests: Failed\n"
fi

exit $RESULT
