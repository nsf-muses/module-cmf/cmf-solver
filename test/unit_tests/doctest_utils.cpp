#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"
#include "utils.hpp"

TEST_CASE("file_exist function") {
    // Provide test cases for the file_exist function
    CHECK(file_exist("YAML_test.yml") == true);
    CHECK(file_exist("non_existing_file.txt") == false);
}

TEST_CASE("contains function") {
    vector<int> vec = {1, 2, 3, 4, 5};

    // Provide test cases for the contains function
    CHECK(contains(vec, 3) == true);
    CHECK(contains(vec, 6) == false);
}

TEST_CASE("copy_file function") {
    // Provide test cases for the copy_file function
    CHECK(copy_file("YAML_test.yml", "YAML_test.yml_temp") == true);

    // Make sure the file was copied.
    CHECK(file_exist("YAML_test.yml_temp") == true);

    // Make sure to clean up the copied file after the test.
    remove("YAML_test.yml_temp");

    // Make sure the function returns false if the source file does not exist.
    CHECK(copy_file("non_existing_source.txt", "destination.txt") == false);
}

TEST_CASE("create_status_file function") {
    // You can test this function by creating a status file and checking its contents.
    create_status_file(200, "status.yaml", "Test message");

    // Make sure the status file was created.
    CHECK(file_exist("status.yaml") == true);

    // Make sure the status file contains the correct message.
    YAML::Node status = YAML::LoadFile("status.yaml");
    CHECK(status["code"].as<int>() == 200);
    CHECK(status["message"].as<string>() == "Test message");

    // Make sure to clean up the status file after the test.
    remove("status.yaml");
}

TEST_CASE("open_file function") {
    // Provide test cases for the open_file function
    FILE *file = nullptr;
    open_file("output.txt", file);
    CHECK(file != nullptr);

    // Make sure the file was opened.
    CHECK(file_exist("output.txt") == true);

    // Close the file if it was successfully opened
    if (file != nullptr) {
        fclose(file);
    }

    // Make sure to clean up the opened file after the test.
    remove("output.txt");
}