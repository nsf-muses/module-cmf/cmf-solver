#!/bin/bash

# Exit on error
set -euo pipefail

echo -e "Local default test started"

# Retrieve the absolute path of the script
cd "$(dirname "$(readlink -f "$0")")"

# change to src directory
cd ../src/

# Run CMF (preprocess, cmf, clean, postprocess)
make run_default

if [ $? -eq 0 ]; then
  echo  -e "\n\tLocal default test : OK\n"
else
  echo  -e "\n\tLocal default test : Failed\n"
  exit 1
fi

echo "Local default test done"
