#!/bin/bash

set -euo pipefail

# Script to run tests for CMF module and building functionalities

# Retrieve the absolute path of the script
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
cd ${SCRIPT_PATH}

# Define variables for test scripts
UNIT_TESTS_SCRIPT="run_unit_tests.sh"
BUILDING_SCRIPTS=("test-local-default.sh" )

# Function to run a test script and check its result
run_test_script() {
    local script="$1"
    echo "Running $script"
    bash "$script"
    if [ $? -eq 0 ]; then
        echo "$script: Passed"
    else
        echo "$script: Failed"
        exit 1  # Exit the script with an error code
    fi
}

# Run building tests
echo -e "Running building tests\n"

# Change directory to unit_tests and run unit tests
cd unit_tests
bash "$UNIT_TESTS_SCRIPT"
cd ..

# Run building test scripts
for script in "${BUILDING_SCRIPTS[@]}"; do
    run_test_script "$script"
done

echo "All tests completed successfully"

exit 0  # Exit with a success code
