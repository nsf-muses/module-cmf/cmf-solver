#!/bin/bash

set -euo pipefail

echo -e "Test Docker container"

# Retrieve the absolute path of the script
cd "$(dirname "$(readlink -f "$0")")"

# Change to the parent directory
cd ../

# Build the docker image
if docker build . -t cmf:dev; then
  echo "CMF container successfully built"
else
  echo "Error: docker build failed. Aborting."
  exit 1
fi

# Create input directory if it doesn't exist
if [[ ! -d "test/input" ]]; then
    mkdir -p test/input
fi

# Create output directory if it doesn't exist
if [[ ! -d "test/output" ]]; then
    mkdir -p test/output
fi

# default case is now created by create_config.py with no parameters which happens inside the makefile recipe run_default
# this is the only input required from the Calculation Engine, for production runs use make run instead of make run_default

# Run the docker image
docker run -it --rm --name cmf \
  -v "$(pwd)/test/input:/opt/input" \
  -v "$(pwd)/test/output:/opt/output" \
  cmf:dev bash -c 'make run_default'

if [ $? -eq 0 ]; then
  echo  -e "\n\tDocker default test: OK\n"
else
  echo  -e "\n\tDocker default test: Failed\n"
  exit 1
fi

echo "Test Docker container done"
