#!/bin/bash

set -euo pipefail

# Script to run tests for CMF module and container functionalities

# Retrieve the absolute path of the script
SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
cd ${SCRIPT_PATH}

# Define variables for test scripts
CONTAINER_SCRIPTS=("test-container-default.sh" )

# Function to run a test script and check its result
run_test_script() {
    local script="$1"
    echo "Running $script"
    bash "$script"
    if [ $? -eq 0 ]; then
        echo "$script: Passed"
    else
        echo "$script: Failed"
        exit 1  # Exit the script with an error code
    fi
}

# Run container tests
echo -e "Running container tests\n"

# Run container test scripts
for script in "${CONTAINER_SCRIPTS[@]}"; do
    run_test_script "$script"
done

echo "All tests completed successfully"

exit 0  # Exit with a success code
