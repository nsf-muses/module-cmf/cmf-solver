#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
file: plot_EoS_output.py
author: Nikolas Cruz Camacho <cnc6@illinois.edu>
purpose: Create scatter plots from CMF++ output data.

This script reads a CSV file containing CMF++ output data and creates scatter plots

Usage:
    python plot_EoS_output.py <input_csv_file> --output_folder <path_to_output_folder> --show

Arguments:
    input_file: Path to the input CSV file containing CMF++ output data.
    --output_folder: Folder to save the generated plots (optional, defaults to the same folder as the input CSV file).
    --show: Show the plots on screen instead of saving them (optional).

Example:
    python plot_EoS_output.py ../output/C4_default/CMF_output.csv --output_folder ../output/plots --show
"""

import pandas as pd
import matplotlib

matplotlib.use("tkagg")
import matplotlib.pyplot as plt
import argparse
import os

# Define the CMF_VARIABLES
CMF_VARIABLES = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "nS",
    "nQ",
    "energy_density",
    "pressure",
    "entropy_density",
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
    "vector_density_without_Phi_order",
    "quark_density",
    "octet_density",
    "decuplet_density",
]

# Define variable groups for subplots
GROUP1 = [
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
]

GROUP2 = [
    "nB",
    "vector_density_without_Phi_order",
    "quark_density",
    "octet_density",
    "decuplet_density",
]

GROUP3 = [
    "nS",
    "nQ",
    "energy_density",
    "pressure",
    "entropy_density",
]

# Constants and configurations
plt.rc("text", usetex=True)
plt.rc("font", family="serif")


def get_color_and_label(df):
    conditions = [
        (df["quark_density"] > 0)
        & (df["octet_density"] == 0)
        & (df["decuplet_density"] == 0),
        (df["octet_density"] > 0)
        & (df["decuplet_density"] == 0)
        & (df["quark_density"] == 0),
        (df["decuplet_density"] > 0)
        & (df["quark_density"] == 0)
        & (df["octet_density"] > 0),
        (df["decuplet_density"] > 0)
        & (df["quark_density"] == 0)
        & (df["octet_density"] == 0),
        (df["quark_density"] == 0)
        & (df["decuplet_density"] == 0)
        & (df["octet_density"] == 0)
        & (df["Phi_order_field"] < 1e-8),
        (df["quark_density"] == 0)
        & (df["decuplet_density"] == 0)
        & (df["octet_density"] == 0)
        & (df["Phi_order_field"] >= 1e-8),
    ]
    colors = ["#377eb8", "#4daf4a", "#e41a1c", "#984ea3", "#ff7f00", "#17becf"]
    labels = [
        "Quarks",
        "Octet Baryons",
        "Octet+Decuplet Baryons",
        "Decuplet Baryons",
        "Vacuum (Baryons)",
        "Vacuum (Quarks)",
    ]
    default_color = "gray"
    default_label = "Unknown"

    color = [default_color] * len(df)
    label = [default_label] * len(df)

    for cond, col, lbl in zip(conditions, colors, labels):
        for i in range(len(df)):
            if cond[i]:
                color[i] = col
                label[i] = lbl

    return color, label


def create_subplot(df, variables, title, output_folder=None, show=False):
    muB = df["muB"]
    num_vars = len(variables)
    num_cols = 2
    num_rows = (num_vars + num_cols - 1) // num_cols
    fig, axes = plt.subplots(num_rows, num_cols, figsize=(10, num_rows * 5))
    axes = axes.flatten()

    colors, labels = get_color_and_label(df)

    for ax, variable in zip(axes[:-1], variables):
        scatter = ax.scatter(muB, df[variable], c=colors, label=labels)
        ax.set_xlabel("muB")
        ax.set_ylabel(variable)
        ax.set_title(f"{variable} vs muB")
        handles, _ = scatter.legend_elements(prop="colors", alpha=0.6)
        unique_labels = list(set(labels))
        legend_handles = [
            plt.Line2D(
                [0],
                [0],
                marker="o",
                color="w",
                label=lbl,
                markerfacecolor=colors[labels.index(lbl)],
                markersize=10,
            )
            for lbl in unique_labels
        ]
        ax.legend(handles=legend_handles)
        ax.grid(True)

    # Add pressure vs energy scatter plot to the last axis
    ax_last = axes[-1]
    scatter_pressure_energy = ax_last.scatter(
        df["energy_density"], df["pressure"], c=colors, label=labels
    )
    ax_last.set_xlabel("Energy Density")
    ax_last.set_ylabel("Pressure")
    ax_last.set_title("Pressure vs Energy Density")
    handles, _ = scatter_pressure_energy.legend_elements(prop="colors", alpha=0.6)
    legend_handles = [
        plt.Line2D(
            [0],
            [0],
            marker="o",
            color="w",
            label=lbl,
            markerfacecolor=colors[labels.index(lbl)],
            markersize=10,
        )
        for lbl in unique_labels
    ]
    ax_last.legend(handles=legend_handles)
    ax_last.grid(True)

    fig.suptitle(title, fontsize=16)

    if output_folder:
        output_path = os.path.join(
            output_folder, f"{title.replace(' ', '_').replace(':', '')}.pdf"
        )
        plt.savefig(output_path, format="pdf")

    if show:
        plt.show()

    plt.close()


def main():
    parser = argparse.ArgumentParser(description="Create scatter plots from CSV data.")
    parser.add_argument("input_file", type=str, help="Path to the input CSV file")
    parser.add_argument(
        "--output_folder",
        type=str,
        help="Folder to save the plots (defaults to the same folder as the input CSV file)",
    )
    parser.add_argument("--show", action="store_true", help="Show the plots on screen")

    args = parser.parse_args()

    df = pd.read_csv(args.input_file, names=CMF_VARIABLES, engine="python")

    if args.output_folder:
        output_folder = args.output_folder
    else:
        output_folder = os.path.dirname(args.input_file)

    os.makedirs(output_folder, exist_ok=True)

    create_subplot(
        df,
        GROUP1,
        "Group 1: Mean Fields",
        show=args.show,
        output_folder=output_folder,
    )
    create_subplot(
        df,
        GROUP2,
        "Group 2: Densities",
        show=args.show,
        output_folder=output_folder,
    )
    create_subplot(
        df,
        GROUP3,
        "Group 3: Thermodynamic Quantities",
        show=args.show,
        output_folder=output_folder,
    )


if __name__ == "__main__":
    main()
