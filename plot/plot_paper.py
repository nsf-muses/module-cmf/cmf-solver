#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import glob
import yaml
import numpy as np
import pandas as pd
import matplotlib
import warnings
import numpy as np

matplotlib.use("tkagg")
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
from matplotlib.colors import Normalize

"""
file: plot_paper.py
author: Nikolas Cruz Camacho <cnc6@illinois.edu>
purpose: plot the output files from CMF++ for the zero temperature paper
"""

# Constants and configurations
plt.rc("text", usetex=True)
plt.rc("font", family="serif")

# Calculate figsize for 16:9 aspect ratio on letter size paper
letter_size = (8.5, 11)
aspect_ratio = 16 / 9
one_figsize_width = letter_size[0]
one_figsize_height = 1.2 * one_figsize_width / aspect_ratio

# UIUC colors
UIUC_BLUE = "#13294B"
UIUC_ORANGE = "#E84A27"

CMF_VARIABLES = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "nS",
    "nQ",
    "energy_density",
    "pressure",
    "entropy_density",
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
    "nB_without_Phi_order ",
    "quark_density",
    "octet_density",
    "decuplet_density",
]

CMF_FORTRAN_VARIABLES = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "Y_S",
    "Y_Q",
    "energy_density",
    "pressure",
    "entropy_density",
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
    "nB_without_Phi_order ",
    "quark_density",
    "octet_density",
    "decuplet_density",
    "bah",
]

SEPARATOR = ","

N_SAT = 0.15  # [fm^-3]
HBARC = 197.3269804  # [MeV fm]
SIGMA_0 = -93.300003  # [MeV]
ZETA_0 = -106.560990  # [MeV]

LINEWIDTH = 5.0
MARKERSIZE = 60.0

FONTSIZE_TITLE = 34
FONTSIZE_LEGEND = 18
FONTSIZE_TICKS = 26
FONTSIZE_LABEL = 26
FONTSIZE_LETTER = 26

WIDTH_SPACE = 0.75
HEIGHT_SPACE = 1.5
PAD_LABEL = 8


def compute_derivative(y, x, order):
    """
    Recursive function to calculate the nth derivative of a function y(x) using np.gradient
    input:
        y: function to be differentiated
        x: independent variable
        order: order of the derivative
    output:
        order nth derivative of y(x)
    """
    # Suppress specific warnings
    warnings.filterwarnings("ignore", category=RuntimeWarning)

    try:
        if len(y) == 0 or len(x) == 0:
            return np.zeros_like(x)

        for _ in range(order):
            y = np.gradient(y, x, edge_order=2)

        return y
    except Exception as e:
        print(f"Unable to compute derivative: {e}")
        return np.zeros_like(x)


def compute_speed_of_sound(df):
    groups = df.groupby(["muS", "muQ"])

    for name, group in groups:
        # Sort by energy_density
        group = group.sort_values(by="energy_density")

        # Compute speed of sound squared
        group["cs2"] = compute_derivative(group["pressure"], group["energy_density"], 1)

        # Assign the modified group back to the original DataFrame
        df.loc[group.index, "cs2"] = group["cs2"]

    return df


def compute_muB_derivatives(df):

    groups = df.groupby(["muS", "muQ"])

    for name, group in groups:

        # Sort by muB
        group = group.sort_values(by="muB")

        # Compute second derivative of nB with respect to muB
        group["chi2"] = compute_derivative(group["nB"], group["muB"], 1)

        # Compute third derivative of nB with respect to muB
        group["chi3"] = compute_derivative(group["nB"], group["muB"], 2)

        # Rescale susceptibilities to MeV^3
        group["chi2"] = group["chi2"] * HBARC ** 3
        group["chi3"] = group["chi3"] * HBARC ** 3

        # Renormalize chi2 and chi3
        group["chi2_renorm"] = group["chi2"] / (group["muB"] ** 2)
        group["chi3_renorm"] = group["chi3"] / group["muB"]

        # Assign the modified group back to the original DataFrame
        df.loc[group.index, "chi2"] = group["chi2"]
        df.loc[group.index, "chi3"] = group["chi3"]
        df.loc[group.index, "chi2_renorm"] = group["chi2_renorm"]
        df.loc[group.index, "chi3_renorm"] = group["chi3_renorm"]

    return df


def compute_derived_observables(df):
    df = df.copy()  # Explicitly create a copy to avoid chained indexing warnings

    # if Y_S is already computed, then we don't need to compute it again (for fortran)
    if "Y_S" not in df.columns:
        df.loc[:, "Y_S"] = df["nS"] / (
            df["quark_density"] + df["octet_density"] + df["decuplet_density"]
        )

    # if Y_Q is already computed, then we don't need to compute it again (for fortran)
    if "Y_Q" not in df.columns:
        df.loc[:, "Y_Q"] = df["nQ"] / (
            df["quark_density"] + df["octet_density"] + df["decuplet_density"]
        )

    if "delta" not in df.columns:
        df.loc[:, "delta"] = 1.0 - 2.0 * (df.loc[:, "Y_Q"] - df.loc[:, "Y_S"] / 2.0)

    # df.loc[:, "trace_anomaly"] = 1.0 / 3.0 - df["pressure"] / df["energy_density"]

    # df.loc[:, "adiabatic_index"] = df["cs2"] / (1.0 / 3.0 - df["trace_anomaly"])

    # df.loc[:, "delta_prime"] = (
    #     compute_derivative(df["trace_anomaly"], df["energy_density"], 1)
    #     / df["energy_density"]
    # )

    # df.loc[:, "conformality"] = np.sqrt(df["trace_anomaly"] ** 2 + df["delta_prime"] ** 2)

    return df


def load_yaml_data(file_path):
    with open(file_path, "r") as file:
        return yaml.safe_load(file)


def create_output_directories():
    plot_directory = os.path.join(".", "plots_paper")
    muS_values = ["muS0", "muSNot0"]
    muQ_values = ["muQ0", "muQNot0"]

    for muS in muS_values:
        for muQ in muQ_values:
            directory_path = os.path.join(plot_directory, f"{muS}_{muQ}")
            os.makedirs(directory_path, exist_ok=True)

    return plot_directory


def load_data(file_path, variables, delimiter):
    return pd.read_csv(file_path, delimiter=delimiter, names=variables, engine="python")


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Plot CMF++ output.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--show", action="store_true", default=False, help="Show the plots."
    )
    parser.add_argument(
        "--save", action="store_true", default=True, help="Save the plots."
    )
    return parser.parse_args()


def read_data(run_name, fortran=False, debug=False, read_metastable_and_unstable=False):
    df_stable = load_data(
        os.path.join("..", "output", run_name, "CMF_output_stable.csv"),
        CMF_VARIABLES,
        SEPARATOR,
    )

    # check_test_data(df_stable)

    if read_metastable_and_unstable:
        df_metastable = load_data(
            os.path.join("..", "output", run_name, "CMF_output_metastable.csv"),
            CMF_VARIABLES,
            SEPARATOR,
        )
        df_unstable = load_data(
            os.path.join("..", "output", run_name, "CMF_output_unstable.csv"),
            CMF_VARIABLES,
            SEPARATOR,
        )
    if debug:
        df_debug = load_data(
            os.path.join("..", "output", run_name, "CMF_intermediate_output_debug.csv"),
            CMF_VARIABLES,
            SEPARATOR,
        )
    if fortran:
        fortran_filename = os.path.join("..", "output", f"{run_name}.fortran_EoS")

        df_fortran = load_data(fortran_filename, CMF_FORTRAN_VARIABLES, SEPARATOR)

        # drop bah column
        df_fortran = df_fortran.drop(columns=["bah"])

        # flip the values of muS for fortran
        df_fortran["muS"] = -df_fortran["muS"]

        # rescale quark_density for fortran (Raj forgot this in the fortran code)
        df_fortran["quark_density"] = df_fortran["quark_density"] / 3

    dataframes = [df_stable]

    if read_metastable_and_unstable:
        dataframes.append(df_metastable)
        dataframes.append(df_unstable)

    if debug:
        dataframes.append(df_debug)
    if fortran:
        dataframes.append(df_fortran)

        # check_test_data(df_fortran)

    # drop Nan's in all dataframes
    dataframes = [df.dropna() for df in dataframes]

    return dataframes


def check_test_data(df):
    print(df.head())
    print(df.describe())

    # group by muS and muQ
    groups = df.groupby(["muS", "muQ"])

    for (muS, muQ), group in groups:

        # plot pressure vs muB
        plt.plot(group["muB"], group["pressure"], label=f"muS={muS}, muQ={muQ}")
        plt.xlabel("muB")
        plt.ylabel("pressure")
        plt.legend()
        plt.show()


def filter_fortan_unphysical_data(df_fortran, df_stable, mean_fields, tolerance=0.1):
    # Group both dataframes by muS and muQ
    groups_fortran = df_fortran.groupby(["muS", "muQ"])
    groups_stable = df_stable.groupby(["muS", "muQ"])

    filtered_rows = []

    for (muS, muQ), group_stable in groups_stable:
        # Check if the muS, muQ group is present in Fortran
        if (muS, muQ) in groups_fortran.groups:
            # Sort the Fortran group by 'muB' in ascending order
            group_fortran = groups_fortran.get_group((muS, muQ)).sort_values(
                by="muB", ascending=True
            )
            group_stable = group_stable.sort_values(by="muB", ascending=True)

            # Iterate through each point in stable group
            for __, row_stable in group_stable.iterrows():
                muB_stable = row_stable["muB"]

                # Check if muB_stable is present in Fortran group
                if muB_stable in group_fortran["muB"].values:
                    # Find the corresponding row in Fortran
                    closest_row = group_fortran[
                        group_fortran["muB"] == muB_stable
                    ].iloc[0]

                    # Check the absolute difference of mean fields
                    absolute_diff = abs(
                        closest_row[mean_fields] - row_stable[mean_fields]
                    )

                    # If all absolute differences are small, add the Fortran row to the list
                    if all(absolute_diff <= tolerance):
                        filtered_rows.append(closest_row)

    # Create a DataFrame from the collected rows
    filtered_data = pd.DataFrame(filtered_rows)

    return filtered_data


def filter_data(
    dataframes,
    fortran=False,
    debug=False,
    read_metastable_and_unstable=False,
):
    df_stable = dataframes[0]
    if read_metastable_and_unstable:
        df_metastable = dataframes[1]
        df_unstable = dataframes[2]

    if debug:
        df_debug = dataframes[-2]
    if fortran:
        df_fortran = dataframes[-1]

    min_muB = df_stable["muB"].min()
    max_muB = df_stable["muB"].max()

    if fortran:

        df_fortran = df_fortran[
            (df_fortran["muB"] > min_muB) & (df_fortran["muB"] < max_muB)
        ]

    if fortran:
        mean_fields = [
            "sigma_mean_field",
            "zeta_mean_field",
            "delta_mean_field",
            "omega_mean_field",
            "phi_mean_field",
            "rho_mean_field",
            "Phi_order_field",
        ]
        df_fortran = filter_fortan_unphysical_data(
            df_fortran, df_stable, mean_fields, tolerance=0.1
        )

    dataframes = [
        df_stable,
    ]

    if read_metastable_and_unstable:
        dataframes.append(df_metastable)
        dataframes.append(df_unstable)

    if debug:
        dataframes.append(df_debug)
    if fortran:
        dataframes.append(df_fortran)

    return dataframes


def compute_observables(
    dataframes,
    fortran=False,
    debug=False,
    read_metastable_and_unstable=False,
):
    df_stable = dataframes[0]
    if read_metastable_and_unstable:
        df_metastable = dataframes[1]
        df_unstable = dataframes[2]

    if debug:
        df_debug = dataframes[-2]

    if fortran:
        df_fortran = dataframes[-1]

    df_stable = compute_speed_of_sound(df_stable)
    if read_metastable_and_unstable:
        df_metastable = compute_speed_of_sound(df_metastable)
        df_unstable = compute_speed_of_sound(df_unstable)

    if debug:
        df_debug = compute_speed_of_sound(df_debug)

    if fortran:
        df_fortran = compute_speed_of_sound(df_fortran)

    df_stable = compute_muB_derivatives(df_stable)
    if read_metastable_and_unstable:
        df_metastable = compute_muB_derivatives(df_metastable)
        df_unstable = compute_muB_derivatives(df_unstable)

    if debug:
        df_debug = compute_muB_derivatives(df_debug)

    if fortran:
        df_fortran = compute_muB_derivatives(df_fortran)

    df_stable = compute_derived_observables(df_stable)
    if read_metastable_and_unstable:
        df_metastable = compute_derived_observables(df_metastable)
        df_unstable = compute_derived_observables(df_unstable)

    if debug:
        df_debug = compute_derived_observables(df_debug)

    if fortran:
        df_fortran = compute_derived_observables(df_fortran)

        # flip the sign of Y_S for fortran
        df_fortran["Y_S"] = -df_fortran["Y_S"]

    dataframes = [
        df_stable,
    ]

    if read_metastable_and_unstable:
        dataframes.append(df_metastable)
        dataframes.append(df_unstable)

    if debug:
        dataframes.append(df_debug)

    if fortran:
        dataframes.append(df_fortran)

    return dataframes


def plot_line_2D_data(ax, x, y, color, linestyle, linewidth, label, zorder):
    ax.plot(
        x,
        y,
        color=color,
        linestyle=linestyle,
        linewidth=linewidth,
        label=label,
        zorder=zorder,
    )


def plot_scatter_2D_data(
    ax, x, y, edgecolor, facecolor, marker, markersize, label, zorder
):
    ax.scatter(
        x,
        y,
        edgecolors=edgecolor,
        facecolors=facecolor,
        marker=marker,
        s=markersize,
        label=label,
        zorder=zorder,
    )


def make_cs2_zero_for_first_order_PT(df):
    # Sort by muB and calculate differences in energy density
    df = df.sort_values(by="muB")

    muB = df["muB"]

    # Get the dataframe index of the highest energy density difference
    energy_diff = df["energy_density"].diff()

    # Find the muB for the maximum energy density difference
    max_energy_diff_index = energy_diff.idxmax()

    # Create a new dataframe until the muB for the maximum energy density difference
    df2 = df[df["muB"] < muB[max_energy_diff_index]]

    # Get the last nB and muB values from df2
    last_nB = df2["nB"].iloc[-1]
    last_muB = df2["muB"].iloc[-1]

    # Create a new dataframe after the muB for the maximum energy density difference
    df3 = df[df["muB"] >= muB[max_energy_diff_index]]

    # Get the first nB and muB values from df3
    first_nB = df3["nB"].iloc[0]
    first_muB = df3["muB"].iloc[0]

    # Generate new nB and muB values for the intermediate DataFrame
    nB_values = np.linspace(last_nB * 1.001, first_nB * 0.999, num=100)
    muB_values = np.linspace(last_muB * 1.001, first_muB * 0.999, num=100)

    # Create a new DataFrame with the same columns as df, filling with NaN
    df_cs2 = pd.DataFrame(columns=df.columns)

    # Set the muB, nB, and cs2 columns
    df_cs2["muB"] = muB_values
    df_cs2["nB"] = nB_values
    df_cs2["cs2"] = 0

    # sort by nB
    df2 = df2.sort_values(by="nB")
    df3 = df3.sort_values(by="nB")
    df_cs2 = df_cs2.sort_values(by="nB")

    # Concatenate df2, df_cs2, and df3
    df_merged = pd.concat([df2, df_cs2, df3])

    return df_merged


def create_2D_mean_fields_and_obs_panel_4_mean_fields(
    dataframes,
    legends,
    plot_type,
    linestyles,
    linewidths,
    markers,
    markersizes,
    colors,
    zorder,
    cs2_interp_list,
    **kwargs,
):

    # Create the figure and subplots
    fig, axs = plt.subplots(
        nrows=4, ncols=2, figsize=(2 * one_figsize_width, 4 * one_figsize_height)
    )

    # Plot the data on the subplots
    for i, df in enumerate(dataframes):
        if df is not None:

            if plot_type[i] == "plot":
                plot_line_2D_data(
                    axs[0, 0],
                    df["muB"],
                    df["sigma_mean_field"] / SIGMA_0,
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[0, 1],
                    df["muB"],
                    df["zeta_mean_field"] / ZETA_0,
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[1, 0],
                    df["muB"],
                    df["omega_mean_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[1, 1],
                    df["muB"],
                    df["phi_mean_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[2, 0],
                    df["muB"],
                    df["Phi_order_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[2, 1],
                    df["energy_density"],
                    df["pressure"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )

                if i in cs2_interp_list:

                    df_cs2_first_PT = make_cs2_zero_for_first_order_PT(df)

                    plot_line_2D_data(
                        axs[3, 0],
                        df_cs2_first_PT["nB"] / N_SAT,
                        df_cs2_first_PT["cs2"],
                        colors[i],
                        linestyles[i],
                        linewidths[i],
                        legends[i],
                        zorder[i],
                    )

                else:
                    plot_line_2D_data(
                        axs[3, 0],
                        df["nB"] / N_SAT,
                        df["cs2"],
                        colors[i],
                        linestyles[i],
                        linewidths[i],
                        legends[i],
                        zorder[i],
                    )

                plot_line_2D_data(
                    axs[3, 1],
                    df["muB"],
                    df["nB"] / N_SAT,
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
            elif plot_type[i] == "scatter":
                plot_scatter_2D_data(
                    axs[0, 0],
                    df["muB"],
                    df["sigma_mean_field"] / SIGMA_0,
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[0, 1],
                    df["muB"],
                    df["zeta_mean_field"] / ZETA_0,
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[1, 0],
                    df["muB"],
                    df["omega_mean_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[1, 1],
                    df["muB"],
                    df["phi_mean_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[2, 0],
                    df["muB"],
                    df["Phi_order_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[2, 1],
                    df["energy_density"],
                    df["pressure"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[3, 0],
                    df["nB"] / N_SAT,
                    df["cs2"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[3, 1],
                    df["muB"],
                    df["nB"] / N_SAT,
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )

    # add legend for [0, 1]
    legend = axs[0, 1].legend(loc="best", fontsize=FONTSIZE_LEGEND)

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)", "g)", "h)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("xlim") is not None:
        ax = axs.flatten()
        for i in range(8):
            ax[i].set_xlim(kwargs.get("xlim")[i])

    if kwargs.get("ylim") is not None:
        ax = axs.flatten()
        for i in range(8):
            ax[i].set_ylim(kwargs.get("ylim")[i])

    # Set x labels, y labels, and formatting for each subplot
    x_label = r"$\mu_B\, \mathrm{[MeV]}$"
    x_label_nB = r"$n_B\, \mathrm{[n_{sat}]}$"

    axs[0, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[0, 0].set_ylabel(
        r"$\sigma/\sigma_0$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[0, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[0, 1].set_ylabel(
        r"$\zeta/\zeta_0$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[1, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[1, 0].set_ylabel(
        r"$\omega \, \mathrm{[MeV]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[1, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[1, 1].set_ylabel(
        r"$\phi \, \mathrm{[MeV]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[2, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[2, 0].set_ylabel(r"$\Phi$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)

    axs[2, 1].set_xlabel(
        r"$\varepsilon \, \mathrm{[MeV/fm^{3}]}$",
        fontsize=FONTSIZE_LABEL,
        labelpad=PAD_LABEL,
    )
    axs[2, 1].set_ylabel(
        r"$P \, \mathrm{[MeV/fm^{3}]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[3, 0].set_xlabel(x_label_nB, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[3, 0].set_ylabel(r"$c_s^2$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)

    axs[3, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[3, 1].set_ylabel(
        r"$n_B \, \mathrm{[n_{sat}]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[3, 0].set_xlim([0, 25.1])

    for i in range(4):
        for j in range(2):
            axs[i, j].tick_params(axis="both", labelsize=FONTSIZE_TICKS)
            axs[i, j].yaxis.get_offset_text().set_size(FONTSIZE_TICKS)

    if kwargs.get("grid"):
        for i in range(4):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".pdf")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="pdf")

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_2D_mean_fields_and_obs_panel_6_mean_fields(
    dataframes,
    legends,
    plot_type,
    linestyles,
    linewidths,
    markers,
    markersizes,
    colors,
    zorder,
    cs2_interp_list,
    **kwargs,
):

    # Create the figure and subplots
    fig, axs = plt.subplots(
        nrows=5, ncols=2, figsize=(2 * one_figsize_width, 5 * one_figsize_height)
    )

    # Plot the data on the subplots
    for i, df in enumerate(dataframes):
        if df is not None:

            if plot_type[i] == "plot":
                plot_line_2D_data(
                    axs[0, 0],
                    df["muB"],
                    df["sigma_mean_field"] / SIGMA_0,
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[0, 1],
                    df["muB"],
                    df["zeta_mean_field"] / ZETA_0,
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[1, 0],
                    df["muB"],
                    df["delta_mean_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[1, 1],
                    df["muB"],
                    df["omega_mean_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[2, 0],
                    df["muB"],
                    df["phi_mean_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[2, 1],
                    df["muB"],
                    df["rho_mean_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[3, 0],
                    df["muB"],
                    df["Phi_order_field"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[3, 1],
                    df["energy_density"],
                    df["pressure"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                if i in cs2_interp_list:

                    df_cs2_first_PT = make_cs2_zero_for_first_order_PT(df)

                    plot_line_2D_data(
                        axs[4, 0],
                        df_cs2_first_PT["nB"] / N_SAT,
                        df_cs2_first_PT["cs2"],
                        colors[i],
                        linestyles[i],
                        linewidths[i],
                        legends[i],
                        zorder[i],
                    )
                else:
                    plot_line_2D_data(
                        axs[4, 0],
                        df["nB"] / N_SAT,
                        df["cs2"],
                        colors[i],
                        linestyles[i],
                        linewidths[i],
                        legends[i],
                        zorder[i],
                    )
                plot_line_2D_data(
                    axs[4, 1],
                    df["muB"],
                    df["nB"] / N_SAT,
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )

            elif plot_type[i] == "scatter":
                plot_scatter_2D_data(
                    axs[0, 0],
                    df["muB"],
                    df["sigma_mean_field"] / SIGMA_0,
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[0, 1],
                    df["muB"],
                    df["zeta_mean_field"] / ZETA_0,
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[1, 0],
                    df["muB"],
                    df["delta_mean_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[1, 1],
                    df["muB"],
                    df["omega_mean_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[2, 0],
                    df["muB"],
                    df["phi_mean_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[2, 1],
                    df["muB"],
                    df["rho_mean_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[3, 0],
                    df["muB"],
                    df["Phi_order_field"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[3, 1],
                    df["energy_density"],
                    df["pressure"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[4, 0],
                    df["nB"] / N_SAT,
                    df["cs2"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[4, 1],
                    df["muB"],
                    df["nB"] / N_SAT,
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )

    # add legend for [0, 1]
    legend = axs[0, 1].legend(loc="best", fontsize=FONTSIZE_LEGEND)

    # add letter to subplots
    for i, letter in enumerate(
        ["a)", "b)", "c)", "d)", "e)", "f)", "g)", "h)", "i)", "j)"]
    ):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    axs[4, 0].set_xlim([0, 25.1])

    if kwargs.get("xlim") is not None:
        ax = axs.flatten()
        for i in range(10):
            ax[i].set_xlim(kwargs.get("xlim")[i])

    if kwargs.get("ylim") is not None:
        ax = axs.flatten()
        for i in range(10):
            ax[i].set_ylim(kwargs.get("ylim")[i])

    # Set x labels, y labels, and formatting for each subplot
    x_label = r"$\mu_B\, \mathrm{[MeV]}$"
    x_label_nB = r"$n_B\, \mathrm{[n_{sat}]}$"

    axs[0, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[0, 0].set_ylabel(
        r"$\sigma/\sigma_0$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[0, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[0, 1].set_ylabel(
        r"$\zeta/\zeta_0$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[1, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[1, 0].set_ylabel(
        r"$\delta \, \mathrm{[MeV]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[1, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[1, 1].set_ylabel(
        r"$\omega \, \mathrm{[MeV]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[2, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[2, 0].set_ylabel(
        r"$\phi \, \mathrm{[MeV]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[2, 1].set_xlabel(
        x_label,
        fontsize=FONTSIZE_LABEL,
        labelpad=PAD_LABEL,
    )
    axs[2, 1].set_ylabel(
        r"$\rho \, \mathrm{[MeV]}$",
        fontsize=FONTSIZE_LABEL,
        labelpad=PAD_LABEL,
    )

    axs[3, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[3, 0].set_ylabel(r"$\Phi$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)

    axs[3, 1].set_xlabel(
        r"$\varepsilon \, \mathrm{[MeV/fm^{3}]}$",
        fontsize=FONTSIZE_LABEL,
        labelpad=PAD_LABEL,
    )
    axs[3, 1].set_ylabel(
        r"$P \, \mathrm{[MeV/fm^{3}]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[4, 0].set_xlabel(x_label_nB, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[4, 0].set_ylabel(r"$c_s^2$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)

    axs[4, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[4, 1].set_ylabel(
        r"$n_B \, \mathrm{[n_{sat}]}$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    for i in range(5):
        for j in range(2):
            axs[i, j].tick_params(axis="both", labelsize=FONTSIZE_TICKS)
            axs[i, j].yaxis.get_offset_text().set_size(FONTSIZE_TICKS)

    if kwargs.get("grid"):
        for i in range(5):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".pdf")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="pdf")

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_2D_densities_and_sus_panel(
    dataframes,
    legends,
    plot_type,
    linestyles,
    linewidths,
    markers,
    markersizes,
    colors,
    zorder,
    **kwargs,
):

    # Create the figure and subplots
    fig, axs = plt.subplots(
        nrows=2, ncols=2, figsize=(2 * one_figsize_width, 2 * one_figsize_height)
    )

    # Plot the data on the subplots
    for i, df in enumerate(dataframes):
        if df is not None:
            if plot_type[i] == "plot":
                plot_line_2D_data(
                    axs[0, 0],
                    df["muB"],
                    df["Y_S"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[0, 1],
                    df["muB"],
                    df["Y_Q"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[1, 0],
                    df["muB"],
                    df["chi2_renorm"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
                plot_line_2D_data(
                    axs[1, 1],
                    df["muB"],
                    df["chi3_renorm"],
                    colors[i],
                    linestyles[i],
                    linewidths[i],
                    legends[i],
                    zorder[i],
                )
            elif plot_type[i] == "scatter":
                plot_scatter_2D_data(
                    axs[0, 0],
                    df["muB"],
                    df["Y_S"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[0, 1],
                    df["muB"],
                    df["Y_Q"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[1, 0],
                    df["muB"],
                    df["chi2_renorm"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )
                plot_scatter_2D_data(
                    axs[1, 1],
                    df["muB"],
                    df["chi3_renorm"],
                    colors[i],
                    colors[i],
                    markers[i],
                    markersizes[i],
                    legends[i],
                    zorder[i],
                )

    # add legend for [0, 0]
    legend = axs[0, 0].legend(loc="best", fontsize=FONTSIZE_LEGEND)

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    # Set x labels, y labels, and formatting for each subplot
    x_label = r"$\mu_B\, \mathrm{[MeV]}$"

    axs[0, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[0, 0].set_ylabel(r"$Y_S$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)

    axs[0, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[0, 1].set_ylabel(r"$Y_Q$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)

    axs[1, 0].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[1, 0].set_ylabel(
        r"$\chi_2 / \mu_B^2$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    axs[1, 1].set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    axs[1, 1].set_ylabel(
        r"$\chi_3 / \mu_B$", fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL
    )

    for i in range(2):
        for j in range(2):
            axs[i, j].tick_params(axis="both", labelsize=FONTSIZE_TICKS)
            axs[i, j].yaxis.get_offset_text().set_size(FONTSIZE_TICKS)

            if kwargs.get("grid"):
                axs[i, j].grid(True)

    if kwargs.get("xlim") is not None:
        ax = axs.flatten()
        for i in range(4):
            ax[i].set_xlim(kwargs.get("xlim")[i])

    if kwargs.get("ylim") is not None:
        ax = axs.flatten()
        for i in range(4):
            ax[i].set_ylim(kwargs.get("ylim")[i])

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".pdf")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="pdf")

    if kwargs.get("show"):
        plt.show()

    plt.close()


def plot_scatter_data(
    ax,
    x,
    y,
    z,
    x_label,
    y_label,
    z_label,
    xlim=None,
    ylim=None,
    kwargs=None,
    colorbar_kwargs=None,
    colorbar=True,
):
    """
    Create a scatter plot on a given axis.

    Parameters:
        ax (matplotlib.axes.Axes): The axis to plot on.
        x (array-like): 1D array of x values.
        y (array-like): 1D array of y values.
        z (array-like): 1D array of z values.
        kwargs (dict, optional): Additional keyword arguments for customization.
        colorbar_kwargs (dict, optional): Additional keyword arguments for the colorbar.
    """
    if kwargs is None:
        kwargs = {}
    if colorbar_kwargs is None:
        colorbar_kwargs = {}

    # Plot the scatter
    sc = ax.scatter(x, y, c=z, marker="o", s=1, **kwargs)

    if colorbar:
        # plot the colorbar
        cbar = plt.colorbar(sc, ax=ax)
        cbar.set_label(
            z_label,
            fontsize=colorbar_kwargs.get("cbar_label_size"),
            labelpad=colorbar_kwargs.get("cbar_label_pad"),
        )

    # Set x and y labels
    ax.set_xlabel(x_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)
    ax.set_ylabel(y_label, fontsize=FONTSIZE_LABEL, labelpad=PAD_LABEL)

    # Set x and y ticks
    ax.tick_params(axis="both", labelsize=FONTSIZE_TICKS)
    ax.yaxis.get_offset_text().set_size(FONTSIZE_TICKS)

    # Set the x and y limits
    if xlim is not None:
        ax.set_xlim(xlim)

    if ylim is not None:
        ax.set_ylim(ylim)

    if colorbar:
        # Set the colorbar ticks
        cbar.ax.tick_params(
            labelsize=colorbar_kwargs.get("cbar_tick_params_size"),
            pad=colorbar_kwargs.get("cbar_tick_pad", 20),
        )

        # Set the colorbar size and padding
        cbar.ax.yaxis.set_tick_params(
            size=colorbar_kwargs.get("cbar_size"), pad=colorbar_kwargs.get("cbar_pad")
        )


def create_3D_vs_muQ_reduced_panel(
    df,
    **kwargs,
):

    # if speed of sound is negative, make it zero
    df["cs2"] = df["cs2"].apply(lambda x: 0 if x < 0 else x)

    # remove cs2 < 0 and cs2 > 1 data
    df = df[(df["cs2"] >= 0) & (df["cs2"] <= 1)]

    # Create the 3D figure and subplots
    fig = plt.figure(figsize=(2 * one_figsize_width, 3 * one_figsize_height))
    axs = fig.subplots(nrows=3, ncols=2)

    # Plot the data on the subplots

    other_kwargs = {
        "cbar_size": 0.4,
        "cbar_pad": 0.15,
        "cbar_tick_params_size": FONTSIZE_TICKS,
        "cbar_label_size": FONTSIZE_LABEL,
        "cbar_label_pad": 10,
    }

    x_label = r"$\mu_B\, \mathrm{[MeV]}$"
    y_label = r"$\mu_Q\, \mathrm{[MeV]}$"

    z_label = r"$\sigma/\sigma_0$"
    plot_scatter_data(
        axs[0, 0],
        df["muB"],
        df["muQ"],
        df["sigma_mean_field"] / SIGMA_0,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\Phi$"
    plot_scatter_data(
        axs[0, 1],
        df["muB"],
        df["muQ"],
        df["Phi_order_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$Y_S$"
    plot_scatter_data(
        axs[1, 0],
        df["muB"],
        df["muQ"],
        df["Y_S"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$Y_Q$"
    plot_scatter_data(
        axs[1, 1],
        df["muB"],
        df["muQ"],
        df["Y_Q"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$n_B \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[2, 0],
        df["muB"],
        df["muQ"],
        df["nB"] / N_SAT,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$c_s^2$"
    plot_scatter_data(
        axs[2, 1],
        df["muB"],
        df["muQ"],
        df["cs2"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("grid"):
        for i in range(3):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Display, save, and close the plot
    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".png")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="png", dpi=300)

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_3D_vs_muS_reduced_panel(
    df,
    **kwargs,
):

    # if speed of sound is negative, make it zero
    df["cs2"] = df["cs2"].apply(lambda x: 0 if x < 0 else x)

    # remove cs2 < 0 and cs2 > 1 data
    df = df[(df["cs2"] >= 0) & (df["cs2"] <= 1)]

    # remove vacuum data

    # Create the 3D figure and subplots
    fig = plt.figure(figsize=(2 * one_figsize_width, 3 * one_figsize_height))
    axs = fig.subplots(nrows=3, ncols=2)

    other_kwargs = {
        "cbar_size": 0.4,
        "cbar_pad": 0.15,
        "cbar_tick_params_size": FONTSIZE_TICKS,
        "cbar_label_size": FONTSIZE_LABEL,
        "cbar_label_pad": 10,
    }

    x_label = r"$\mu_B\, \mathrm{[MeV]}$"
    y_label = r"$\mu_S\, \mathrm{[MeV]}$"

    z_label = r"$\sigma/\sigma_0$"
    plot_scatter_data(
        axs[0, 0],
        df["muB"],
        df["muS"],
        df["sigma_mean_field"] / SIGMA_0,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\Phi$"
    plot_scatter_data(
        axs[0, 1],
        df["muB"],
        df["muS"],
        df["Phi_order_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$Y_S$"
    plot_scatter_data(
        axs[1, 0],
        df["muB"],
        df["muS"],
        df["Y_S"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$Y_Q$"
    plot_scatter_data(
        axs[1, 1],
        df["muB"],
        df["muS"],
        df["Y_Q"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$n_B \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[2, 0],
        df["muB"],
        df["muS"],
        df["nB"] / N_SAT,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$c_s^2$"
    plot_scatter_data(
        axs[2, 1],
        df["muB"],
        df["muS"],
        df["cs2"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("grid"):
        for i in range(3):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Display, save, and close the plot
    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".png")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="png", dpi=300)

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_3D_mean_fields_vs_muQ_panel(
    df,
    **kwargs,
):

    # if speed of sound is negative, make it zero
    df["cs2"] = df["cs2"].apply(lambda x: 0 if x < 0 else x)

    # remove cs2 < 0 and cs2 > 1 data
    df = df[(df["cs2"] >= 0) & (df["cs2"] <= 1)]

    # Create the 3D figure and subplots
    fig = plt.figure(figsize=(2 * one_figsize_width, 3 * one_figsize_height))
    axs = fig.subplots(nrows=3, ncols=2)

    # Plot the data on the subplots

    other_kwargs = {
        "cbar_size": 0.4,
        "cbar_pad": 0.15,
        "cbar_tick_params_size": FONTSIZE_TICKS,
        "cbar_label_size": FONTSIZE_LABEL,
        "cbar_label_pad": 10,
    }

    x_label = r"$\mu_B\, \mathrm{[MeV]}$"
    y_label = r"$\mu_Q\, \mathrm{[MeV]}$"

    z_label = r"$\sigma/\sigma_0$"
    plot_scatter_data(
        axs[0, 0],
        df["muB"],
        df["muQ"],
        df["sigma_mean_field"] / SIGMA_0,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\zeta/\zeta_0$"
    plot_scatter_data(
        axs[0, 1],
        df["muB"],
        df["muQ"],
        df["zeta_mean_field"] / ZETA_0,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\delta \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[1, 0],
        df["muB"],
        df["muQ"],
        df["delta_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\omega \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[1, 1],
        df["muB"],
        df["muQ"],
        df["omega_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\phi \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[2, 0],
        df["muB"],
        df["muQ"],
        df["phi_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\rho \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[2, 1],
        df["muB"],
        df["muQ"],
        df["rho_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("grid"):
        for i in range(3):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Display, save, and close the plot
    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".png")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="png", dpi=300)

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_3D_observables_vs_muQ_panel(
    df,
    **kwargs,
):

    # if speed of sound is negative, make it zero
    df["cs2"] = df["cs2"].apply(lambda x: 0 if x < 0 else x)

    # remove cs2 < 0 and cs2 > 1 data
    df = df[(df["cs2"] >= 0) & (df["cs2"] <= 1)]

    # Create the 3D figure and subplots
    fig = plt.figure(figsize=(2 * one_figsize_width, 3 * one_figsize_height))
    axs = fig.subplots(nrows=3, ncols=2)

    # Plot the data on the subplots

    other_kwargs = {
        "cbar_size": 0.4,
        "cbar_pad": 0.15,
        "cbar_tick_params_size": FONTSIZE_TICKS,
        "cbar_label_size": FONTSIZE_LABEL,
        "cbar_label_pad": 10,
    }

    x_label = r"$\mu_B \, \mathrm{[MeV]}$"
    y_label = r"$\mu_Q\, \mathrm{[MeV]}$"

    z_label = r"$\Phi$"
    plot_scatter_data(
        axs[0, 0],
        df["muB"],
        df["muQ"],
        df["Phi_order_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$n_B \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[0, 1],
        df["muB"],
        df["muQ"],
        df["nB"] / N_SAT,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$c_s^2$"
    plot_scatter_data(
        axs[1, 0],
        df["muB"],
        df["muQ"],
        df["cs2"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$Y_Q$"
    plot_scatter_data(
        axs[1, 1],
        df["muB"],
        df["muQ"],
        df["Y_Q"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$\chi_2 / \mu_B^2$"
    plot_scatter_data(
        axs[2, 0],
        df["muB"],
        df["muQ"],
        df["chi2_renorm"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$\chi_3 / \mu_B$"
    plot_scatter_data(
        axs[2, 1],
        df["muB"],
        df["muQ"],
        df["chi3_renorm"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("grid"):
        for i in range(3):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Display, save, and close the plot
    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".png")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="png", dpi=300)

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_3D_mean_fields_vs_muS_panel(
    df,
    **kwargs,
):

    # if speed of sound is negative, make it zero
    df["cs2"] = df["cs2"].apply(lambda x: 0 if x < 0 else x)

    # remove cs2 < 0 and cs2 > 1 data
    df = df[(df["cs2"] >= 0) & (df["cs2"] <= 1)]

    # Create the 3D figure and subplots
    fig = plt.figure(figsize=(2 * one_figsize_width, 3 * one_figsize_height))
    axs = fig.subplots(nrows=3, ncols=2)

    # Plot the data on the subplots

    other_kwargs = {
        "cbar_size": 0.4,
        "cbar_pad": 0.15,
        "cbar_tick_params_size": FONTSIZE_TICKS,
        "cbar_label_size": FONTSIZE_LABEL,
        "cbar_label_pad": 10,
    }

    x_label = r"$\mu_B\, \mathrm{[MeV]}$"
    y_label = r"$\mu_S\, \mathrm{[MeV]}$"

    z_label = r"$\sigma/\sigma_0$"
    plot_scatter_data(
        axs[0, 0],
        df["muB"],
        df["muS"],
        df["sigma_mean_field"] / SIGMA_0,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\zeta/\zeta_0$"
    plot_scatter_data(
        axs[0, 1],
        df["muB"],
        df["muS"],
        df["zeta_mean_field"] / ZETA_0,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\delta \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[1, 0],
        df["muB"],
        df["muS"],
        df["delta_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\omega \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[1, 1],
        df["muB"],
        df["muS"],
        df["omega_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\phi \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[2, 0],
        df["muB"],
        df["muS"],
        df["phi_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\rho \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[2, 1],
        df["muB"],
        df["muS"],
        df["rho_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("grid"):
        for i in range(3):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Display, save, and close the plot
    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".png")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="png", dpi=300)

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_3D_densities_vs_muS_panel(
    df,
    **kwargs,
):

    # if speed of sound is negative, make it zero
    df["cs2"] = df["cs2"].apply(lambda x: 0 if x < 0 else x)

    # remove cs2 < 0 and cs2 > 1 data
    df = df[(df["cs2"] >= 0) & (df["cs2"] <= 1)]

    # Create the 3D figure and subplots
    fig = plt.figure(figsize=(2 * one_figsize_width, 3 * one_figsize_height))
    axs = fig.subplots(nrows=3, ncols=2)

    # Plot the data on the subplots

    other_kwargs = {
        "cbar_size": 0.4,
        "cbar_pad": 0.15,
        "cbar_tick_params_size": FONTSIZE_TICKS,
        "cbar_label_size": FONTSIZE_LABEL,
        "cbar_label_pad": 10,
    }

    x_label = r"$\mu_B\, \mathrm{[MeV]}$"
    y_label = r"$\mu_S\, \mathrm{[MeV]}$"

    z_label = r"$\sigma/\sigma_0$"
    plot_scatter_data(
        axs[0, 0],
        df["muB"],
        df["muS"],
        df["sigma_mean_field"] / SIGMA_0,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$\omega \, \mathrm{[MeV]}$"
    plot_scatter_data(
        axs[0, 1],
        df["muB"],
        df["muS"],
        df["omega_mean_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$n_B \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[1, 0],
        df["muB"],
        df["muS"],
        df["nB"] / N_SAT,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$n_{B, octet} \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[1, 1],
        df["muB"],
        df["muS"],
        df["octet_density"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$n_{B, decuplet} \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[2, 0],
        df["muB"],
        df["muS"],
        df["decuplet_density"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )
    z_label = r"$n_{B, quarks} \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[2, 1],
        df["muB"],
        df["muS"],
        df["quark_density"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("grid"):
        for i in range(3):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Display, save, and close the plot
    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".png")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="png", dpi=300)

    if kwargs.get("show"):
        plt.show()

    plt.close()


def create_3D_observables_vs_muS_panel(
    df,
    **kwargs,
):

    # if speed of sound is negative, make it zero
    df["cs2"] = df["cs2"].apply(lambda x: 0 if x < 0 else x)

    # remove cs2 < 0 and cs2 > 1 data
    df = df[(df["cs2"] >= 0) & (df["cs2"] <= 1)]

    # Create the 3D figure and subplots
    fig = plt.figure(figsize=(2 * one_figsize_width, 3 * one_figsize_height))
    axs = fig.subplots(nrows=3, ncols=2)

    # Plot the data on the subplots

    other_kwargs = {
        "cbar_size": 0.4,
        "cbar_pad": 0.15,
        "cbar_tick_params_size": FONTSIZE_TICKS,
        "cbar_label_size": FONTSIZE_LABEL,
        "cbar_label_pad": 10,
    }

    x_label = r"$\mu_B \, \mathrm{[MeV]}$"
    y_label = r"$\mu_S\, \mathrm{[MeV]}$"

    z_label = r"$\Phi$"
    plot_scatter_data(
        axs[0, 0],
        df["muB"],
        df["muS"],
        df["Phi_order_field"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$n_B \, \mathrm{[n_{sat}]}$"
    plot_scatter_data(
        axs[0, 1],
        df["muB"],
        df["muS"],
        df["nB"] / N_SAT,
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$c_s^2$"
    plot_scatter_data(
        axs[1, 0],
        df["muB"],
        df["muS"],
        df["cs2"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$Y_S$"
    plot_scatter_data(
        axs[1, 1],
        df["muB"],
        df["muS"],
        df["Y_S"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$\chi_2 / \mu_B^2$"
    plot_scatter_data(
        axs[2, 0],
        df["muB"],
        df["muS"],
        df["chi2_renorm"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    z_label = r"$\chi_3 / \mu_B$"
    plot_scatter_data(
        axs[2, 1],
        df["muB"],
        df["muS"],
        df["chi3_renorm"],
        x_label,
        y_label,
        z_label,
        kwargs=kwargs.get("heatmap_kwargs"),
        colorbar_kwargs=other_kwargs,
    )

    # add letter to subplots
    for i, letter in enumerate(["a)", "b)", "c)", "d)", "e)", "f)"]):
        ax = axs.flatten()[i]
        ax.text(
            -0.13,
            1.08,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=ax.transAxes,
        )

    if kwargs.get("grid"):
        for i in range(3):
            for j in range(2):
                axs[i, j].grid(True)

    # Add title
    if kwargs.get("title") is not None:
        fig.suptitle(kwargs["title"], fontsize=FONTSIZE_TITLE, y=0.99)

    # Adjust the spacing between subplots
    fig.tight_layout()

    # Display, save, and close the plot
    if kwargs.get("save"):
        name_out = os.path.join(kwargs.get("plot_dir"), kwargs.get("plotname") + ".png")
        print("Saved\t ", name_out)
        plt.savefig(name_out, format="png", dpi=300)

    if kwargs.get("show"):
        plt.show()

    plt.close()


def read_and_convert(
    run_name,
    fortran=False,
    debug=False,
    read_metastable_and_unstable=False,
):
    dataframes = read_data(
        run_name,
        fortran=fortran,
        debug=debug,
        read_metastable_and_unstable=read_metastable_and_unstable,
    )

    filtered_dataframes = filter_data(
        dataframes,
        fortran=fortran,
        debug=debug,
        read_metastable_and_unstable=read_metastable_and_unstable,
    )

    filtered_dataframes = compute_observables(
        filtered_dataframes,
        fortran=fortran,
        debug=debug,
        read_metastable_and_unstable=read_metastable_and_unstable,
    )

    # sort dataframes by muB after grouping it by muS, muQ
    sorted_dataframes = []

    for df in filtered_dataframes:
        grouped_df = df.groupby(["muS", "muQ"], group_keys=False)
        sorted_grouped_df = grouped_df.apply(lambda group: group.sort_values(by="muB"))
        sorted_dataframes.append(sorted_grouped_df)

    return sorted_dataframes


def create_panel_for_one_C_muS_muQ_0(
    dataframes,
    plotname,
    title,
    plot_directory,
    fortran=False,
    debug=False,
    show=False,
    save=False,
):
    # -----------------------------------------------------------------------------------------------------------------#
    # plot
    # -----------------------------------------------------------------------------------------------------------------#

    legends = ["C++ stable", "C++ metastable", "C++ unstable"]
    plot_type = ["plot", "scatter", "scatter"]
    linestyles = ["dashed", "none", "none"]
    linewidths = [LINEWIDTH, None, None]
    markers = ["*", "v", "X"]
    markersizes = [MARKERSIZE, MARKERSIZE, MARKERSIZE]
    colors = [UIUC_ORANGE, "tab:green", "tab:pink"]
    zorder = [4, 2, 1]
    ylim = [
        [-0.01, 1.01],
        [-0.01, 1.01],
        [-1, 121],
        [-66, 1.0],
        [-0.01, 1.01],
        [-10, 2510],
        [-0.01, 0.61],
        [-0.01, 25.0],
    ]

    if debug:
        legends = [*legends, "C++ debug"]
        plot_type = [*plot_type, "scatter"]
        linestyles = [*linestyles, "none"]
        linewidths = [*linewidths, None]
        markers = [*markers, "*"]
        markersizes = [*markersizes, MARKERSIZE]
        colors = [*colors, "tab:purple"]
        zorder = [*zorder, 0]

    if fortran:
        legends = [*legends, "Fortran"]
        plot_type = [*plot_type, "plot"]
        linestyles = [*linestyles, "solid"]
        linewidths = [*linewidths, 1.5 * LINEWIDTH]
        markers = [*markers, "none"]
        markersizes = [*markersizes, None]
        colors = [*colors, UIUC_BLUE]
        zorder = [*zorder, 3]

    cs2_list = [0]

    if fortran:
        cs2_list = [*cs2_list, 3]

    create_2D_mean_fields_and_obs_panel_4_mean_fields(
        dataframes,
        legends,
        plot_type,
        linestyles,
        linewidths,
        markers,
        markersizes,
        colors,
        zorder,
        cs2_list,
        plot_dir=plot_directory,
        plotname=f"2D_{plotname}_muS_0_muQ_0_mean_fields_and_observables_vs_muB_panel",
        show=show,
        save=save,
        grid=True,
        title=f"{title}" + r" $(\mu_S=\mu_Q=0)$",
        ylim=ylim,
    )

    ylim = [[-1.05, 0.05], [-0.01, 0.51], [-0.001, 0.065], [-1.05, 1.05]]

    create_2D_densities_and_sus_panel(
        dataframes,
        legends,
        plot_type,
        linestyles,
        linewidths,
        markers,
        markersizes,
        colors,
        zorder,
        plot_dir=plot_directory,
        plotname=f"2D_{plotname}_muS_0_muQ_0_densities_and_susceptibilities_vs_muB_panel",
        show=show,
        save=save,
        grid=True,
        title=f"{title}" + r"  $(\mu_S=\mu_Q=0)$",
        ylim=ylim,
    )


def create_panel_for_C1_to_C4_muS_muQ_0(
    C1,
    C2,
    C3,
    C4,
    plotname,
    title,
    plot_directory,
    fortran=False,
    debug=False,
    show=False,
    save=False,
):
    dataframes = [C1[0], C2[0], C3[0], C4[0]]

    if fortran:
        dataframes = [*dataframes, C1[-1], C2[-1], C3[-1], C4[-1]]

    if debug:
        dataframes = [*dataframes, C1[-2], C2[-2], C3[-2], C4[-2]]

    legends = [
        "C1 C++",
        "C2 C++",
        "C3 C++",
        "C4 C++",
    ]
    plot_type = ["plot", "plot", "plot", "plot"]

    if fortran:
        linestyles = [
            "solid",
            "solid",
            "solid",
            "solid",
        ]
        linewidths = [
            LINEWIDTH / 2,
            1.2 * LINEWIDTH / 2,
            1.4 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
        ]
        zorder = [4, 3, 2, 1]
    else:
        linestyles = [
            "solid",
            "dashed",
            "dotted",
            "dashdot",
        ]
        linewidths = [
            1.6 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
        ]
        zorder = [1, 2, 3, 4]
    markers = ["none", "none", "none", "none"]
    markersizes = [None, None, None, None]
    colors = [
        UIUC_ORANGE,
        UIUC_BLUE,
        "tab:green",
        "tab:cyan",
    ]
    ylim = [
        [-0.01, 1.01],
        [-0.01, 1.01],
        [-1, 121],
        [-66, 1.0],
        [-0.01, 1.01],
        [-10, 2510],
        [-0.01, 0.61],
        [-0.01, 25.0],
    ]

    if fortran:
        legends = [*legends, "C1 Fortran", "C2 Fortran", "C3 Fortran", "C4 Fortran"]
        plot_type = [*plot_type, "plot", "plot", "plot", "plot"]
        linestyles = [*linestyles, "dotted", "dotted", "dotted", "dotted"]
        linewidths = [
            *linewidths,
            LINEWIDTH,
            1.2 * LINEWIDTH,
            1.4 * LINEWIDTH,
            1.6 * LINEWIDTH,
        ]
        markers = [*markers, "none", "none", "none", "none"]
        markersizes = [*markersizes, None, None, None, None]
        colors = [
            *colors,
            UIUC_ORANGE,
            UIUC_BLUE,
            "tab:green",
            "tab:cyan",
        ]
        zorder = [*zorder, 3, 2, 1, 0]

    if debug:
        legends = [*legends, "C++ debug 1", "C++ debug 2", "C++ debug 3", "C++ debug 4"]
        plot_type = [*plot_type, "scatter", "scatter", "scatter", "scatter"]
        linestyles = [*linestyles, "none", "none", "none", "none"]
        linewidths = [*linewidths, None, None, None, None]
        markers = [*markers, "*", "v", "X", "s"]
        markersizes = [*markersizes, MARKERSIZE, MARKERSIZE, MARKERSIZE, MARKERSIZE]
        colors = [*colors, "tab:purple", "tab:olive", "tab:gray", "tab:brown"]
        zorder = [*zorder, 0, 0, 0, 0]

    cs2_list = [0, 1, 2, 3]

    if fortran:
        cs2_list = [*cs2_list, 4, 5, 6, 7]

    create_2D_mean_fields_and_obs_panel_4_mean_fields(
        dataframes,
        legends,
        plot_type,
        linestyles,
        linewidths,
        markers,
        markersizes,
        colors,
        zorder,
        cs2_list,
        plot_dir=plot_directory,
        plotname=f"2D_{plotname}_muS_0_muQ_0_mean_fields_and_observables_vs_muB_panel",
        show=show,
        save=save,
        grid=True,
        title=f"{title}" + r" $(\mu_S=\mu_Q=0)$",
        ylim=ylim,
    )

    ylim = [[-1.05, 0.05], [-0.01, 0.51], [-0.001, 0.065], [-1.05, 1.05]]

    create_2D_densities_and_sus_panel(
        dataframes,
        legends,
        plot_type,
        linestyles,
        linewidths,
        markers,
        markersizes,
        colors,
        zorder,
        plot_dir=plot_directory,
        plotname=f"2D_{plotname}_muS_0_muQ_0_densities_and_susceptibilities_vs_muB_panel",
        show=show,
        save=save,
        grid=True,
        title=f"{title}" + r" $(\mu_S=\mu_Q=0)$",
        ylim=ylim,
    )


def create_panel_for_C1_to_C4_muS_muQ_Not0(
    muS,
    muQ,
    C1,
    C2,
    C3,
    C4,
    plotname,
    title,
    plot_directory,
    fortran=False,
    debug=False,
    show=False,
    save=False,
):
    dataframes = [C1[0], C2[0], C3[0], C4[0]]

    if fortran:
        dataframes = [*dataframes, C1[-1], C2[-1], C3[-1], C4[-1]]

    if debug:
        dataframes = [*dataframes, C1[-2], C2[-2], C3[-2], C4[-2]]

    legends = [
        "C1 C++",
        "C2 C++",
        "C3 C++",
        "C4 C++",
    ]
    plot_type = ["plot", "plot", "plot", "plot"]
    if fortran:
        linestyles = [
            "solid",
            "solid",
            "solid",
            "solid",
        ]
        linewidths = [
            LINEWIDTH / 2,
            1.2 * LINEWIDTH / 2,
            1.4 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
        ]
        zorder = [4, 3, 2, 1]
    else:
        linestyles = [
            "solid",
            "dashed",
            "dotted",
            "dashdot",
        ]
        linewidths = [
            1.6 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
            1.6 * LINEWIDTH / 2,
        ]
        zorder = [1, 2, 3, 4]
    markers = ["none", "none", "none", "none"]
    markersizes = [None, None, None, None]
    colors = [
        UIUC_ORANGE,
        UIUC_BLUE,
        "tab:green",
        "tab:cyan",
    ]

    if fortran:
        legends = [*legends, "C1 Fortran", "C2 Fortran", "C3 Fortran", "C4 Fortran"]
        plot_type = [*plot_type, "plot", "plot", "plot", "plot"]
        linestyles = [*linestyles, "dotted", "dotted", "dotted", "dotted"]
        linewidths = [
            *linewidths,
            LINEWIDTH,
            1.2 * LINEWIDTH,
            1.4 * LINEWIDTH,
            1.6 * LINEWIDTH,
        ]
        markers = [*markers, "none", "none", "none", "none"]
        markersizes = [*markersizes, None, None, None, None]
        colors = [
            *colors,
            UIUC_ORANGE,
            UIUC_BLUE,
            "tab:green",
            "tab:cyan",
        ]
        zorder = [*zorder, 3, 2, 1, 0]

    if debug:
        legends = [*legends, "C++ debug 1", "C++ debug 2", "C++ debug 3", "C++ debug 4"]
        plot_type = [*plot_type, "scatter", "scatter", "scatter", "scatter"]
        linestyles = [*linestyles, "none", "none", "none", "none"]
        linewidths = [*linewidths, None, None, None, None]
        markers = [*markers, "*", "v", "X", "s"]
        markersizes = [*markersizes, MARKERSIZE, MARKERSIZE, MARKERSIZE, MARKERSIZE]
        colors = [*colors, "tab:purple", "tab:olive", "tab:gray", "tab:brown"]
        zorder = [*zorder, 0, 0, 0, 0]

    ylim = [
        [-0.01, 1.01],
        [-0.01, 1.01],
        [-1, 121],
        [-66, 1.0],
        [-0.01, 1.01],
        [-10, 2510],
        [-0.01, 0.61],
        [-0.01, 25.0],
    ]

    cs2_list = [0, 1, 2, 3]

    if fortran:
        cs2_list = [*cs2_list, 4, 5, 6, 7]

    create_2D_mean_fields_and_obs_panel_4_mean_fields(
        dataframes,
        legends,
        plot_type,
        linestyles,
        linewidths,
        markers,
        markersizes,
        colors,
        zorder,
        cs2_list,
        plot_dir=plot_directory,
        plotname=f"2D_{plotname}_mean_fields_and_observables_vs_muB_panel",
        show=show,
        save=save,
        grid=True,
        title=f"{title}"
        + rf" $(\mu_S={muS} \,"
        + r"\mathrm{MeV}, "
        + rf"\mu_Q={muQ} \,"
        + r"\mathrm{MeV})$",
        ylim=ylim,
    )

    ylim = [
        [-0.01, 1.01],
        [-0.01, 1.01],
        [-1.55, 0.05],
        [-1, 121],
        [-66, 1.0],
        [-5.1, 0.1],
        [-0.01, 1.01],
        [-10, 2510],
        [-0.01, 0.61],
        [-0.01, 25.0],
    ]

    create_2D_mean_fields_and_obs_panel_6_mean_fields(
        dataframes,
        legends,
        plot_type,
        linestyles,
        linewidths,
        markers,
        markersizes,
        colors,
        zorder,
        cs2_list,
        plot_dir=plot_directory,
        plotname=f"2D_{plotname}_mean_fields_and_observables_vs_muB_panel_2",
        show=show,
        save=save,
        grid=True,
        title=f"{title}"
        rf" $(\mu_S={muS} \,"
        + r"\mathrm{MeV}, "
        + rf"\mu_Q={muQ} \,"
        + r"\mathrm{MeV})$",
        ylim=ylim,
    )

    ylim = [[-1.05, 0.05], [-0.01, 0.51], [-0.001, 0.065], [-1.05, 1.05]]

    create_2D_densities_and_sus_panel(
        dataframes,
        legends,
        plot_type,
        linestyles,
        linewidths,
        markers,
        markersizes,
        colors,
        zorder,
        plot_dir=plot_directory,
        plotname=f"2D_{plotname}_densities_and_susceptibilities_vs_muB_panel",
        show=show,
        save=save,
        grid=True,
        title=f"{title}"
        rf" $(\mu_S={muS} \,"
        + r"\mathrm{MeV}, "
        + rf"\mu_Q={muQ} \,"
        + r"\mathrm{MeV})$",
        ylim=ylim,
    )


def create_2D_panels_default(
    plot_directory,
    label_suffix,
    show=False,
    save=True,
    all=False,
    single=False,
    debug=False,
    fortran=False,
):
    C = {}

    for i in range(1, 5):
        print(f"Reading C{i}_default{label_suffix}")
        C[i] = read_and_convert(
            f"C{i}_default{label_suffix}",
            fortran=fortran,
            debug=debug,
            read_metastable_and_unstable=True,
        )

    if single:
        for i in range(3, 5):  # temp only C3, C4
            try:
                create_panel_for_one_C_muS_muQ_0(
                    C[i],
                    f"C{i}"
                    + ("_decuplet" if "decuplet" in label_suffix else "")
                    + ("_nohyper" if "nohyper" in label_suffix else "_hyper")
                    + ("_noquarks" if "noquarks" in label_suffix else "_quarks"),
                    f"C{i} octet"
                    + (" + decuplet" if "decuplet" in label_suffix else "")
                    + (" without" if "noquarks" in label_suffix else " +")
                    + " quarks"
                    + (" without hyperons" if "nohyper" in label_suffix else ""),
                    os.path.join(plot_directory, "muS0_muQ0"),
                    fortran=fortran,
                    debug=debug,
                    show=show,
                    save=save,
                )
            except Exception as e:
                print(f"Error creating panel for C{i}_default{label_suffix}: {e}")

    if all:
        try:
            create_panel_for_C1_to_C4_muS_muQ_0(
                C[1],
                C[2],
                C[3],
                C[4],
                f"all"
                + ("_decuplet" if "decuplet" in label_suffix else "")
                + ("_nohyper" if "nohyper" in label_suffix else "_hyper")
                + ("_noquarks" if "noquarks" in label_suffix else "_quarks"),
                f"C1 - C4 octet"
                + (" + decuplet" if "decuplet" in label_suffix else "")
                + (" without" if "noquarks" in label_suffix else " +")
                + " quarks"
                + (" without hyperons" if "nohyper" in label_suffix else ""),
                os.path.join(plot_directory, "muS0_muQ0"),
                fortran=fortran,
                debug=debug,
                show=show,
                save=save,
            )
        except Exception as e:
            print(f"Error creating panel for C1 to C4 {label_suffix}: {e}")

    # remove vars
    for i in range(1, 5):
        del C[i]


def create_2D_panels_muS_muQ(
    plot_directory,
    muS,
    muQ,
    show=False,
    save=True,
    debug=False,
    fortran=False,
):
    C = {}

    label_suffix = f"muS_{muS}_muQ_{muQ}"

    for i in range(1, 5):
        print(f"Reading C{i}_{label_suffix}")
        C[i] = read_and_convert(
            f"C{i}_{label_suffix}",
            fortran=fortran,
            debug=debug,
            read_metastable_and_unstable=False,
        )

    try:
        create_panel_for_C1_to_C4_muS_muQ_Not0(
            muS,
            muQ,
            C[1],
            C[2],
            C[3],
            C[4],
            f"all_decuplet_hyper_quarks_" + label_suffix,
            f"C1 - C4 octet + decuplet + quarks",
            os.path.join(plot_directory, "muSNot0_muQNot0"),
            fortran=fortran,
            debug=debug,
            show=show,
            save=save,
        )
    except Exception as e:
        print(f"Error creating panel for C1 to C4 {label_suffix}: {e}")

    # remove vars
    for i in range(1, 5):
        del C[i]


def split_df_into_baryons_quarks_vacuum(df):

    df_baryons = df[
        ((df["octet_density"] > 0) & (df["quark_density"] == 0))
        | ((df["decuplet_density"] > 0) & (df["quark_density"] == 0))
    ]

    df_quarks = df[
        (df["quark_density"] > 0)
        & (df["octet_density"] == 0)
        & (df["decuplet_density"] == 0)
    ]

    df_vacuum = df[
        (df["quark_density"] == 0)
        & (df["octet_density"] == 0)
        & (df["decuplet_density"] == 0)
    ]

    return df_baryons, df_quarks, df_vacuum


def plot_3d_scatter(
    df_baryons, df_quarks, df_vacuum, x="muB", y="muS", z="muQ", color_by="pressure"
):

    df_baryons = df_baryons[(df_baryons["cs2"] >= 0) & (df_baryons["cs2"] <= 1)]
    df_quarks = df_quarks[(df_quarks["cs2"] >= 0) & (df_quarks["cs2"] <= 1)]

    fig = plt.figure(figsize=(10, 7))
    ax = fig.add_subplot(111, projection="3d")

    # Plot for stable
    sc1 = ax.scatter(
        df_baryons[x],
        df_baryons[y],
        df_baryons[z],
        c=df_baryons[color_by],
        cmap="Reds",
        label="Baryons (Stable)",
    )
    sc2 = ax.scatter(
        df_quarks[x],
        df_quarks[y],
        df_quarks[z],
        c=df_quarks[color_by],
        cmap="Blues",
        label="Quarks (Stable)",
    )
    sc3 = ax.scatter(
        df_vacuum[x],
        df_vacuum[y],
        df_vacuum[z],
        c=df_vacuum[color_by],
        cmap="Greens",
        label="Vacuum (Stable)",
    )

    ax.set_xlabel(x)
    ax.set_ylabel(y)
    ax.set_zlabel(z)

    ax.legend()
    plt.title(f"3D Scatter Plot of {x}, {y}, {z} Colored by {color_by}")
    plt.show()

    plt.close()


def create_3D_panels_muS_not0_muQ_0(
    plot_directory,
    vector_potential,
    show=False,
    save=True,
    debug=False,
    fortran=False,
):

    heatmap_kwargs = {"cmap": "Spectral", "antialiased": False, "alpha": 0.8}

    muS_Not0_muQ_0 = read_and_convert(
        f"C{vector_potential}_muS_N0_muQ_0",
        fortran=fortran,
        debug=debug,
        read_metastable_and_unstable=False,
    )

    plot_directory = os.path.join(plot_directory, "muSNot0_muQ0")

    print("Creating 3D panels for muS != 0 and muQ = 0")

    create_3D_vs_muS_reduced_panel(
        muS_Not0_muQ_0[0],
        plotname=f"3D_C{vector_potential}_decuplet_hyper_quarks_muS_N0_muQ_0_panel",
        plot_dir=plot_directory,
        show=show,
        save=save,
        grid=True,
        heatmap_kwargs=heatmap_kwargs,
        title=rf"C{vector_potential} octet + decuplet + quarks"
        + r" $(\mu_S \neq 0, \, \mu_Q=0)$",
    )

    del muS_Not0_muQ_0


def create_3D_panels_muS_0_muQ_not0(
    plot_directory,
    vector_potential,
    show=False,
    save=True,
    debug=False,
    fortran=False,
):

    heatmap_kwargs = {"cmap": "Spectral", "antialiased": False, "alpha": 0.8}

    muS_0_muQ_Not0 = read_and_convert(
        f"C{vector_potential}_muS_0_muQ_N0",
        fortran=fortran,
        debug=debug,
        read_metastable_and_unstable=False,
    )

    plot_directory = os.path.join(plot_directory, "muS0_muQNot0")

    create_3D_vs_muQ_reduced_panel(
        muS_0_muQ_Not0[0],
        plotname=f"3D_C{vector_potential}_decuplet_hyper_quarks_muS_0_muQ_N0_panel",
        plot_dir=plot_directory,
        show=show,
        save=save,
        grid=True,
        heatmap_kwargs=heatmap_kwargs,
        title=rf"C{vector_potential} octet + decuplet + quarks"
        + r" $(\mu_S = 0, \, \mu_Q \neq 0)$",
    )

    del muS_0_muQ_Not0


def create_3D_panels_muS_n50_muQ_not0(
    plot_directory,
    vector_potential,
    show=False,
    save=True,
    debug=False,
    fortran=False,
):

    heatmap_kwargs = {"cmap": "Spectral", "antialiased": False, "alpha": 0.8}

    muS_n50_muQ_Not0 = read_and_convert(
        f"C{vector_potential}_muS_-50_muQ_N0",
        fortran=fortran,
        debug=debug,
        read_metastable_and_unstable=False,
    )

    plot_directory = os.path.join(plot_directory, "muSNot0_muQNot0")

    create_3D_vs_muQ_reduced_panel(
        muS_n50_muQ_Not0[0],
        plotname=f"3D_C{vector_potential}_decuplet_hyper_quarks_muS_-50_muQ_N0_panel",
        plot_dir=plot_directory,
        show=show,
        save=save,
        grid=True,
        heatmap_kwargs=heatmap_kwargs,
        title=rf"C{vector_potential} octet + decuplet + quarks"
        + r" $(\mu_S = -50 \, \mathrm{MeV}, \, \mu_Q \neq 0)$",
    )

    del muS_n50_muQ_Not0


def create_3D_panels_muS_not0_muQ_n200(
    plot_directory,
    vector_potential,
    show=False,
    save=True,
    debug=False,
    fortran=False,
):

    heatmap_kwargs = {"cmap": "Spectral", "antialiased": False, "alpha": 0.8}

    muS_Not0_muQ_n200 = read_and_convert(
        f"C{vector_potential}_muS_N0_muQ_-200",
        fortran=fortran,
        debug=debug,
        read_metastable_and_unstable=False,
    )

    plot_directory = os.path.join(plot_directory, "muSNot0_muQNot0")

    create_3D_vs_muS_reduced_panel(
        muS_Not0_muQ_n200[0],
        plotname=f"3D_C{vector_potential}_decuplet_hyper_quarks_muS_N0_muQ_-200_panel",
        plot_dir=plot_directory,
        show=show,
        save=save,
        grid=True,
        heatmap_kwargs=heatmap_kwargs,
        title=rf"C{vector_potential} octet + decuplet + quarks"
        + r" $(\mu_S \neq 0, \, \mu_Q = -200 \, \mathrm{MeV})$",
    )

    del muS_Not0_muQ_n200

def use_fortran():
    
    # Define the directory to search in
    folder_to_search = os.path.join("..", "output")

    # Search for files with the .fortran_EoS extension
    fortran_files = glob.glob(os.path.join(folder_to_search, '*.fortran_EoS'))

    # Set the fortran flag depending on whether any such files are found
    if len(fortran_files) > 0:
        return True
    else:
        return False

def main():
    args = parse_arguments()
    show, save = args.show, args.save
    
    fortran = use_fortran()

    plot_directory = create_output_directories()

    # -----------------------------------------------------------------------------------------------------------------#
    # 2D muS=0 muQ=0
    # -----------------------------------------------------------------------------------------------------------------#

    # muS=muQ=0 octet decuplet off quarks on Phi order on
    create_2D_panels_default(
        plot_directory,
        "",
        show=show,
        save=save,
        single=True,
        all=True,
        debug=False,
        fortran=fortran,
    )

    # # muS=muQ=0 octet decuplet on quarks on Phi order on # unphysical
    # create_2D_panels_default(
    #     plot_directory,
    #     "_decuplet",
    #     show=show,
    #     save=save,
    #     single=True,
    #     all=True,
    #     debug=False,
    #     fortran=fortran,
    # )

    # # muS=muQ=0 octet decuplet off quarks off Phi order on
    # create_2D_panels_default(
    #     plot_directory,
    #     "_noquarks",
    #     show=show,
    #     save=save,
    #     single=True,
    #     all=True,
    #     debug=False,
    #     fortran=fortran,
    # )

    # muS=muQ=0 octet decuplet on quarks off Phi order on
    create_2D_panels_default(
        plot_directory,
        "_decuplet_noquarks",
        show=show,
        save=save,
        all=True,
        debug=False,
        fortran=False,
    )

    # -----------------------------------------------------------------------------------------------------------------#
    # 2D muS!=0 muQ!=0
    # -----------------------------------------------------------------------------------------------------------------#

    create_2D_panels_muS_muQ(
        plot_directory,
        0,
        0,
        show=show,
        save=save,
        debug=False,
        fortran=False,
    )
    # create_2D_panels_muS_muQ(
    #     plot_directory,
    #     0,
    #     -50,
    #     show=show,
    #     save=save,
    #     debug=False,
    #     fortran=fortran,
    # )
    # create_2D_panels_muS_muQ(
    #     plot_directory,
    #     -50,
    #     0,
    #     show=show,
    #     save=save,
    #     debug=False,
    #     fortran=fortran,
    # )
    create_2D_panels_muS_muQ(
        plot_directory,
        -50,
        -50,
        show=show,
        save=save,
        debug=False,
        fortran=False,
    )

    # -----------------------------------------------------------------------------------------------------------------#
    # 3D muS!=0, muQ=0
    # -----------------------------------------------------------------------------------------------------------------#

    # create_3D_panels_muS_not0_muQ_0(plot_directory, 1, show=show, save=save)
    # create_3D_panels_muS_not0_muQ_0(plot_directory, 2, show=show, save=save)
    create_3D_panels_muS_not0_muQ_0(plot_directory, 3, show=show, save=save)
    create_3D_panels_muS_not0_muQ_0(plot_directory, 4, show=show, save=save)

    # -----------------------------------------------------------------------------------------------------------------#
    # 3D muS=0, muQ!=0
    # -----------------------------------------------------------------------------------------------------------------#

    # create_3D_panels_muS_0_muQ_not0(plot_directory, 1, show=show, save=save)
    # create_3D_panels_muS_0_muQ_not0(plot_directory, 2, show=show, save=save)
    create_3D_panels_muS_0_muQ_not0(plot_directory, 3, show=show, save=save)
    create_3D_panels_muS_0_muQ_not0(plot_directory, 4, show=show, save=save)

    # -----------------------------------------------------------------------------------------------------------------#
    # 3D muS!=0, muQ !=0
    # -----------------------------------------------------------------------------------------------------------------#

    # create_3D_panels_muS_n50_muQ_not0(plot_directory, 1, show=show, save=save)
    # create_3D_panels_muS_n50_muQ_not0(plot_directory, 2, show=show, save=save)
    create_3D_panels_muS_n50_muQ_not0(plot_directory, 3, show=show, save=save)
    create_3D_panels_muS_n50_muQ_not0(plot_directory, 4, show=show, save=save)

    # create_3D_panels_muS_not0_muQ_n200(plot_directory, 1, show=show, save=save)
    # create_3D_panels_muS_not0_muQ_n200(plot_directory, 2, show=show, save=save)
    create_3D_panels_muS_not0_muQ_n200(plot_directory, 3, show=show, save=save)
    create_3D_panels_muS_not0_muQ_n200(plot_directory, 4, show=show, save=save)


if __name__ == "__main__":
    print("\nStarting execution of plot_paper.py...")
    main()
    print("\nExecution of plot_paper.py completed.")
