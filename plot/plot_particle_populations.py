#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os

import matplotlib.pyplot as plt
import pandas as pd

from plot_paper import filter_fortan_unphysical_data

"""
file: plot_particle_properties.py
author: Nikolas Cruz Camacho <cnc6@illinois.edu>
purpose: plot the particle_properties output files from CMF
"""

# Latex notation
plt.rc("text", usetex=True)
plt.rc("font", family="serif")

# Calculate figsize for 16:9 aspect ratio on letter size paper
letter_size = (8.5, 11)
aspect_ratio = 16 / 9
one_figsize_width = letter_size[0]
one_figsize_height = 1.2 * one_figsize_width / aspect_ratio

PARTICLE_PROPERTIES_VARIABLES = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "nS",
    "nQ",
    "energy_density",
    "pressure",
    "entropy_density",
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
    "vector_density_without_Phi_order ",
    "quark_density",
    "octet_density",
    "decuplet_density",
    "massq_1",
    "massq_2",
    "massq_3",
    "masseffq_1",
    "masseffq_2",
    "masseffq_3",
    "cpq_1",
    "cpq_2",
    "cpq_3",
    "cpeffq_1",
    "cpeffq_2",
    "cpeffq_3",
    "baryon_densityq_1",
    "baryon_densityq_2",
    "baryon_densityq_3",
    "potentialq_1",
    "potentialq_2",
    "potentialq_3",
    "mass_1",
    "mass_2",
    "mass_3",
    "mass_4",
    "mass_5",
    "mass_6",
    "mass_7",
    "mass_8",
    "masseff_1",
    "masseff_2",
    "masseff_3",
    "masseff_4",
    "masseff_5",
    "masseff_6",
    "masseff_7",
    "masseff_8",
    "cp_1",
    "cp_2",
    "cp_3",
    "cp_4",
    "cp_5",
    "cp_6",
    "cp_7",
    "cp_8",
    "cpeff_1",
    "cpeff_2",
    "cpeff_3",
    "cpeff_4",
    "cpeff_5",
    "cpeff_6",
    "cpeff_7",
    "cpeff_8",
    "baryon_density_1",
    "baryon_density_2",
    "baryon_density_3",
    "baryon_density_4",
    "baryon_density_5",
    "baryon_density_6",
    "baryon_density_7",
    "baryon_density_8",
    "potential_1",
    "potential_2",
    "potential_3",
    "potential_4",
    "potential_5",
    "potential_6",
    "potential_7",
    "potential_8",
    "massd_1",
    "massd_2",
    "massd_3",
    "massd_4",
    "massd_5",
    "massd_6",
    "massd_7",
    "massd_8",
    "massd_9",
    "massd_10",
    "masseffd_1",
    "masseffd_2",
    "masseffd_3",
    "masseffd_4",
    "masseffd_5",
    "masseffd_6",
    "masseffd_7",
    "masseffd_8",
    "masseffd_9",
    "masseffd_10",
    "cpd_1",
    "cpd_2",
    "cpd_3",
    "cpd_4",
    "cpd_5",
    "cpd_6",
    "cpd_7",
    "cpd_8",
    "cpd_9",
    "cpd_10",
    "cpeffd_1",
    "cpeffd_2",
    "cpeffd_3",
    "cpeffd_4",
    "cpeffd_5",
    "cpeffd_6",
    "cpeffd_7",
    "cpeffd_8",
    "cpeffd_9",
    "cpeffd_10",
    "baryon_densityd_1",
    "baryon_densityd_2",
    "baryon_densityd_3",
    "baryon_densityd_4",
    "baryon_densityd_5",
    "baryon_densityd_6",
    "baryon_densityd_7",
    "baryon_densityd_8",
    "baryon_densityd_9",
    "baryon_densityd_10",
    "potentiald_1",
    "potentiald_2",
    "potentiald_3",
    "potentiald_4",
    "potentiald_5",
    "potentiald_6",
    "potentiald_7",
    "potentiald_8",
    "potentiald_9",
    "potentiald_10",
]

PARTICLE_PROPERTIES_VARIABLES_FORTRAN = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "Y_S",
    "Y_Q",
    "energy_density",
    "pressure",
    "entropy_density",
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
    "vector_density_without_Phi_order ",
    "quark_density",
    "octet_density",
    "decuplet_density",
    "massq_1",
    "massq_2",
    "massq_3",
    "masseffq_1",
    "masseffq_2",
    "masseffq_3",
    "cpq_1",
    "cpq_2",
    "cpq_3",
    "cpeffq_1",
    "cpeffq_2",
    "cpeffq_3",
    "baryon_densityq_1",
    "baryon_densityq_2",
    "baryon_densityq_3",
    "potentialq_1",
    "potentialq_2",
    "potentialq_3",
    "mass_1",
    "mass_2",
    "mass_3",
    "mass_4",
    "mass_5",
    "mass_6",
    "mass_7",
    "mass_8",
    "masseff_1",
    "masseff_2",
    "masseff_3",
    "masseff_4",
    "masseff_5",
    "masseff_6",
    "masseff_7",
    "masseff_8",
    "cp_1",
    "cp_2",
    "cp_3",
    "cp_4",
    "cp_5",
    "cp_6",
    "cp_7",
    "cp_8",
    "cpeff_1",
    "cpeff_2",
    "cpeff_3",
    "cpeff_4",
    "cpeff_5",
    "cpeff_6",
    "cpeff_7",
    "cpeff_8",
    "baryon_density_1",
    "baryon_density_2",
    "baryon_density_3",
    "baryon_density_4",
    "baryon_density_5",
    "baryon_density_6",
    "baryon_density_7",
    "baryon_density_8",
    "potential_1",
    "potential_2",
    "potential_3",
    "potential_4",
    "potential_5",
    "potential_6",
    "potential_7",
    "potential_8",
    "massd_1",
    "massd_2",
    "massd_3",
    "massd_4",
    "massd_5",
    "massd_6",
    "massd_7",
    "massd_8",
    "massd_9",
    "massd_10",
    "masseffd_1",
    "masseffd_2",
    "masseffd_3",
    "masseffd_4",
    "masseffd_5",
    "masseffd_6",
    "masseffd_7",
    "masseffd_8",
    "masseffd_9",
    "masseffd_10",
    "cpd_1",
    "cpd_2",
    "cpd_3",
    "cpd_4",
    "cpd_5",
    "cpd_6",
    "cpd_7",
    "cpd_8",
    "cpd_9",
    "cpd_10",
    "cpeffd_1",
    "cpeffd_2",
    "cpeffd_3",
    "cpeffd_4",
    "cpeffd_5",
    "cpeffd_6",
    "cpeffd_7",
    "cpeffd_8",
    "cpeffd_9",
    "cpeffd_10",
    "baryon_densityd_1",
    "baryon_densityd_2",
    "baryon_densityd_3",
    "baryon_densityd_4",
    "baryon_densityd_5",
    "baryon_densityd_6",
    "baryon_densityd_7",
    "baryon_densityd_8",
    "baryon_densityd_9",
    "baryon_densityd_10",
    "potentiald_1",
    "potentiald_2",
    "potentiald_3",
    "potentiald_4",
    "potentiald_5",
    "potentiald_6",
    "potentiald_7",
    "potentiald_8",
    "potentiald_9",
    "potentiald_10",
    "bah",
]

SEPARATOR = ","

N_SAT = 0.16  # [fm^-3]
HBARC = 197.3269804  # [MeV fm]
SIGMA_0 = -93.300003  # [MeV]
ZETA_0 = -106.560990  # [MeV]

LINEWIDTH = 5.0
MARKERSIZE = 60.0

FONTSIZE_TITLE = 28
FONTSIZE_LEGEND = 14
FONTSIZE_TICKS = 26
FONTSIZE_LABEL = 26
FONTSIZE_LETTER = 26

WIDTH_SPACE = 0.75
HEIGHT_SPACE = 0.75
PAD_TITLE = 15
PAD_LABEL = 10

xlim_muB = [900, 1950]  # [Mev]


def plot_particle_populations_2D_line(
    df,
    plot_directory="./plots_paper/muS0_muQ0/",
    plotname=None,
    show=True,
    save=False,
    xlim=xlim_muB,
    ylim=None,
    alpha=0.85,
    title=None,
    letter=None,
):
    # Create the figure and subplots
    fig = plt.figure(figsize=(1.2 * one_figsize_width, 1.2 * one_figsize_height))

    # Plot the data on the subplots
    # if df["baryon_densityq_1"] is none then the plot will not be shown

    if df["baryon_densityq_1"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityq_1"],
            c="red",
            linestyle="solid",
            label="up quark",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityq_2"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityq_2"],
            c="green",
            linestyle="dashed",
            label="down quark",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityq_3"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityq_3"],
            c="blue",
            linestyle="dotted",
            label="strange quark",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_1"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_1"],
            c="#1f77b4",
            linestyle="solid",
            label="$p$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_2"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_2"],
            c="#6fa6f4",
            linestyle="dashed",
            label="$n$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_3"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_3"],
            c="#f27473",
            linestyle="solid",
            label="$\Lambda$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_4"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_4"],
            c="#2ca02c",
            linestyle="solid",
            label="$\Sigma^+$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_5"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_5"],
            c="#50c050",
            linestyle="dashed",
            label="$\Sigma^0$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_6"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_6"],
            c="#75d775",
            linestyle="dotted",
            label="$\Sigma^-$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_7"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_7"],
            c="#9467bd",
            linestyle="solid",
            label="$\Xi^0$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_density_8"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_density_8"],
            c="#c8a1e3",
            linestyle="dashed",
            label="$\Xi^-$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_1"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_1"],
            c="#ff7f0e",
            linestyle="solid",
            label="$\Delta^{++}$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_2"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_2"],
            c="#ff993d",
            linestyle="dashed",
            label="$\Delta^+$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_3"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_3"],
            c="#ffb273",
            linestyle="dotted",
            label="$\Delta^0$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_4"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_4"],
            c="#ffc9a0",
            linestyle="dashdot",
            label="$\Delta^-$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_5"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_5"],
            c="#17becf",
            linestyle="solid",
            label="$\Sigma^{*+}$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_6"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_6"],
            c="#4ad3e8",
            linestyle="dashed",
            label="$\Sigma^{*0}$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_7"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_7"],
            c="#7ce7f0",
            linestyle="dotted",
            label="$\Sigma^{*-}$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_8"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_8"],
            c="#7f7f7f",
            linestyle="solid",
            label="$\Xi^{*0}$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_9"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_9"],
            c="#c8c8c8",
            linestyle="dashed",
            label="$\Xi^{*-}$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )
    if df["baryon_densityd_10"].ge(1e-12).any():
        plt.plot(
            df["muB"],
            df["baryon_densityd_10"],
            c="#ff69b4",
            linestyle="solid",
            label="$\Omega$",
            alpha=alpha,
            linewidth=LINEWIDTH,
        )

    plt.xlabel(r"$\mu_B \, \mathrm{[MeV]}$", fontsize=FONTSIZE_LABEL)
    plt.ylabel(r"$n_{B, i} \, \mathrm{[fm^{-3}]}$", fontsize=FONTSIZE_LABEL)

    plt.legend(loc="upper left", fontsize=FONTSIZE_LEGEND)

    plt.tick_params(axis="both", labelsize=FONTSIZE_TICKS)

    if xlim is not None:
        plt.xlim(xlim)

    if ylim is not None:
        plt.ylim(ylim)

    if title is not None:
        plt.title(title, fontsize=FONTSIZE_TITLE)

    if letter is not None:
        plt.text(
            -0.1,
            1.05,
            letter,
            fontsize=FONTSIZE_LETTER,
            ha="center",
            va="center",
            transform=plt.gca().transAxes,
        )

    # add tittle padding
    plt.title(title, fontsize=FONTSIZE_TITLE, pad=PAD_TITLE)

    plt.grid()

    # Adjust the spacing between subplots
    plt.tight_layout()

    if save:
        filename = os.path.join(plot_directory, plotname)
        print("Saved\t ", filename)
        plt.savefig(filename, format="pdf")

    if show:
        plt.show()

    plt.close()


def plot_particle_populations_2D_scatter(
    df,
    plot_directory="./plots_paper/muS0_muQ0/",
    plotname=None,
    show=True,
    save=False,
    xlim=xlim_muB,
    ylim=None,
    alpha=0.85,
    title=None,
):
    # Create the figure and subplots
    fig = plt.figure(figsize=(1.2 * one_figsize_width, 1.2 * one_figsize_height))

    # Plot the data on the subplots
    plt.scatter(
        df["muB"],
        df["baryon_densityq_1"],
        c="blue",
        marker="^",
        s=MARKERSIZE,
        label="Up quark",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityq_2"],
        c="red",
        marker="v",
        s=MARKERSIZE,
        label="Down quark",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityq_3"],
        c="green",
        marker="x",
        s=MARKERSIZE,
        label="Strange quark",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_density_1"],
        c="tab:red",
        marker="o",
        s=MARKERSIZE,
        label="$p$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_density_2"],
        c="tab:blue",
        marker="x",
        s=1 / 2 * MARKERSIZE,
        label="$n$",
        alpha=alpha,
    )

    plt.scatter(
        df["muB"],
        df["baryon_density_3"],
        c="tab:pink",
        marker="D",
        s=MARKERSIZE,
        label="$\Lambda$",
        alpha=alpha,
    )

    plt.scatter(
        df["muB"],
        df["baryon_density_4"],
        c="tab:brown",
        marker="s",
        s=MARKERSIZE,
        label="$\Sigma^-$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_density_5"],
        c="tab:gray",
        marker="s",
        s=2 / 3 * MARKERSIZE,
        label="$\Sigma^0$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_density_6"],
        c="tab:cyan",
        marker="s",
        s=1 / 3 * MARKERSIZE,
        label="$\Sigma^+$",
        alpha=alpha,
    )

    plt.scatter(
        df["muB"],
        df["baryon_density_7"],
        c="tab:olive",
        marker="P",
        s=MARKERSIZE,
        label="$\Xi^-$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_density_8"],
        c="tab:orange",
        marker="P",
        s=1 / 2 * MARKERSIZE,
        label="$\Xi^0$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_1"],
        c="tab:blue",
        marker="p",
        s=MARKERSIZE,
        label="$\Delta^{++}$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_2"],
        c="tab:green",
        marker="p",
        s=3 / 4 * MARKERSIZE,
        label="$\Delta^+$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_3"],
        c="tab:red",
        marker="p",
        s=1 / 2 * MARKERSIZE,
        label="$\Delta^0$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_4"],
        c="tab:purple",
        marker="p",
        s=1 / 4 * MARKERSIZE,
        label="$\Delta^-$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_5"],
        c="tab:pink",
        marker="*",
        s=MARKERSIZE,
        label="$\Sigma^{*+}$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_6"],
        c="tab:brown",
        marker="*",
        s=2 / 3 * MARKERSIZE,
        label="$\Sigma^{*0}$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_7"],
        c="tab:gray",
        marker="*",
        s=1 / 3 * MARKERSIZE,
        label="$\Sigma^{*-}$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_8"],
        c="tab:cyan",
        marker="X",
        s=MARKERSIZE,
        label="$\Xi^{*0}$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_9"],
        c="tab:olive",
        marker="X",
        s=1 / 2 * MARKERSIZE,
        label="$\Xi^{*-}$",
        alpha=alpha,
    )
    plt.scatter(
        df["muB"],
        df["baryon_densityd_10"],
        c="tab:orange",
        marker="8",
        s=MARKERSIZE,
        label="$\Omega$",
        alpha=alpha,
    )

    plt.xlabel(r"$\mu_B \, \mathrm{[MeV]}$", fontsize=FONTSIZE_LABEL)
    plt.ylabel(r"$n_{B, i} \, \mathrm{[fm^{-3}]}$", fontsize=FONTSIZE_LABEL)

    plt.legend(loc="best", fontsize=FONTSIZE_LEGEND)

    plt.tick_params(axis="both", labelsize=FONTSIZE_TICKS)

    if xlim is not None:
        plt.xlim(xlim)

    if ylim is not None:
        plt.ylim(ylim)

    # if title is not None:
    #     plt.title(title, fontsize=FONTSIZE_TITLE)

    plt.grid()

    # Adjust the spacing between subplots
    plt.tight_layout()

    if save:
        filename = os.path.join(plot_directory, plotname)
        print("Saved\t ", filename)
        plt.savefig(filename, format="pdf")

    if show:
        plt.show()

    plt.close()


def read_data(run_name, filename, names=PARTICLE_PROPERTIES_VARIABLES):
    # -----------------------------------------------------------------------------------------------------------------#
    # read data
    # -----------------------------------------------------------------------------------------------------------------#

    filename = os.path.join("../output", run_name, filename)

    # check if file exists
    if not os.path.isfile(filename):
        print("File does not exist: ", filename)
        return None

    print("Reading data from ", filename)
    df = pd.read_csv(
        filename,
        delimiter=SEPARATOR,
        names=names,
    )

    return df


def preprocess_stable_data(df):

    # -----------------------------------------------------------------------------------------------------------------#
    # check test data
    # -----------------------------------------------------------------------------------------------------------------#

    # print(df.head())
    # print(df.describe())

    # -----------------------------------------------------------------------------------------------------------------#
    # unit conversion for nB
    # -----------------------------------------------------------------------------------------------------------------#

    df["nB"] = df["nB"] / N_SAT

    df = df.sort_values("pressure")
    # df = df.sort_values("muB")

    return df


def preprocess_fortran_data(df, df_stable):

    # -----------------------------------------------------------------------------------------------------------------#
    # check test data
    # -----------------------------------------------------------------------------------------------------------------#

    # print(df.head())
    # print(df.describe())

    # drop bah column
    df = df.drop(columns=["bah"])

    # -----------------------------------------------------------------------------------------------------------------#
    # unit conversion for nB
    # -----------------------------------------------------------------------------------------------------------------#

    df["nB"] = df["nB"] / N_SAT

    df = df.sort_values("pressure")
    # df = df.sort_values("muB")

    # -----------------------------------------------------------------------------------------------------------------#
    # filter data
    # -----------------------------------------------------------------------------------------------------------------#

    mean_fields = [
        "sigma_mean_field",
        "zeta_mean_field",
        "delta_mean_field",
        "omega_mean_field",
        "phi_mean_field",
        "rho_mean_field",
        "Phi_order_field",
    ]
    df = filter_fortan_unphysical_data(df, df_stable, mean_fields, tolerance=0.1)

    return df


def plot_data(df, plotname, plot_directory, title, show, save, letter=None):

    # -----------------------------------------------------------------------------------------------------------------#
    # plot
    # -----------------------------------------------------------------------------------------------------------------#

    plot_particle_populations_2D_line(
        df,
        plotname=plotname,
        title=title,
        show=show,
        save=save,
        plot_directory=plot_directory,
        alpha=0.85,
        letter=letter,
    )


if __name__ == "__main__":
    # -----------------------------------------------------------------------------------------------------------------#
    # parse arguments
    # -----------------------------------------------------------------------------------------------------------------#

    # Create the parser
    parser = argparse.ArgumentParser(description="parser with only one argument")

    # Add a positional argument (optional)
    parser.add_argument(
        "--run_name",
        default="C4_default",
        help="Input folder to process. If none is provided, C4 default folder will be used.",
    )
    parser.add_argument(
        "--show", action="store_true", default=False, help="Show the plots?"
    )
    parser.add_argument(
        "--save", action="store_true", default=True, help="Save the plots?"
    )
    parser.add_argument(
        "--fortran", action="store_true", default=False, help="plot fortran data?"
    )
    parser.add_argument(
        "--letter", default=None, help="Letter to put on the plot", type=str
    )

    # Parse the arguments from the command line
    args = parser.parse_args()

    run_name = args.run_name
    show = args.show
    save = args.save
    fortran = args.fortran
    letter = args.letter

    # -----------------------------------------------------------------------------------------------------------------#
    # define parameters
    # -----------------------------------------------------------------------------------------------------------------#

    if save:
        plot_directory = os.path.join(
            "./plots_paper/populations/", run_name.split("_")[0]
        )

        os.makedirs(plot_directory, exist_ok=True)

    # -----------------------------------------------------------------------------------------------------------------#
    # read and plot call
    # -----------------------------------------------------------------------------------------------------------------#

    df = read_data(run_name, "CMF_output_particle_properties_stable.csv")
    df = preprocess_stable_data(df)
    df[df < 1e-12] = None  # set to None values that are too small
    plot_data(
        df,
        f"2D_{run_name}_populations_vs_muB.pdf",
        plot_directory,
        title=fr" $C_{run_name.split('_')[0][-1]}$ octet"
        + (" + decuplet" if "decuplet" in run_name else "")
        + (" without" if "_noquarks" in run_name else " +")
        + " quarks"
        + (" without hyperons" if "_nohyper" in run_name else "")
        + r" populations vs $\mu_B$",
        show=show,
        save=save,
        letter=letter,
    )

    if fortran:
        try:
            df = read_data(run_name, "CMF_output_particle_properties_stable.csv")
            df_fortran = read_data(
                ".",
                f"{run_name}.fortran_PP",
                names=PARTICLE_PROPERTIES_VARIABLES_FORTRAN,
            )

            print(df.columns)
            print(df_fortran.head())

            df = preprocess_stable_data(df)
            df_fortran = preprocess_fortran_data(df_fortran, df)

            df_fortran[
                df_fortran < 1e-12
            ] = None  # set to None values that are too small

            plot_data(
                df_fortran,
                f"2D_{run_name}_populations_vs_muB_fortran.pdf",
                plot_directory,
                title=fr" $C_{run_name.split('_')[0][-1]}$ octet"
                + (" + decuplet" if "decuplet" in run_name else "")
                + (" without" if "_noquarks" in run_name else " +")
                + " quarks"
                + (" without hyperons" if "_nohyper" in run_name else "")
                + r" populations vs $\mu_B (fortran data)",
                show=show,
                save=save,
                letter=letter,
            )
        except Exception as e:
            print("Fortran data not found")
            print(e)
            pass
