#!/bin/bash

python plot_particle_populations.py --run_name=C1_default
python plot_particle_populations.py --run_name=C2_default
python plot_particle_populations.py --run_name=C3_default
python plot_particle_populations.py --run_name=C4_default

# python plot_particle_populations.py --run_name=C1_default_nohyper
# python plot_particle_populations.py --run_name=C2_default_nohyper
# python plot_particle_populations.py --run_name=C3_default_nohyper
# python plot_particle_populations.py --run_name=C4_default_nohyper

# unphysical
# python plot_particle_populations.py --run_name=C1_default_decuplet
# python plot_particle_populations.py --run_name=C2_default_decuplet
# python plot_particle_populations.py --run_name=C3_default_decuplet
# python plot_particle_populations.py --run_name=C4_default_decuplet

python plot_particle_populations.py --run_name=C1_default_decuplet_noquarks --letter="a)"
python plot_particle_populations.py --run_name=C2_default_decuplet_noquarks --letter="b)"
python plot_particle_populations.py --run_name=C3_default_decuplet_noquarks --letter="c)"
python plot_particle_populations.py --run_name=C4_default_decuplet_noquarks --letter="d)"

# python plot_particle_populations.py --run_name=C1_default_decuplet_nohyper
# python plot_particle_populations.py --run_name=C2_default_decuplet_nohyper
# python plot_particle_populations.py --run_name=C3_default_decuplet_nohyper
# python plot_particle_populations.py --run_name=C4_default_decuplet_nohyper

# python plot_particle_populations.py --run_name=C1_muS_0_muQ_-50
# python plot_particle_populations.py --run_name=C2_muS_0_muQ_-50
# python plot_particle_populations.py --run_name=C3_muS_0_muQ_-50
# python plot_particle_populations.py --run_name=C4_muS_0_muQ_-50

# python plot_particle_populations.py --run_name=C1_muS_-50_muQ_0
# python plot_particle_populations.py --run_name=C2_muS_-50_muQ_0
# python plot_particle_populations.py --run_name=C3_muS_-50_muQ_0
# python plot_particle_populations.py --run_name=C4_muS_-50_muQ_0

# python plot_particle_populations.py --run_name=C1_muS_-50_muQ_-50
# python plot_particle_populations.py --run_name=C2_muS_-50_muQ_-50
python plot_particle_populations.py --run_name=C3_muS_-50_muQ_-50
python plot_particle_populations.py --run_name=C4_muS_-50_muQ_-50