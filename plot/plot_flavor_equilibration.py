#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import yaml
import sys
import os
import numpy as np

import pandas as pd
import matplotlib.pyplot as plt

"""
file: plot_flavor_equilibration.py
author: Nikolas Cruz Camacho <cnc6@illinois.edu>
purpose: plot the flavor equilibration module output file from CMF
"""

N_SAT = 0.16  # [fm^-3]
HBARC = 197.3269804  # [MeV fm]

SEPARATOR = ","

FLAVOR_EQUILIBRATION_VARIABLES = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "nS",
    "nQ",
    "energy",
    "pressure",
    "entropy",
    "proton_effective_mass",
    "neutron_effective_mass",
    "proton_chemical_potential",
    "neutron_chemical_potential",
    "proton_vector_density",
    "neutron_vector_density",
    "proton_potential",
    "neutron_potential",
]


def plot2D_flavor(x="muB", plotname=None, show=True, save=False):
    # Create the figure and subplots
    fig, axs = plt.subplots(nrows=3, ncols=3, figsize=(8 * 1.75, 4.5 * 1.75))

    # Plot the data on the subplots
    axs[0, 0].scatter(
        df[x], df["pressure"], c="darkolivegreen", marker=".", s=10, label="C++"
    )
    if x == "muB":
        axs[0, 0].set_title(
            r"$p \, \mathrm{[MeV \, fm^{-3}]} \, vs \, \mu_B\, \mathrm{[MeV]}$ "
        )
        axs[0, 0].set_xlim([900, 1800])
    elif x == "nB":
        axs[0, 0].set_title(
            r"$p \, \mathrm{[MeV \, fm^{-3}]} \, vs \, n_B \, \mathrm{[n_{sat}]}$ "
        )

    axs[0, 0].legend(loc="best", fontsize=8)

    axs[0, 1].scatter(
        df[x], df["energy"], c="darkolivegreen", marker=".", s=10, label="C++"
    )
    if x == "muB":
        axs[0, 1].set_title(
            r"$\epsilon \, \mathrm{[MeV \, fm^{-3}]} \, vs \, \mu_B\, \mathrm{[MeV]}$ "
        )
        axs[0, 1].set_xlim([900, 1800])
    elif x == "nB":
        axs[0, 1].set_title(
            r"$\epsilon \, \mathrm{[MeV \, fm^{-3}]} \, vs \, n_B \, \mathrm{[n_{sat}]}$ "
        )

    axs[0, 1].legend(loc="best", fontsize=8)

    axs[0, 2].scatter(
        df[x], df["nB"], c="darkolivegreen", marker=".", s=10, label="C++"
    )
    if x == "muB":
        axs[0, 2].set_title(
            r"$n_B \, \mathrm{[n_{sat}]} vs \, \mu_B\, \mathrm{[MeV]}$ "
        )
        axs[0, 2].set_xlim([900, 1800])
    elif x == "nB":
        axs[0, 2].set_title(
            r"$n_B \, \mathrm{[n_{sat}]} \, vs \, n_B \, \mathrm{[n_{sat}]}$ "
        )

    axs[0, 2].legend(loc="best", fontsize=8)

    Y_Q = df["nQ"] / (N_SAT * df["nB"])

    axs[1, 0].scatter(df[x], Y_Q, c="darkolivegreen", marker=".", s=10, label="C++")
    if x == "muB":
        axs[1, 0].set_title(r"$Y_Q \, vs \, \mu_B\, \mathrm{[MeV]}$ ")
        axs[1, 0].set_xlim([900, 1800])
    elif x == "nB":
        axs[1, 0].set_title(r"$Y_Q \, vs \, n_B \, \mathrm{[n_{sat}]}$ ")

    axs[1, 0].legend(loc="best", fontsize=8)

    axs[1, 1].scatter(
        df[x],
        df["proton_chemical_potential"],
        c="darkorange",
        marker="x",
        s=10,
        label=r"$p$",
    )
    axs[1, 1].scatter(
        df[x],
        df["neutron_chemical_potential"],
        c="darkolivegreen",
        marker=".",
        s=10,
        label=r"$n$",
    )

    if x == "muB":
        axs[1, 1].set_title(r"$\mu \, vs \, \mu_B\, \mathrm{[MeV]}$ ")
        axs[1, 1].set_xlim([900, 1800])
    elif x == "nB":
        axs[1, 1].set_title(r"$\mu \, vs \, n_B \, \mathrm{[n_{sat}]}$ ")

    axs[1, 1].legend(loc="best", fontsize=8)

    axs[1, 2].scatter(
        df[x],
        df["proton_chemical_potential"],
        c="darkorange",
        marker="x",
        s=10,
        label=r"$p$",
    )

    axs[1, 2].scatter(
        df[x],
        df["neutron_chemical_potential"],
        c="darkolivegreen",
        marker=".",
        s=10,
        label=r"$n$",
    )

    if x == "muB":
        axs[1, 2].set_title(r"$\mu \, vs \, \mu_B\, \mathrm{[MeV]}$ ")
        axs[1, 2].set_xlim([900, 1800])
    elif x == "nB":
        axs[1, 2].set_title(r"$\mu \, vs \, n_B \, \mathrm{[n_{sat}]}$ ")

    axs[1, 2].legend(loc="best", fontsize=8)

    axs[2, 0].scatter(
        df[x],
        df["proton_effective_mass"],
        c="darkorange",
        marker="x",
        s=10,
        label=r"$p$",
    )
    axs[2, 0].scatter(
        df[x],
        df["neutron_effective_mass"],
        c="darkolivegreen",
        marker=".",
        s=10,
        label=r"$n$",
    )

    if x == "muB":
        axs[2, 0].set_title(r"$m^* \, vs \, \mu_B\, \mathrm{[MeV]}$ ")
        axs[2, 0].set_xlim([900, 1800])
    elif x == "nB":
        axs[2, 0].set_title(r"$m^* \, vs \, n_B \, \mathrm{[n_{sat}]}$ ")

    axs[2, 0].legend(loc="best", fontsize=8)

    axs[2, 1].scatter(
        df[x],
        df["proton_vector_density"],
        c="darkorange",
        marker="x",
        s=10,
        label=r"$p$",
    )
    axs[2, 1].scatter(
        df[x],
        df["neutron_vector_density"],
        c="darkolivegreen",
        marker=".",
        s=10,
        label=r"$n$",
    )

    if x == "muB":
        axs[2, 1].set_title(
            r"$n_{B,i} \, \mathrm{[fm^-3]} \, vs \, \mu_B\, \mathrm{[MeV]}$ "
        )
        axs[2, 2].set_xlim([900, 1800])
    elif x == "nB":
        axs[2, 1].set_title(
            r"$n_{B,i} \, \mathrm{[fm^-3]} \, vs \, n_B \, \mathrm{[n_{sat}]}$ "
        )

    axs[2, 1].legend(loc="best", fontsize=8)

    axs[2, 2].scatter(
        df[x],
        df["proton_potential"],
        c="darkorange",
        marker="x",
        s=10,
        label=r"$p$",
    )

    axs[2, 2].scatter(
        df[x],
        df["neutron_potential"],
        c="darkolivegreen",
        marker=".",
        s=10,
        label=r"$n$",
    )

    if x == "muB":
        axs[2, 2].set_title(r"$U \, vs \, \mu_B\, \mathrm{[MeV]}$ ")
        axs[2, 2].set_xlim([900, 1800])
    elif x == "nB":
        axs[2, 2].set_title(r"$U \, vs \, n_B \, \mathrm{[n_{sat}]}$ ")

    axs[2, 2].legend(loc="best", fontsize=8)

    # Set the main title of the figure
    # fig.suptitle('Results summary \, vs \, '+x)

    # Adjust the spacing between subplots
    fig.tight_layout()

    if save:
        filename = os.path.join(plot_directory, plotname)
        print("Saved\t ", filename + ".png")
        plt.savefig(filename, dpi=500)

    if show:
        plt.show()

    plt.close()


if __name__ == "__main__":
    # -----------------------------------------------------------------------------------------------------------------#
    # parse arguments
    # -----------------------------------------------------------------------------------------------------------------#

    # Create the parser
    parser = argparse.ArgumentParser(description="parser with only one argument")

    # Add a positional argument (optional)
    parser.add_argument(
        "run_name",
        nargs="?",
        default="default",
        help="Input folder to process. If none is provided, default folder will be used.",
    )

    # Parse the arguments from the command line
    args = parser.parse_args()

    # Get the value of the run_name argument
    if len(sys.argv) == 1:
        print(f"Running with user-provided folder by config.yaml")
        with open("../input/validated_config.yaml", "r") as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)
            run_name = yaml_data["run_name"]
    elif len(sys.argv) == 2:
        run_name = args.run_name
    else:
        raise ValueError(
            "Too many arguments provided. Maximum one argument is allowed."
        )

    # -----------------------------------------------------------------------------------------------------------------#
    # define parameters
    # -----------------------------------------------------------------------------------------------------------------#

    show = True
    save = True

    if save:
        plot_directory = "../output/" + run_name + "/plots/"

        if not os.path.exists(plot_directory):
            os.makedirs(plot_directory)

    # -----------------------------------------------------------------------------------------------------------------#
    # read data
    # -----------------------------------------------------------------------------------------------------------------#

    df = pd.read_csv(
        "../output/CMF_output_for_flavor_equilibration.csv",
        delimiter=SEPARATOR,
        names=FLAVOR_EQUILIBRATION_VARIABLES,
    )

    # -----------------------------------------------------------------------------------------------------------------#
    # unit conversion for nB and vector_density_without_Phi_order
    # -----------------------------------------------------------------------------------------------------------------#

    df["nB"] = df["nB"] / N_SAT

    # -----------------------------------------------------------------------------------------------------------------#
    # check test data
    # -----------------------------------------------------------------------------------------------------------------#

    print(df.head())
    print(df.mean())

    # -----------------------------------------------------------------------------------------------------------------#
    # plot summaries
    # -----------------------------------------------------------------------------------------------------------------#

    plot2D_flavor(x="muB", plotname="flavor_eq_summary_muB", show=show, save=save)

    plot2D_flavor(x="nB", plotname="flavor_eq_summary_nB", show=show, save=save)
