#include "input.hpp"

/**
 * @file input.cpp
 * @brief (implementation) input class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

input_params_struct input::input_params;

/**
 * Default constructor for the input class.
 * Does not perform any initialization.
 */
input::input() {}

/**
 * Sets the path to the configuration YAML file.
 * @param config_path The file path to the YAML configuration file.
 */
void input::set_config_path(const string &config_path) { config_path_ = config_path; }

/**
 * Gets the current path of the configuration YAML file.
 * @return The file path as a string.
 */
string input::get_config_path() { return config_path_; }

/**
 * Initializes the input class by reading and validating two YAML files.
 * Reads the user configuration file and stores it as a YAML::Node.
 */
void input::create_input_params() {
    double g_4 = 0.0;
    double gN_omega = 0.0;
    double gN_phi = 0.0;
    double gN_rho = 0.0;
    double mass0 = 0.0;
    double m_3H = 0.0;
    double V_Delta = 0.0;
    double T0 = 0.0;
    double sigma0_begin = 0.0;
    double sigma0_end = 0.0;
    double sigma0_step = 0.0;
    double zeta0_begin = 0.0;
    double zeta0_end = 0.0;
    double zeta0_step = 0.0;
    double delta0_begin = 0.0;
    double delta0_end = 0.0;
    double delta0_step = 0.0;
    double omega0_begin = 0.0;
    double omega0_end = 0.0;
    double omega0_step = 0.0;
    double phi0_begin = 0.0;
    double phi0_end = 0.0;
    double phi0_step = 0.0;
    double rho0_begin = 0.0;
    double rho0_end = 0.0;
    double rho0_step = 0.0;
    double Phi_order0_begin = 0.0;
    double Phi_order0_end = 0.0;
    double Phi_order0_step = 0.0;
    double d_betaQCD = 0.0;

    // Read in the user configuration file and store the resulting YAML::Node object in 'config_node'
    config_node = read_YAML_file();

    if (config_node["output_debug"].as<bool>()) {
        cout << "DEBUG: User configuration file contents:" << endl;
        print_YAML_node();
    }

    // Initialize input_params using values from config_node
    bool use_pure_glue = config_node["use_pure_glue"].as<bool>();
    bool use_quarks = config_node["use_quarks"].as<bool>();
    bool use_octet = config_node["use_octet"].as<bool>();
    bool use_decuplet = config_node["use_decuplet"].as<bool>();
    bool use_constant_sigma_mean_field = config_node["use_constant_sigma_mean_field"].as<bool>();
    bool use_constant_zeta_mean_field = config_node["use_constant_zeta_mean_field"].as<bool>();
    bool use_constant_delta_mean_field = config_node["use_constant_delta_mean_field"].as<bool>();
    bool use_constant_omega_mean_field = config_node["use_constant_omega_mean_field"].as<bool>();
    bool use_constant_phi_mean_field = config_node["use_constant_phi_mean_field"].as<bool>();
    bool use_constant_rho_mean_field = config_node["use_constant_rho_mean_field"].as<bool>();
    bool use_constant_Phi_order_field = config_node["use_constant_Phi_order_field"].as<bool>();

    if (use_pure_glue || (!use_quarks && !use_octet && !use_decuplet)) {
        // gauge means no particles only mean fields
        config_node["use_quarks"] = false;
        config_node["use_octet"] = false;
        config_node["use_decuplet"] = false;
        T0 = config_node["T0_gauge"].as<double>();
    } else {
        T0 = config_node["T0"].as<double>();
    }

    // don't loop over mean fields if they are not used
    if (use_constant_sigma_mean_field) {
        sigma0_begin = config_node["sigma0_begin"].as<double>();
        sigma0_end = sigma0_begin + 1.;
        sigma0_step = 10.;
    } else {
        sigma0_begin = config_node["sigma0_begin"].as<double>();
        sigma0_end = config_node["sigma0_end"].as<double>();
        sigma0_step = config_node["sigma0_step"].as<double>();
    }

    if (use_constant_zeta_mean_field) {
        zeta0_begin = config_node["zeta0_begin"].as<double>();
        zeta0_end = zeta0_begin + 1.;
        zeta0_step = 10.;
    } else {
        zeta0_begin = config_node["zeta0_begin"].as<double>();
        zeta0_end = config_node["zeta0_end"].as<double>();
        zeta0_step = config_node["zeta0_step"].as<double>();
    }

    if (use_constant_delta_mean_field) {
        delta0_begin = config_node["delta0_begin"].as<double>();
        delta0_end = delta0_begin + 1.;
        delta0_step = 10.;
    } else {
        delta0_begin = config_node["delta0_begin"].as<double>();
        delta0_end = config_node["delta0_end"].as<double>();
        delta0_step = config_node["delta0_step"].as<double>();
    }

    if (use_constant_omega_mean_field) {
        omega0_begin = config_node["omega0_begin"].as<double>();
        omega0_end = omega0_begin + 1.;
        omega0_step = 10.;
    } else {
        omega0_begin = config_node["omega0_begin"].as<double>();
        omega0_end = config_node["omega0_end"].as<double>();
        omega0_step = config_node["omega0_step"].as<double>();
    }

    if (use_constant_phi_mean_field) {
        phi0_begin = config_node["phi0_begin"].as<double>();
        phi0_end = phi0_begin + 1.;
        phi0_step = 10.;
    } else {
        phi0_begin = config_node["phi0_begin"].as<double>();
        phi0_end = config_node["phi0_end"].as<double>();
        phi0_step = config_node["phi0_step"].as<double>();
    }

    if (use_constant_rho_mean_field) {
        rho0_begin = config_node["rho0_begin"].as<double>();
        rho0_end = rho0_begin + 1.;
        rho0_step = 10.;
    } else {
        rho0_begin = config_node["rho0_begin"].as<double>();
        rho0_end = config_node["rho0_end"].as<double>();
        rho0_step = config_node["rho0_step"].as<double>();
    }

    if (use_constant_Phi_order_field) {
        Phi_order0_begin = config_node["Phi_order0_begin"].as<double>();
        Phi_order0_end = Phi_order0_begin + 1.;
        Phi_order0_step = 10.;
    } else {
        Phi_order0_begin = config_node["Phi_order0_begin"].as<double>();
        Phi_order0_end = config_node["Phi_order0_end"].as<double>();
        Phi_order0_step = config_node["Phi_order0_step"].as<double>();
    }

    // don't loop over Phi_order if quarks are not used
    if (!use_quarks) {
        Phi_order0_begin = 0.;
        Phi_order0_end = 1.;
        Phi_order0_step = 10.;
    }

    // convert 0.0606060606 to 2./33. if it is close enough
    if (config_node["d_betaQCD"].as<double>() - 2. / 33. < 1e-9) {
        d_betaQCD = 2. / 33.;
    } else {
        d_betaQCD = config_node["d_betaQCD"].as<double>();
    }

    int vector_potential = config_node["vector_potential"].as<int>();

    if (config_node["use_default_vector_couplings"].as<bool>()) {
        switch (vector_potential) {
            case 1:
                g_4 = 58.40;
                gN_omega = 13.66;
                gN_phi = 0.0;
                gN_rho = 4.94;
                mass0 = 0.0;
                m_3H = 1.24;
                V_Delta = 1.07;
                break;
            case 2:
                g_4 = 58.40;
                gN_omega = 13.66;
                gN_phi = 0.0;
                gN_rho = 3.97;
                mass0 = 0.0;
                m_3H = 1.24;
                V_Delta = 1.07;
                break;
            case 3:
                g_4 = 58.40;
                gN_omega = 13.66;
                gN_phi = 0.0;
                gN_rho = 4.318;
                mass0 = 0.0;
                m_3H = 1.24;
                V_Delta = 1.07;
                break;
            case 4:
                g_4 = 38.90;
                gN_omega = 11.90;
                gN_phi = 0.0;
                gN_rho = 4.03;
                mass0 = 150.0;
                m_3H = 0.85914584;
                V_Delta = 1.2;
                break;
            default:
                cout << "Invalid vector potential choice" << endl;
                exit(1);
                break;
        }
    } else {
        g_4 = config_node["g_4"].as<double>();
        gN_omega = config_node["gN_omega"].as<double>();
        gN_phi = config_node["gN_phi"].as<double>();
        gN_rho = config_node["gN_rho"].as<double>();
        mass0 = config_node["mass0"].as<double>();
        m_3H = config_node["m_3H"].as<double>();
        V_Delta = config_node["V_Delta"].as<double>();
    }

    input_params = {
        config_node["run_name"].as<string>(), config_node["use_ideal_gas"].as<bool>(), use_quarks, use_octet,
        use_decuplet, use_pure_glue, config_node["use_hyperons"].as<bool>(), use_constant_sigma_mean_field,
        use_constant_zeta_mean_field, use_constant_delta_mean_field, use_constant_omega_mean_field,
        use_constant_phi_mean_field, use_constant_rho_mean_field, config_node["use_Phi_order"].as<bool>(),
        use_constant_Phi_order_field, config_node["use_default_vector_couplings"].as<bool>(),
        config_node["output_debug"].as<bool>(), config_node["output_flavor_equilibration"].as<bool>(),
        config_node["output_particle_properties"].as<bool>(),
        vector_potential, config_node["baryon_mass_coupling"].as<int>(), V_Delta,
        config_node["solution_resolution"].as<double>(), config_node["maximum_for_residues"].as<double>(),
        config_node["muB_begin"].as<double>(), config_node["muB_end"].as<double>(),
        config_node["muB_step"].as<double>(), config_node["muQ_begin"].as<double>(),
        config_node["muQ_end"].as<double>(), config_node["muQ_step"].as<double>(),
        config_node["muS_begin"].as<double>(), config_node["muS_end"].as<double>(),
        config_node["muS_step"].as<double>(), sigma0_begin, sigma0_end, sigma0_step, zeta0_begin, zeta0_end, zeta0_step,
        delta0_begin, delta0_end, delta0_step, omega0_begin, omega0_end, omega0_step, phi0_begin, phi0_end, phi0_step,
        rho0_begin, rho0_end, rho0_step, Phi_order0_begin, Phi_order0_end, Phi_order0_step, d_betaQCD,
        config_node["f_pi"].as<double>(), config_node["f_K"].as<double>(), config_node["gqu_sigma"].as<double>(),
        config_node["gqu_zeta"].as<double>(), config_node["gqu_delta"].as<double>(),
        config_node["gqu_omega"].as<double>(), config_node["gqu_phi"].as<double>(), config_node["gqu_rho"].as<double>(),
        config_node["gqd_sigma"].as<double>(), config_node["gqd_zeta"].as<double>(),
        config_node["gqd_delta"].as<double>(), config_node["gqd_omega"].as<double>(),
        config_node["gqd_phi"].as<double>(), config_node["gqd_rho"].as<double>(), config_node["gqs_sigma"].as<double>(),
        config_node["gqs_zeta"].as<double>(), config_node["gqs_delta"].as<double>(),
        config_node["gqs_omega"].as<double>(), config_node["gqs_phi"].as<double>(), config_node["gqs_rho"].as<double>(),
        config_node["gQ_Phi_order"].as<double>(), config_node["gB_Phi_order"].as<double>(),
        config_node["pion_vacuum_mass"].as<double>(), config_node["kaon_vacuum_mass"].as<double>(),
        config_node["nucleon_vacuum_mass"].as<double>(), config_node["Lambda_vacuum_mass"].as<double>(),
        config_node["Sigma_vacuum_mass"].as<double>(), config_node["Delta_vacuum_mass"].as<double>(),
        config_node["Sigma_star_vacuum_mass"].as<double>(), config_node["Omega_vacuum_mass"].as<double>(),
        config_node["up_quark_bare_mass"].as<double>(), config_node["down_quark_bare_mass"].as<double>(),
        config_node["strange_quark_bare_mass"].as<double>(), config_node["m_1"].as<double>(),
        config_node["m_2"].as<double>(), m_3H, config_node["m_3D"].as<double>(), config_node["k_0"].as<double>(),
        config_node["k_1"].as<double>(), config_node["k_2"].as<double>(), config_node["k_3"].as<double>(),
        config_node["a_1"].as<double>(), config_node["a_3"].as<double>(), T0, config_node["alpha_X"].as<double>(),
        config_node["gN_sigma"].as<double>(), config_node["gN_zeta"].as<double>(), g_4, gN_omega, gN_phi, gN_rho, mass0,
        // vacuum values
        -config_node["f_pi"].as<double>(),
        1. / sqrt(2.) * config_node["f_pi"].as<double>() - sqrt(2.) * config_node["f_K"].as<double>(),
        config_node["omega_mean_field_vacuum_mass"].as<double>(),
        config_node["phi_mean_field_vacuum_mass"].as<double>(), config_node["rho_mean_field_vacuum_mass"].as<double>(),
        config_node["chi_mean_field_vacuum_value"].as<double>(),
        config_node["hbarc"].as<double>() * config_node["hbarc"].as<double>() * config_node["hbarc"].as<double>()};

    // print input parameters
    print_input_params();
}

// Function to print all elements of the struct
void input::print_input_params() {
    cout << "\nInput parameters for this run:" << endl;
    cout << "\trun_name:\t" << input_params.run_name << endl;
    cout << "\tuse_ideal_gas:\t" << input_params.use_ideal_gas << endl;
    cout << "\tuse_quarks:\t" << input_params.use_quarks << endl;
    cout << "\tuse_octet:\t" << input_params.use_octet << endl;
    cout << "\tuse_decuplet:\t" << input_params.use_decuplet << endl;
    cout << "\tuse_pure_glue:\t" << input_params.use_pure_glue << endl;
    cout << "\tuse_hyperons:\t" << input_params.use_hyperons << endl;
    cout << "\tuse_constant_sigma_mean_field:\t" << input_params.use_constant_sigma_mean_field << endl;
    cout << "\tuse_constant_zeta_mean_field:\t" << input_params.use_constant_zeta_mean_field << endl;
    cout << "\tuse_constant_delta_mean_field:\t" << input_params.use_constant_delta_mean_field << endl;
    cout << "\tuse_constant_omega_mean_field:\t" << input_params.use_constant_omega_mean_field << endl;
    cout << "\tuse_constant_phi_mean_field:\t" << input_params.use_constant_phi_mean_field << endl;
    cout << "\tuse_constant_rho_mean_field:\t" << input_params.use_constant_rho_mean_field << endl;
    cout << "\tuse_Phi_order :\t" << input_params.use_Phi_order << endl;
    cout << "\tuse_constant_Phi_order_field :\t" << input_params.use_constant_Phi_order_field << endl;
    cout << "\toutput_debug:\t" << input_params.output_debug << endl;
    cout << "\tuse_default_vector_couplings:\t" << input_params.use_default_vector_couplings << endl;
    cout << "\toutput_flavor_equilibration:\t" << input_params.output_flavor_equilibration << endl;
    cout << "\toutput_particle_properties:\t" << input_params.output_particle_properties << endl;
    cout << "\tvector_potential:\t" << input_params.vector_potential << endl;
    cout << "\tbaryon_mass_coupling:\t" << input_params.baryon_mass_coupling << endl;
    cout << "\tV_Delta:\t" << input_params.V_Delta << endl;
    cout << "\tsolution_resolution:\t" << input_params.solution_resolution << endl;
    cout << "\tmaximum_for_residues:\t" << input_params.maximum_for_residues << endl;
    cout << "\tmuB_begin:\t" << input_params.muB_begin << endl;
    cout << "\tmuB_end:\t" << input_params.muB_end << endl;
    cout << "\tmuB_step:\t" << input_params.muB_step << endl;
    cout << "\tmuQ_begin:\t" << input_params.muQ_begin << endl;
    cout << "\tmuQ_end:\t" << input_params.muQ_end << endl;
    cout << "\tmuQ_step:\t" << input_params.muQ_step << endl;
    cout << "\tmuS_begin:\t" << input_params.muS_begin << endl;
    cout << "\tmuS_end:\t" << input_params.muS_end << endl;
    cout << "\tmuS_step:\t" << input_params.muS_step << endl;
    cout << "\tsigma0_begin:\t" << input_params.sigma0_begin << endl;
    cout << "\tsigma0_end:\t" << input_params.sigma0_end << endl;
    cout << "\tsigma0_step:\t" << input_params.sigma0_step << endl;
    cout << "\tzeta0_begin:\t" << input_params.zeta0_begin << endl;
    cout << "\tzeta0_end:\t" << input_params.zeta0_end << endl;
    cout << "\tzeta0_step:\t" << input_params.zeta0_step << endl;
    cout << "\tdelta0_begin:\t" << input_params.delta0_begin << endl;
    cout << "\tdelta0_end:\t" << input_params.delta0_end << endl;
    cout << "\tdelta0_step:\t" << input_params.delta0_step << endl;
    cout << "\tomega0_begin:\t" << input_params.omega0_begin << endl;
    cout << "\tomega0_end:\t" << input_params.omega0_end << endl;
    cout << "\tomega0_step:\t" << input_params.omega0_step << endl;
    cout << "\tphi0_begin:\t" << input_params.phi0_begin << endl;
    cout << "\tphi0_end:\t" << input_params.phi0_end << endl;
    cout << "\tphi0_step:\t" << input_params.phi0_step << endl;
    cout << "\trho0_begin:\t" << input_params.rho0_begin << endl;
    cout << "\trho0_end:\t" << input_params.rho0_end << endl;
    cout << "\trho0_step:\t" << input_params.rho0_step << endl;
    cout << "\tPhi_order0_begin:\t" << input_params.Phi_order0_begin << endl;
    cout << "\tPhi_order0_end:\t" << input_params.Phi_order0_end << endl;
    cout << "\tPhi_order0_step:\t" << input_params.Phi_order0_step << endl;
    cout << "\td_betaQCD:\t" << input_params.d_betaQCD << endl;
    cout << "\tf_pi:\t" << input_params.f_pi << endl;
    cout << "\tf_K:\t" << input_params.f_K << endl;
    cout << "\tgqu_sigma:\t" << input_params.gqu_sigma << endl;
    cout << "\tgqu_zeta:\t" << input_params.gqu_zeta << endl;
    cout << "\tgqu_delta:\t" << input_params.gqu_delta << endl;
    cout << "\tgqu_omega:\t" << input_params.gqu_omega << endl;
    cout << "\tgqu_phi:\t" << input_params.gqu_phi << endl;
    cout << "\tgqu_rho:\t" << input_params.gqu_rho << endl;
    cout << "\tgqd_sigma:\t" << input_params.gqd_sigma << endl;
    cout << "\tgqd_zeta:\t" << input_params.gqd_zeta << endl;
    cout << "\tgqd_delta:\t" << input_params.gqd_delta << endl;
    cout << "\tgqd_omega:\t" << input_params.gqd_omega << endl;
    cout << "\tgqd_phi:\t" << input_params.gqd_phi << endl;
    cout << "\tgqd_rho:\t" << input_params.gqd_rho << endl;
    cout << "\tgqs_sigma:\t" << input_params.gqs_sigma << endl;
    cout << "\tgqs_zeta:\t" << input_params.gqs_zeta << endl;
    cout << "\tgqs_delta:\t" << input_params.gqs_delta << endl;
    cout << "\tgqs_omega:\t" << input_params.gqs_omega << endl;
    cout << "\tgqs_phi:\t" << input_params.gqs_phi << endl;
    cout << "\tgqs_rho:\t" << input_params.gqs_rho << endl;
    cout << "\tgQ_Phi_order :\t" << input_params.gQ_Phi_order << endl;
    cout << "\tgB_Phi_order :\t" << input_params.gB_Phi_order << endl;
    cout << "\tpion_vacuum_mass:\t" << input_params.pion_vacuum_mass << endl;
    cout << "\tkaon_vacuum_mass:\t" << input_params.kaon_vacuum_mass << endl;
    cout << "\tnucleon_vacuum_mass:\t" << input_params.nucleon_vacuum_mass << endl;
    cout << "\tLambda_vacuum_mass:\t" << input_params.Lambda_vacuum_mass << endl;
    cout << "\tSigma_vacuum_mass:\t" << input_params.Sigma_vacuum_mass << endl;
    cout << "\tDelta_vacuum_mass:\t" << input_params.Delta_vacuum_mass << endl;
    cout << "\tSigma_star_vacuum_mass:\t" << input_params.Sigma_star_vacuum_mass << endl;
    cout << "\tOmega_vacuum_mass:\t" << input_params.Omega_vacuum_mass << endl;
    cout << "\tup_quark_bare_mass:\t" << input_params.up_quark_bare_mass << endl;
    cout << "\tdown_quark_bare_mass:\t" << input_params.down_quark_bare_mass << endl;
    cout << "\tstrange_quark_bare_mass:\t" << input_params.strange_quark_bare_mass << endl;
    cout << "\tm_1:\t" << input_params.m_1 << endl;
    cout << "\tm_2:\t" << input_params.m_2 << endl;
    cout << "\tm_3H:\t" << input_params.m_3H << endl;
    cout << "\tm_3D:\t" << input_params.m_3D << endl;
    cout << "\tk_0:\t" << input_params.k_0 << endl;
    cout << "\tk_1:\t" << input_params.k_1 << endl;
    cout << "\tk_2:\t" << input_params.k_2 << endl;
    cout << "\tk_3:\t" << input_params.k_3 << endl;
    cout << "\ta_1:\t" << input_params.a_1 << endl;
    cout << "\ta_3:\t" << input_params.a_3 << endl;
    cout << "\tT0:\t" << input_params.T0 << endl;
    cout << "\talpha_X:\t" << input_params.alpha_X << endl;
    cout << "\tgN_sigma:\t" << input_params.gN_sigma << endl;
    cout << "\tgN_zeta:\t" << input_params.gN_zeta << endl;
    cout << "\tg_4:\t" << input_params.g_4 << endl;
    cout << "\tgN_omega:\t" << input_params.gN_omega << endl;
    cout << "\tgN_phi:\t" << input_params.gN_phi << endl;
    cout << "\tgN_rho:\t" << input_params.gN_rho << endl;
    cout << "\tmass0:\t" << input_params.mass0 << endl;
    cout << "\tsigma_mean_field_vacuum_mass:\t" << input_params.sigma_mean_field_vacuum_mass << endl;
    cout << "\tzeta_mean_field_vacuum_mass:\t" << input_params.zeta_mean_field_vacuum_mass << endl;
    cout << "\tomega_mean_field_vacuum_mass:\t" << input_params.omega_mean_field_vacuum_mass << endl;
    cout << "\tphi_mean_field_vacuum_mass:\t" << input_params.phi_mean_field_vacuum_mass << endl;
    cout << "\trho_mean_field_vacuum_mass:\t" << input_params.rho_mean_field_vacuum_mass << endl;
    cout << "\tchi_mean_field_vacuum_value:\t" << input_params.chi_mean_field_vacuum_value << endl;
    cout << "\thbarc3:\t" << input_params.hbarc3 << endl;
}

/**
 * Reads and parses a YAML file into a YAML::Node object.
 *
 * @return YAML::Node representing the contents of the YAML file.
 * @throws runtime_error If the YAML file cannot be read or parsed.
 */
YAML::Node input::read_YAML_file() {
    // Check if the file exists; if not, throw a runtime error
    if (!file_exist(config_path_)) {
        throw runtime_error("YAML file not found: " + config_path_);
    }

    try {
        // Load the YAML file into a YAML::Node object and return it
        return YAML::LoadFile(config_path_);

    } catch (const exception &e) {
        // If an exception is caught during parsing, throw a runtime error
        throw runtime_error("Error reading YAML file " + config_path_ + ": " + e.what());
    }
}

/**
 * Outputs the contents of a YAML::Node to the console.
 * Supports nested nodes and scalar values.
 */
void input::print_YAML_node() {
    // Iterate over the node using a range-based for loop.
    for (const auto &node : config_node) {
        // Output the key or name of the current node to the console as a string.
        cout << "\t" << node.first.as<string>() << "\t";

        // Check if the current node is a scalar value before outputting it.
        if (node.second.IsScalar()) {
            // If so, output the corresponding scalar value to the console as a string.
            cout << node.second.as<string>() << endl;
        } else {
            // Otherwise, output the entire node and its contents, including any nested child nodes, to the console.
            cout << node.second << endl;
        }
    }
}