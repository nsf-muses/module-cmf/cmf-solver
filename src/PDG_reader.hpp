#ifndef PDG_READER_HPP
#define PDG_READER_HPP

/**
 * @file PDG_reader.hpp
 * @brief (header) PDG particle reader class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "PDG_particle.hpp"
#include "utils.hpp"

vector<int>::size_type get_position_from_ID(const vector<int> &P_IDs, const size_t _ID);

class PDG_reader {
   private:
    vector<int> particle_IDs;

   public:
    vector<PDG_Particle> read_particles;

    PDG_reader(const string &filename, const vector<size_t> &desired_particles_IDs);

    void print_read_particles();

    vector<int>::size_type get_particle_position_from_ID(const int _ID);
};

#endif