#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
file: create_config.py
author: Nikolas Cruz-Camacho <cnc6@illinois.edu>
Purpose: Generate a YAML configuration file for CMF++ using user input and OpenAPI specifications.

This script facilitates the creation of a YAML configuration file for the CMF++ (Chiral Mean Field) module
based on user input. The generated configuration file conforms to the structure defined by the OpenAPI specification.
Key functionalities include:
- Parsing command-line arguments to specify paths for API specifications and configuration files.
- Validating user-provided inputs against the OpenAPI specification.
- Generating a hierarchical YAML configuration file based on the OpenAPI-defined schema.
- Saving the resulting configuration file to a specified path.

Usage:
    python create_config.py --api_file_path <path_to_api_spec> --config_file_path <path_to_config>

Arguments:
    --api_file_path: Path to the OpenAPI specification file (optional, defaults to ../api/OpenAPI_Specifications_CMF.yaml).
    --config_file_path: Path to the created configuration file (optional, defaults to ../input/config.yaml).

Example:
    python create_config.py --api_file_path ../api/OpenAPI_Specifications_CMF.yaml --config_file_path ./config.yaml
"""

import argparse
import os
import yaml
import logging
from openapi_core import Spec

# Constants
BASE_DIR = os.path.dirname(__file__)
DEFAULT_API_FILE_PATH = os.path.join(
    BASE_DIR, "..", "api", "OpenAPI_Specifications_CMF.yaml"
)
DEFAULT_CONFIG_FILE_PATH = os.path.join(BASE_DIR, "..", "input", "config.yaml")

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def main() -> None:
    """
    Main function to create a YAML configuration file for the CMF++ module based on user input conforming to the OpenAPI specification.
    """
    parser = argparse.ArgumentParser(
        description="Create a YAML configuration file for the CMF++ module based on user input conforming to the OpenAPI specification"
    )
    parser.add_argument(
        "--api_file_path",
        type=str,
        default=DEFAULT_API_FILE_PATH,
        help="Path to the OpenAPI specification file",
    )
    parser.add_argument(
        "--config_file_path",
        type=str,
        default=DEFAULT_CONFIG_FILE_PATH,
        help="Path to the created configuration file",
    )

    args, _ = parser.parse_known_args()
    args = parse_args_from_spec(parser, spec_file_path=args.api_file_path)

    write_args_to_config(
        args, spec_file_path=args.api_file_path, config_file_path=args.config_file_path
    )


def parse_args_from_spec(
    argparser: argparse.ArgumentParser, spec_file_path: str
) -> argparse.Namespace:
    """
    Parse command line arguments based on the OpenAPI specification.

    Args:
        argparser (argparse.ArgumentParser): Argument parser instance.
        spec_file_path (str): Path to the OpenAPI specification file.

    Returns:
        argparse.Namespace: Parsed command line arguments.
    """
    with open(spec_file_path, "r") as fp:
        openapi_specs = Spec.from_file(fp)

    config_path = openapi_specs / "components" / "schemas" / "config" / "properties"

    def add_arguments(node, required=[]):
        for prop, value in node.items():
            if value["type"] == "object":
                add_arguments(value["properties"], required=value.get("required", []))
            else:
                argparser.add_argument(
                    f"--{prop}",
                    type=str,
                    required=prop in required,
                    help=value.get("description", ""),
                )

    with config_path.open() as config:
        add_arguments(config)

    return argparser.parse_args()


def write_args_to_config(
    args: argparse.Namespace, spec_file_path: str, config_file_path: str
) -> None:
    """
    Write parsed command line arguments to a YAML configuration file based on the OpenAPI specification.

    Args:
        args (argparse.Namespace): Parsed command line arguments.
        spec_file_path (str): Path to the OpenAPI specification file.
        config_file_path (str): Path to the created configuration file.
    """
    with open(spec_file_path, "r") as fp:
        openapi_specs = Spec.from_file(fp)

    config_path = openapi_specs / "components" / "schemas" / "config" / "properties"

    def append_arguments(node, data):
        for prop, value in node.items():
            if value["type"] == "object":
                data[prop] = {}
                append_arguments(value["properties"], data[prop])
            else:
                if vars(args).get(prop):
                    data[prop] = type_cast(vars(args)[prop], value["type"])

    data = {}
    with config_path.open() as config:
        append_arguments(config, data)

    with open(config_file_path, "w") as fp:
        yaml.safe_dump(data, fp)
        logging.info(f"Configuration file saved to {config_file_path}")


def type_cast(value: str, type_name: str):
    """
    Cast a string value to the specified type.

    Args:
        value (str): The string value to cast.
        type_name (str): The type to cast the value to.

    Returns:
        The casted value.
    """
    if type_name == "number":
        return float(value)
    elif type_name == "integer":
        return int(value)
    elif type_name == "boolean":
        return value.lower() in ("true", "1")
    else:
        return value


if __name__ == "__main__":
    logging.info(f"Starting execution of {__file__}...")
    try:
        main()
    except Exception as e:
        logging.error(f"An error occurred: {e}")
    logging.info(f"Execution end of {__file__}.")
