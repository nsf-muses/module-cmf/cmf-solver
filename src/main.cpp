#include "cmf.hpp"

/**
 * @file main.cpp
 * @brief Main driver code for the Chiral Mean Field model (CMF++) application.
 *
 * This file contains the main entry point for the Chiral Mean Field model (CMF++) application.
 * It handles initialization, configuration, and execution of the CMF++ model.
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

/**
 * @brief Main function of the CMF++ application.
 *
 * This function serves as the entry point for the CMF++ application. It performs the following steps:
 * 1. Displays the CMF++ logo in the console.
 * 2. Reads command-line arguments to determine the input mode (using user-provided config or default config).
 * 3. Initializes the CMF++ object and validates the input configuration.
 * 4. Copies the configuration file to the output directory.
 * 5. Initializes particle sectors.
 * 6. Computes fields and chemical potential divisions.
 * 7. Opens output files.
 * 8. Computes constant parameters.
 * 9. Iterates over chemical potentials to compute solutions.
 * 10. Closes output files and prints a success message.
 *
 * @param argc The number of command-line arguments.
 * @param argv An array of strings containing the command-line arguments.
 * @return Returns 0 on successful execution, and non-zero values on errors.
 */

// Define custom error codes
enum ErrorCode { SUCCESS = 200, COMMAND_LINE_ERROR = 500, FILE_IO_ERROR = 500, UNKNOWN_ERROR = 500 };

int main(int argc, char* argv[]) {
    // Print execution start message
    cout << "\n\033[1;32mCMF++ execution start !!!\033[0m\n";

    // Print logos
    cout << "\n\033[1;34m";
    puts(R"(+---------------------------------------------------------------------------------------------------------------------+)");
    puts(R"(|                                                                                                                     |)");
    puts(R"(|        CCCCCCCCCCCCC  MMMMMMMM               MMMMMMMM  FFFFFFFFFFFFFFFFFFFFFF                                       |)");
    puts(R"(|     CCC::::::::::::C  M:::::::M             M:::::::M  F::::::::::::::::::::F                                       |)");
    puts(R"(|   CC:::::::::::::::C  M::::::::M           M::::::::M  F::::::::::::::::::::F                                       |)");
    puts(R"(|  C:::::CCCCCCCC::::C  M:::::::::M         M:::::::::M  F::::::FFFFFFFFF:::::F       ++++++              ++++++      |)");
    puts(R"(| C:::::C       CCCCCC  M::::::::::M       M::::::::::M    F:::::F       FFFFFF       +::::+              +::::+      |)");
    puts(R"(|C:::::C                M:::::::::::M     M:::::::::::M    F:::::F                    +::::+              +::::+      |)");
    puts(R"(|C:::::C                M:::::::M::::M   M::::M:::::::M    F::::::FFFFFFFFFF    +++++++::::+++++++  +++++++::::+++++++|)");
    puts(R"(|C:::::C                M::::::M M::::M M::::M M::::::M    F:::::::::::::::F    +::::::::::::::::+  +::::::::::::::::+|)");
    puts(R"(|C:::::C                M::::::M  M::::M::::M  M::::::M    F:::::::::::::::F    +::::::::::::::::+  +::::::::::::::::+|)");
    puts(R"(|C:::::C                M::::::M   M:::::::M   M::::::M    F::::::FFFFFFFFFF    +++++++::::+++++++  +++++++::::+++++++|)");
    puts(R"(|C:::::C                M::::::M    M:::::M    M::::::M    F:::::F                    +::::+              +::::+      |)");
    puts(R"(| C:::::C       CCCCCC  M::::::M     MMMMM     M::::::M    F:::::F                    +::::+              +::::+      |)");
    puts(R"(|  C:::::CCCCCCCC::::C  M::::::M               M::::::M  FF:::::::FF                  ++++++              ++++++      |)");
    puts(R"(|   CC:::::::::::::::C  M::::::M               M::::::M  F::::::::FF                                                  |)");
    puts(R"(|     CCC::::::::::::C  M::::::M               M::::::M  F::::::::FF                                                  |)");
    puts(R"(|        CCCCCCCCCCCCC  MMMMMMMM               MMMMMMMM  FFFFFFFFFFF                                                  |)");
    puts(R"(|                                                                                                                     |)");
    puts(R"(|      ______     __  __     __     ______     ______     __                   ██    ██   ██     ██████     ██████    |)");
    puts(R"(|     /\  ___\   /\ \_\ \   /\ \   /\  == \   /\  __ \   /\ \                  ██    ██  ███    ██  ████         ██   |)");
    puts(R"(|     \ \ \____  \ \  __ \  \ \ \  \ \  __<   \ \  __ \  \ \ \____             ██    ██   ██    ██ ██ ██     █████    |)");
    puts(R"(|      \ \_____\  \ \_\ \_\  \ \_\  \ \_\ \_\  \ \_\ \_\  \ \_____\             ██  ██    ██    ████  ██    ██        |)");
    puts(R"(|       \/_____/   \/_/\/_/   \/_/   \/_/ /_/   \/_/\/_/   \/_____/              ████     ██ ██  ██████  ██ ███████   |)");
    puts(R"(|                                                                                                                     |)");
    puts(R"(|            __    __     ______     ______     __   __                                                               |)");
    puts(R"(|           /\ "-./  \   /\  ___\   /\  __ \   /\ "-.\ \                                                              |)");
    puts(R"(|           \ \ \-./\ \  \ \  __\   \ \  __ \  \ \ \-.  \                                                             |)");
    puts(R"(|            \ \_\ \ \_\  \ \_____\  \ \_\ \_\  \ \_\\"\_\                         __  __ _   _ ___ ___ ___           |)");
    puts(R"(|             \/_/  \/_/   \/_____/   \/_/\/_/   \/_/ \/_/                        |  \/  | | | / __| __/ __|          |)");
    puts(R"(|                                                                                 | |\/| | |_| \__ \ _|\__ \          |)");
    puts(R"(|                 ______   __     ______     __         _____                     |_|  |_|\___/|___/___|___/          |)");
    puts(R"(|                /\  ___\ /\ \   /\  ___\   /\ \       /\  __-.                                                       |)");
    puts(R"(|                \ \  __\ \ \ \  \ \  __\   \ \ \____  \ \ \/\ \                                                      |)");
    puts(R"(|                 \ \_\    \ \_\  \ \_____\  \ \_____\  \ \____-                                                      |)");
    puts(R"(|                  \/_/     \/_/   \/_____/   \/_____/   \/____/                                                      |)");
    puts(R"(|                                                                                                                     |)");
    puts(R"(+---------------------------------------------------------------------------------------------------------------------+)");
    cout << "\033[0m";

    // vX.X.X from https://patorjk.com/software/taag/#p=display&f=ANSI%20Regular&t=vX.X.X%0A

    // Default paths for input and output
    const string default_config_yaml_input_path = "../input/validated_config.yaml";
    const string default_root_output_path = "../output";
    const string default_PDG_quark_table_path = "./PDG/PDG2021Plus_quarks.dat";
    const string default_PDG_table_path = "./PDG/PDG2021Plus_massorder.dat";

    // Initialize paths with default values
    string config_yaml_input_path;
    string root_output_path;
    string PDG_quark_table_path;
    string PDG_table_path;

    // Parse command-line arguments
    if (argc == 1) {
        // No arguments given. Use default paths
        config_yaml_input_path = default_config_yaml_input_path;
        root_output_path = default_root_output_path;
        PDG_table_path = default_PDG_table_path;
        PDG_quark_table_path = default_PDG_quark_table_path;
    } else if (argc == 5) {
        // Four arguments given. Use provided paths
        config_yaml_input_path = argv[1];
        root_output_path = argv[2];
        PDG_table_path = argv[3];
        PDG_quark_table_path = argv[4];
    } else {
        // Invalid number of arguments
        cout << "\n\033[1;33mInvalid number of arguments. Use no arguments for default paths or provide exactly four "
                "arguments.\033[0m\n";
        create_status_file(COMMAND_LINE_ERROR, static_cast<string>(argv[2]) + "/status.yaml",
                           "Invalid number of arguments provided.");
        exit(1);
    }

    const string status_yaml_path = root_output_path + "/status.yaml";

    // Create status file for Calculation Engine (code 500)
    create_status_file(UNKNOWN_ERROR, status_yaml_path, "starting main.cpp");

    // Display the paths being used
    cout << "\n\033[1;32mUsing " << config_yaml_input_path << " as input file\033[0m\n";
    cout << "\n\033[1;32mUsing " << root_output_path << " as output directory\033[0m\n";
    cout << "\n\033[1;32mUsing " << PDG_table_path << " as PDG table\033[0m\n";
    cout << "\n\033[1;32mUsing " << PDG_quark_table_path << " as PDG quark table\033[0m\n";

    // Create the CMF++ object
    cmf cmf(config_yaml_input_path, root_output_path, PDG_table_path, PDG_quark_table_path);

    cmf.read_config_file();

    // Output paths
    string output_path_run_name = root_output_path + "/" + cmf.Input.input_params.run_name + "/";
    string config_yaml_output_path = output_path_run_name + "validated_config.yaml";

    // Create output directory
    if (mkdir(output_path_run_name.c_str(), 0755) == 0) {
        cout << "\n\033[1;32mOutput directory " << output_path_run_name << " created successfully\033[0m\n";
    } else {
        if (errno == EEXIST) {
            cout << "\n\033[1;33mOutput directory " << output_path_run_name << " already exists\033[0m\n";
        } else {
            cout << "\n\033[1;33mError creating output directory " << output_path_run_name << ": " << strerror(errno)
                 << "\033[0m\n";
        }
    }

    // Copy the config.yaml file to the output directory
    if (!copy_file(config_yaml_input_path, config_yaml_output_path)) {
        create_status_file(FILE_IO_ERROR, status_yaml_path, "cmf object unable to copy the input file.");
        throw runtime_error("\n\t\033[1;31m!!! CMF object unable to copy the input file.\033[0m\n");
    }
    cout << "\n\033[1;32mFile " << config_yaml_input_path << " copied to " << output_path_run_name << "\033[0m\n";

    // Compute divisions
    cmf.compute_fields_and_chemical_potential_divisions();

    // Open output files
    cmf.open_output_files();

    // Compute constant parameters
    cmf.calculate_CMF_constant_parameters();

    cout << "\nComputing solutions, please wait..." << endl;

    // Iterate over chemical potentials to compute solutions
    cmf.iterate_over_chemical_potentials();

    // Close output files
    cmf.close_output_files();

    // Print successful execution message
    cout << "\n\033[1;32mCMF++ successful execution complete !!!\033[0m\n";

    // Overwrite status file with "finished" status (code 200)
    create_status_file(SUCCESS, status_yaml_path, "Success");

    return 0;
}
