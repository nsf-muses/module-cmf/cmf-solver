#include "cmf.hpp"

/**
 * @file cmf.cpp
 * @brief (implementation) Chiral Mean Field model
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

void CMF_system(int n, double x[], double fvec[], void* params) {
    (void)n;  // unused

    double sigma = 0.;
    double zeta = 0.;
    double delta = 0.;
    double omega = 0.;
    double phi = 0.;
    double rho = 0.;
    double Phi_order = 0.;
    double sigma_source_term = 0.;
    double zeta_source_term = 0.;
    double delta_source_term = 0.;
    double omega_source_term = 0.;
    double phi_source_term = 0.;
    double rho_source_term = 0.;
    double Phi_order_source_term = 0.;

    cmf* cmf_vars = static_cast<cmf*>(params);

    sigma = x[0];
    zeta = x[1];
    delta = x[2];
    omega = x[3];
    phi = x[4];
    rho = x[5];
    Phi_order = x[6];

    if (cmf_vars->Input.input_params.use_quarks) {
        cmf_vars->Quarks.compute_source_terms(sigma, zeta, delta, omega, phi, rho, Phi_order);

        sigma_source_term += cmf_vars->Quarks.sigma_source_term;
        zeta_source_term += cmf_vars->Quarks.zeta_source_term;
        delta_source_term += cmf_vars->Quarks.delta_source_term;
        Phi_order_source_term += cmf_vars->Quarks.Phi_order_source_term;
        omega_source_term += cmf_vars->Quarks.omega_source_term;
        phi_source_term += cmf_vars->Quarks.phi_source_term;
        rho_source_term += cmf_vars->Quarks.rho_source_term;
    }

    if (cmf_vars->Input.input_params.use_octet) {
        cmf_vars->Baryon_Octet.compute_source_terms(sigma, zeta, delta, omega, phi, rho, Phi_order);

        sigma_source_term += cmf_vars->Baryon_Octet.sigma_source_term;
        zeta_source_term += cmf_vars->Baryon_Octet.zeta_source_term;
        delta_source_term += cmf_vars->Baryon_Octet.delta_source_term;
        Phi_order_source_term += cmf_vars->Baryon_Octet.Phi_order_source_term;
        omega_source_term += cmf_vars->Baryon_Octet.omega_source_term;
        phi_source_term += cmf_vars->Baryon_Octet.phi_source_term;
        rho_source_term += cmf_vars->Baryon_Octet.rho_source_term;
    }

    if (cmf_vars->Input.input_params.use_decuplet) {
        cmf_vars->Baryon_Decuplet.compute_source_terms(sigma, zeta, delta, omega, phi, rho, Phi_order);

        sigma_source_term += cmf_vars->Baryon_Decuplet.sigma_source_term;
        zeta_source_term += cmf_vars->Baryon_Decuplet.zeta_source_term;
        delta_source_term += cmf_vars->Baryon_Decuplet.delta_source_term;
        Phi_order_source_term += cmf_vars->Baryon_Decuplet.Phi_order_source_term;
        omega_source_term += cmf_vars->Baryon_Decuplet.omega_source_term;
        phi_source_term += cmf_vars->Baryon_Decuplet.phi_source_term;
        rho_source_term += cmf_vars->Baryon_Decuplet.rho_source_term;
    }

    // EoM sigma
    if (cmf_vars->Input.input_params.use_constant_sigma_mean_field) {
        fvec[0] = sigma - cmf_vars->Input.input_params.sigma0_begin;
    } else {
        fvec[0] = sigma_source_term + cmf_vars->CMF_Constant_Parameters.pi_mass_squared_f_pi +
                  cmf_vars->CMF_Constant_Parameters.k_0_chi2 * sigma -
                  4. * cmf_vars->Input.input_params.k_1 * (sigma * sigma + zeta * zeta + delta * delta) * sigma -
                  2. * cmf_vars->Input.input_params.k_2 * (sigma * sigma + 3. * delta * delta) * sigma -
                  2. * cmf_vars->CMF_Constant_Parameters.k3_chi * sigma * zeta -
                  2 * cmf_vars->CMF_Constant_Parameters.eps_chi4 * sigma / (sigma * sigma - delta * delta);
    }

    // EoM zeta
    if (cmf_vars->Input.input_params.use_constant_zeta_mean_field) {
        fvec[1] = zeta - cmf_vars->Input.input_params.zeta0_begin;
    } else {
        fvec[1] = zeta_source_term + cmf_vars->CMF_Constant_Parameters.k_mass_fk_minus_pi_mass_f_pi +
                  cmf_vars->CMF_Constant_Parameters.k_0_chi2 * zeta -
                  4. * cmf_vars->Input.input_params.k_1 * (sigma * sigma + zeta * zeta + delta * delta) * zeta -
                  4. * cmf_vars->Input.input_params.k_2 * zeta * zeta * zeta -
                  cmf_vars->CMF_Constant_Parameters.k3_chi * (sigma * sigma - delta * delta) -
                  cmf_vars->CMF_Constant_Parameters.eps_chi4 / zeta;
    }

    // EoM delta
    if (cmf_vars->Input.input_params.use_constant_delta_mean_field) {
        fvec[2] = delta - cmf_vars->Input.input_params.delta0_begin;
    } else {
        fvec[2] = delta_source_term + cmf_vars->CMF_Constant_Parameters.k_0_chi2 * delta -
                  4. * cmf_vars->Input.input_params.k_1 * (sigma * sigma + zeta * zeta + delta * delta) * delta -
                  2. * cmf_vars->Input.input_params.k_2 * (delta * delta + 3. * sigma * sigma) * delta +
                  2. * cmf_vars->CMF_Constant_Parameters.k3_chi * zeta * delta +
                  2. * cmf_vars->CMF_Constant_Parameters.eps_chi4 * delta / (sigma * sigma - delta * delta);
    }

    switch (cmf_vars->Input.input_params.vector_potential) {
        case 1:  // C1
            // EoM omega
            if (cmf_vars->Input.input_params.use_constant_omega_mean_field) {
                fvec[3] = omega - cmf_vars->Input.input_params.omega0_begin;
            } else {
                fvec[3] = omega_source_term - cmf_vars->CMF_Constant_Parameters.omega_mass_squared * omega -
                          cmf_vars->Input.input_params.g_4 * (4. * omega * omega * omega + 12. * omega * rho * rho);
            }
            // EoM phi
            if (cmf_vars->Input.input_params.use_constant_phi_mean_field) {
                fvec[4] = phi - cmf_vars->Input.input_params.phi0_begin;
            } else {
                fvec[4] = phi_source_term - cmf_vars->CMF_Constant_Parameters.phi_mass_squared * phi -
                          8. * cmf_vars->Input.input_params.g_4 * phi * phi * phi;
            }
            // EoM rho
            if (cmf_vars->Input.input_params.use_constant_rho_mean_field) {
                fvec[5] = rho - cmf_vars->Input.input_params.rho0_begin;
            } else {
                fvec[5] = rho_source_term - cmf_vars->CMF_Constant_Parameters.rho_mass_squared * rho -
                          4 * cmf_vars->Input.input_params.g_4 * (3. * omega * omega * rho + rho * rho * rho);
            }
            break;

        case 2:  // C2
            // EoM omega
            if (cmf_vars->Input.input_params.use_constant_omega_mean_field) {
                fvec[3] = omega - cmf_vars->Input.input_params.omega0_begin;
            } else {
                fvec[3] = omega_source_term - cmf_vars->CMF_Constant_Parameters.omega_mass_squared * omega -
                          2. * cmf_vars->Input.input_params.g_4 * omega * (2. * omega * omega + 3. * phi * phi);
            }
            // EoM phi
            if (cmf_vars->Input.input_params.use_constant_phi_mean_field) {
                fvec[4] = phi - cmf_vars->Input.input_params.phi0_begin;
            } else {
                fvec[4] = phi_source_term - cmf_vars->CMF_Constant_Parameters.phi_mass_squared * phi -
                          2. * cmf_vars->Input.input_params.g_4 * phi * (3. * (omega * omega + rho * rho) + phi * phi);
            }
            // EoM rho
            if (cmf_vars->Input.input_params.use_constant_rho_mean_field) {
                fvec[5] = rho - cmf_vars->Input.input_params.rho0_begin;
            } else {
                fvec[5] = rho_source_term - cmf_vars->CMF_Constant_Parameters.rho_mass_squared * rho -
                          2. * cmf_vars->Input.input_params.g_4 * rho * (2. * rho * rho + 3. * phi * phi);
            }
            break;

        case 3:  // C3

            // EoM omega
            if (cmf_vars->Input.input_params.use_constant_omega_mean_field) {
                fvec[3] = omega - cmf_vars->Input.input_params.omega0_begin;
            } else {
                fvec[3] = omega_source_term - cmf_vars->CMF_Constant_Parameters.omega_mass_squared * omega -
                          4. * cmf_vars->Input.input_params.g_4 * omega * (omega * omega + rho * rho + phi * phi);
            }
            // EoM phi
            if (cmf_vars->Input.input_params.use_constant_phi_mean_field) {
                fvec[4] = phi - cmf_vars->Input.input_params.phi0_begin;
            } else {
                fvec[4] = phi_source_term - cmf_vars->CMF_Constant_Parameters.phi_mass_squared * phi -
                          4. * cmf_vars->Input.input_params.g_4 * phi * (omega * omega + rho * rho + phi * phi);
            }
            // EoM rho
            if (cmf_vars->Input.input_params.use_constant_rho_mean_field) {
                fvec[5] = rho - cmf_vars->Input.input_params.rho0_begin;
            } else {
                fvec[5] = rho_source_term - cmf_vars->CMF_Constant_Parameters.rho_mass_squared * rho -
                          4. * cmf_vars->Input.input_params.g_4 * rho * (omega * omega + rho * rho + phi * phi);
            }
            break;

        case 4:  // C4
            // EoM omega
            if (cmf_vars->Input.input_params.use_constant_omega_mean_field) {
                fvec[3] = omega - cmf_vars->Input.input_params.omega0_begin;
            } else {
                fvec[3] = omega_source_term - cmf_vars->CMF_Constant_Parameters.omega_mass_squared * omega -
                          cmf_vars->Input.input_params.g_4 *
                              (4 * omega * omega * omega + 6 * phi * phi * omega + 6. * sqrt(2.) * omega * omega * phi +
                               sqrt(2.) * phi * phi * phi);
            }

            // EoM phi
            if (cmf_vars->Input.input_params.use_constant_phi_mean_field) {
                fvec[4] = phi - cmf_vars->Input.input_params.phi0_begin;
            } else {
                fvec[4] = phi_source_term - cmf_vars->CMF_Constant_Parameters.phi_mass_squared * phi -
                          cmf_vars->Input.input_params.g_4 *
                              (phi * phi * phi + 6. * omega * omega * phi + 2. * sqrt(2.) * omega * omega * omega +
                               3. * sqrt(2.) * omega * phi * phi);
            }

            // EoM rho
            if (cmf_vars->Input.input_params.use_constant_rho_mean_field) {
                fvec[5] = rho - cmf_vars->Input.input_params.rho0_begin;
            } else {
                fvec[5] = rho_source_term - cmf_vars->CMF_Constant_Parameters.rho_mass_squared * rho;
            }
            break;
    }

    // EoM Phi_order
    if (cmf_vars->Input.input_params.use_constant_Phi_order_field || !(cmf_vars->Input.input_params.use_Phi_order)) {
        fvec[6] = Phi_order - cmf_vars->Input.input_params.Phi_order0_begin;
    } else {
        fvec[6] =
            Phi_order_source_term - (cmf_vars->CMF_Constant_Parameters.muB4 * (2. * cmf_vars->Input.input_params.a_1) +
                                     12. * cmf_vars->Input.input_params.a_3 * cmf_vars->CMF_Constant_Parameters.tnull4 /
                                         (Phi_order - 1.) / (3. * Phi_order + 1.)) *
                                        Phi_order;
    }
}

void cmf::remove_hyerons_from_particle_ids() {
    if (!(Input.input_params.use_hyperons)) {
        // Quarks - no strange quark_sector
        // exclude 3 from particle_ids_quarks
        particle_ids_quarks.erase(remove(particle_ids_quarks.begin(), particle_ids_quarks.end(), 3),
                                  particle_ids_quarks.end());

        // Baryon octet - no strange Baryon-resonances
        // exclude 3122, 3222, 3212, 3112, 3322, 3312 from particle_ids_octet

        particle_ids_octet.erase(remove(particle_ids_octet.begin(), particle_ids_octet.end(), 3122),
                                 particle_ids_octet.end());
        particle_ids_octet.erase(remove(particle_ids_octet.begin(), particle_ids_octet.end(), 3222),
                                 particle_ids_octet.end());
        particle_ids_octet.erase(remove(particle_ids_octet.begin(), particle_ids_octet.end(), 3212),
                                 particle_ids_octet.end());
        particle_ids_octet.erase(remove(particle_ids_octet.begin(), particle_ids_octet.end(), 3112),
                                 particle_ids_octet.end());
        particle_ids_octet.erase(remove(particle_ids_octet.begin(), particle_ids_octet.end(), 3322),
                                 particle_ids_octet.end());
        particle_ids_octet.erase(remove(particle_ids_octet.begin(), particle_ids_octet.end(), 3312),
                                 particle_ids_octet.end());

        // Baryon decuplet - no strange Baryon-resonances
        // exclude 3224, 3214, 3114, 3324, 3314, 3334 from particle_ids_decuplet

        particle_ids_decuplet.erase(remove(particle_ids_decuplet.begin(), particle_ids_decuplet.end(), 3224),
                                    particle_ids_decuplet.end());
        particle_ids_decuplet.erase(remove(particle_ids_decuplet.begin(), particle_ids_decuplet.end(), 3214),
                                    particle_ids_decuplet.end());
        particle_ids_decuplet.erase(remove(particle_ids_decuplet.begin(), particle_ids_decuplet.end(), 3114),
                                    particle_ids_decuplet.end());
        particle_ids_decuplet.erase(remove(particle_ids_decuplet.begin(), particle_ids_decuplet.end(), 3324),
                                    particle_ids_decuplet.end());
        particle_ids_decuplet.erase(remove(particle_ids_decuplet.begin(), particle_ids_decuplet.end(), 3314),
                                    particle_ids_decuplet.end());
        particle_ids_decuplet.erase(remove(particle_ids_decuplet.begin(), particle_ids_decuplet.end(), 3334),
                                    particle_ids_decuplet.end());
    }
}

void cmf::read_config_file() {
    try {
        Input.set_config_path(config_file_path);
        Input.create_input_params();
    } catch (...) {
        cerr << "Error: Could not read the configuration file. Exiting." << endl;
        exit(1);
    }
}

void cmf::update_mean_fields_and_residues(double x_init[], double f_vec_init[], solution& Current_Solution) {
    Current_Solution.update_mean_fields(x_init[0], x_init[1], x_init[2], x_init[3], x_init[4], x_init[5], x_init[6]);

    Current_Solution.update_residues(f_vec_init[0], f_vec_init[1], f_vec_init[2], f_vec_init[3], f_vec_init[4],
                                     f_vec_init[5], f_vec_init[6]);
}

void cmf::initialize_coupling_constants() { Couplings.create_couplings_and_bare_masses(); }

void cmf::initialize_particle_sectors() {
    // must happen before initializing particles
    remove_hyerons_from_particle_ids();

    if (Input.input_params.use_quarks) {
        cout << "\nInitializing Quarks" << endl;
        Quarks.initialize(PDG_reader(PDG_quark_table_path, particle_ids_quarks), Couplings);
    } else {
        Quarks.reset();
    }

    if (Input.input_params.use_octet) {
        cout << "\nInitializing Baryon octet" << endl;
        Baryon_Octet.initialize(PDG_reader(PDG_table_path, particle_ids_octet), Couplings);
    } else {
        Baryon_Octet.reset();
    }

    if (Input.input_params.use_decuplet) {
        cout << "\nInitializing Baryon decuplet" << endl;
        Baryon_Decuplet.initialize(PDG_reader(PDG_table_path, particle_ids_decuplet), Couplings);
    } else {
        Baryon_Decuplet.reset();
    }
}

int cmf::calculate_divisions(double end, double begin, double step) {
    return static_cast<int>(round(fabs(end - begin) / fabs(step)));
}

void cmf::display_initial_point_divisions() {
    cout << "\nPoints for initial guessess:\n"
         << "\n\tBaryon chemical potential \t" << muB0_divisions + 1 << "\n\tStrangeness chemical potential \t"
         << muS0_divisions + 1 << "\n\tCharge chemical potential \t" << muQ0_divisions + 1 << "\n\nPoints per field:\n"
         << "\n\tsigma \t" << sigma0_divisions + 1 << "\n\tzeta \t" << zeta0_divisions + 1 << "\n\tdelta \t"
         << delta0_divisions + 1 << "\n\tomega \t" << omega0_divisions + 1 << "\n\tphi \t" << phi0_divisions + 1
         << "\n\trho \t" << rho0_divisions + 1 << "\n\tPhi order \t" << Phi_order0_divisions + 1 << endl;
}

int cmf::calculate_total_points() {
    int total_p = 1;

    if (muB0_divisions != 0) total_p *= static_cast<int>(muB0_divisions + 1);
    if (muS0_divisions != 0) total_p *= static_cast<int>(muS0_divisions + 1);
    if (muQ0_divisions != 0) total_p *= static_cast<int>(muQ0_divisions + 1);
    if (sigma0_divisions != 0) total_p *= static_cast<int>(sigma0_divisions + 1);
    if (zeta0_divisions != 0) total_p *= static_cast<int>(zeta0_divisions + 1);
    if (delta0_divisions != 0) total_p *= static_cast<int>(delta0_divisions + 1);
    if (omega0_divisions != 0) total_p *= static_cast<int>(omega0_divisions + 1);
    if (phi0_divisions != 0) total_p *= static_cast<int>(phi0_divisions + 1);
    if (rho0_divisions != 0) total_p *= static_cast<int>(rho0_divisions + 1);
    if (Phi_order0_divisions != 0) total_p *= static_cast<int>(Phi_order0_divisions + 1);

    return total_p;
}

void cmf::calculate_CMF_constant_parameters() {
    CMF_Constant_Parameters = {
        Input.input_params.pion_vacuum_mass * Input.input_params.pion_vacuum_mass * Input.input_params.f_pi,
        Input.input_params.k_0 * Input.input_params.chi_mean_field_vacuum_value *
            Input.input_params.chi_mean_field_vacuum_value,
        Input.input_params.k_3 * Input.input_params.chi_mean_field_vacuum_value,
        Input.input_params.d_betaQCD * pow(Input.input_params.chi_mean_field_vacuum_value, 4) / 3.,
        Input.input_params.omega_mean_field_vacuum_mass * Input.input_params.omega_mean_field_vacuum_mass,
        sqrt(2) * Input.input_params.kaon_vacuum_mass * Input.input_params.kaon_vacuum_mass * Input.input_params.f_K -
            Input.input_params.pion_vacuum_mass * Input.input_params.pion_vacuum_mass * Input.input_params.f_pi /
                sqrt(2),
        Input.input_params.phi_mean_field_vacuum_mass * Input.input_params.phi_mean_field_vacuum_mass,
        Input.input_params.rho_mean_field_vacuum_mass * Input.input_params.rho_mean_field_vacuum_mass,
        0.,
        pow(Input.input_params.T0, 4)};
}

void cmf::update_particles_chemical_potentials(double muB, double muS, double muQ) {
    if (Input.input_params.use_quarks) {
        for (auto& particle : Quarks.particles) {
            particle.compute_chemical_potential(muB, muS, muQ);
        }
    }

    if (Input.input_params.use_octet) {
        for (auto& particle : Baryon_Octet.particles) {
            particle.compute_chemical_potential(muB, muS, muQ);
        }
    }

    if (Input.input_params.use_decuplet) {
        for (auto& particle : Baryon_Decuplet.particles) {
            particle.compute_chemical_potential(muB, muS, muQ);
        }
    }
}

void cmf::close_output_files() {
    if (OutFile_EoS) fclose(OutFile_EoS);
    if (OutFile_debug) fclose(OutFile_debug);
    if (OutFile_particle_properties) fclose(OutFile_particle_properties);
}

void cmf::set_initial_guess(double* x_init, int sigma0_i, int zeta0_i, int delta0_i, int omega0_i, int phi0_i,
                            int rho0_i, int Phi_order0_i) {
    x_init[0] = Input.input_params.sigma0_begin + sigma0_i * Input.input_params.sigma0_step;
    x_init[1] = Input.input_params.zeta0_begin + zeta0_i * Input.input_params.zeta0_step;
    x_init[2] = Input.input_params.delta0_begin + delta0_i * Input.input_params.delta0_step;
    x_init[3] = Input.input_params.omega0_begin + omega0_i * Input.input_params.omega0_step;
    x_init[4] = Input.input_params.phi0_begin + phi0_i * Input.input_params.phi0_step;
    x_init[5] = Input.input_params.rho0_begin + rho0_i * Input.input_params.rho0_step;
    x_init[6] = Input.input_params.use_Phi_order
                    ? Input.input_params.Phi_order0_begin + Phi_order0_i * Input.input_params.Phi_order0_step
                    : 0;
}

bool cmf::is_solution_valid_in_domain(const solution& Current_Solution) {
    return (Current_Solution.mean_fields.Phi_order >= 0 - 1e-14 &&
            Current_Solution.mean_fields.Phi_order <= 1 + 1e-14 &&
            fabs(Current_Solution.residues.sigma) < Input.input_params.maximum_for_residues &&
            fabs(Current_Solution.residues.zeta) < Input.input_params.maximum_for_residues &&
            fabs(Current_Solution.residues.delta) < Input.input_params.maximum_for_residues &&
            fabs(Current_Solution.residues.omega) < Input.input_params.maximum_for_residues &&
            fabs(Current_Solution.residues.phi) < Input.input_params.maximum_for_residues &&
            fabs(Current_Solution.residues.rho) < Input.input_params.maximum_for_residues &&
            fabs(Current_Solution.residues.Phi_order) < Input.input_params.maximum_for_residues &&
            Current_Solution.mean_fields.sigma <= 0 && Current_Solution.mean_fields.zeta <= 0);
}

bool cmf::is_solution_unique(const solution& Current_Solution,
                             const vector<solution>& Solutions_per_chemical_potential) {
    for (const auto& sol : Solutions_per_chemical_potential) {
        if (Current_Solution.calculate_mean_squared_difference(sol) < 1.e-8) {
            return false;
        }
    }
    return true;
}

bool cmf::is_solution_physical(const solution& Current_Solution) {
    bool is_solution_quark = false;
    bool is_solution_baryon_vacuum = false;
    bool is_solution_baryon_octet = false;
    bool is_solution_baryon_decuplet = false;
    bool is_solution_mixed_baryons = false;

    // if no baryons or quarks then this is gauge, so accept it
    if (!Input.input_params.use_octet && !Input.input_params.use_decuplet && !Input.input_params.use_quarks) {
        return true;
    }

    if (Input.input_params.use_quarks) {
        bool is_solution_quark_vacuum =
            (Baryon_Octet.total_vector_density + Baryon_Decuplet.total_vector_density + Quarks.total_vector_density ==
                 0 &&
             Current_Solution.mean_fields.Phi_order >=
                 1e-8);  // vacuum solution, zero vector density and finite Phi order loop

        if (is_solution_quark_vacuum) {
            return false;
        }

        is_solution_quark = ((Quarks.total_vector_density > 0 && Current_Solution.mean_fields.Phi_order > 1e-8 &&
                              Baryon_Octet.total_vector_density == 0 &&
                              Baryon_Decuplet.total_vector_density ==
                                  0));  // Quark with finite vector density and finite Phi order loop
    }

    is_solution_baryon_vacuum =
        (Baryon_Octet.total_vector_density + Baryon_Decuplet.total_vector_density + Quarks.total_vector_density == 0 &&
         Current_Solution.mean_fields.Phi_order <
             1e-8);  // vacuum solution, zero vector density and zero Phi order loop

    if (Input.input_params.use_octet) {
        is_solution_baryon_octet =
            ((Baryon_Octet.total_vector_density > 0 && Current_Solution.mean_fields.Phi_order <= 1e-8 &&
              Quarks.total_vector_density == 0 &&
              Baryon_Decuplet.total_vector_density ==
                  0));  // Baryon octet with finite vector density and zero Phi order loop
    }

    if (Input.input_params.use_decuplet) {
        is_solution_baryon_decuplet =
            ((Baryon_Decuplet.total_vector_density > 0 && Current_Solution.mean_fields.Phi_order <= 1e-8 &&
              Quarks.total_vector_density == 0));  // Baryon decuplet with finite vector density and zero Phi order loop
    }

    if (Input.input_params.use_octet && Input.input_params.use_decuplet) {
        is_solution_mixed_baryons =
            ((Baryon_Octet.total_vector_density > 0 && Baryon_Decuplet.total_vector_density > 0 &&
              Current_Solution.mean_fields.Phi_order <= 1e-8 &&
              Quarks.total_vector_density ==
                  0));  // Baryon octet and decuplet with finite vector density and zero Phi order loop
    }

    return (is_solution_baryon_vacuum || is_solution_baryon_octet || is_solution_baryon_decuplet ||
            is_solution_mixed_baryons || is_solution_quark);
}

void cmf::handle_solution(const solution& Current_Solution, vector<solution>& Solutions_per_chemical_potential) {
    if (Input.input_params.output_debug) {
        // write debug output
        write_solution_to_output(OutFile_debug, Current_Solution);
    }

    if (is_solution_physical(Current_Solution)) {
        if (is_solution_unique(Current_Solution, Solutions_per_chemical_potential)) {
            Solutions_per_chemical_potential.push_back(Current_Solution);
            write_solution_to_output(OutFile_EoS, Current_Solution);

            if (Input.input_params.output_particle_properties) {
                write_solution_to_particle_properties(OutFile_particle_properties, Current_Solution);
            }
        }
    }
}

void cmf::iterate_over_fields(solution& Current_Solution, vector<solution>& Solutions_per_chemical_potential) {
    const size_t equations_dimension = 7;

    double x_init[equations_dimension];                                          // initial guess
    double f_vec_init[equations_dimension];                                      // initial guess function values
    const int lwa = (equations_dimension * (3 * equations_dimension + 13)) / 2;  // work array length
    double wa[lwa];                                                              // work array

    observables Observables;

    // Loop over mean fields: sigma, zeta, delta, omega, phi, rho, Phi_order
    for (int sigma0_i = 0; sigma0_i <= sigma0_divisions; sigma0_i++) {
        for (int zeta0_i = 0; zeta0_i <= zeta0_divisions; zeta0_i++) {
            for (int delta0_i = 0; delta0_i <= delta0_divisions; delta0_i++) {
                for (int omega0_i = 0; omega0_i <= omega0_divisions; omega0_i++) {
                    for (int phi0_i = 0; phi0_i <= phi0_divisions; phi0_i++) {
                        for (int rho0_i = 0; rho0_i <= rho0_divisions; rho0_i++) {
                            for (int Phi_order0_i = 0; Phi_order0_i <= Phi_order0_divisions; Phi_order0_i++) {
                                // Print progress bar
                                print_progress_bar(Iteration_counter, Total_points);

                                // set initial guess
                                set_initial_guess(x_init, sigma0_i, zeta0_i, delta0_i, omega0_i, phi0_i, rho0_i,
                                                  Phi_order0_i);

                                // solve system
                                int solution_reached = fsolve(CMF_system, equations_dimension, x_init, f_vec_init, this,
                                                              Input.input_params.solution_resolution, wa, lwa);

                                // uncoment to test one iteration
                                // Current_Solution.print();
                                // exit(1);

                                if (solution_reached == 1) {
                                    // Update mean fields and residues for Current_Solution
                                    update_mean_fields_and_residues(x_init, f_vec_init, Current_Solution);

                                    if (is_solution_valid_in_domain(Current_Solution)) {
                                        // compute physical observables if solution is valid
                                        Observables.compute_observables(Quarks, Baryon_Octet, Baryon_Decuplet,
                                                                        Current_Solution);

                                        // output solution to file
                                        if (isfinite(Current_Solution.observables.pressure) &&
                                            isfinite(Current_Solution.observables.energy_density)) {
                                            handle_solution(Current_Solution, Solutions_per_chemical_potential);
                                        }
                                    }
                                }
                                // solution reached update counter
                                Iteration_counter++;
                            }
                        }
                    }
                }
            }
        }
    }
}

void cmf::open_output_files() {
    open_file(root_output_path + "/" + Input.input_params.run_name + "/CMF_intermediate_output.csv", OutFile_EoS);

    if (Input.input_params.output_particle_properties) {
        open_file(
            root_output_path + "/" + Input.input_params.run_name + "/CMF_intermediate_output_particle_properties.csv",
            OutFile_particle_properties);
    }

    if (Input.input_params.output_debug) {
        open_file(root_output_path + "/" + Input.input_params.run_name + "/CMF_intermediate_output_debug.csv",
                  OutFile_debug);
    }
}

void cmf::compute_fields_and_chemical_potential_divisions() {
    muB0_divisions =
        calculate_divisions(Input.input_params.muB_begin, Input.input_params.muB_end, Input.input_params.muB_step);
    muS0_divisions =
        calculate_divisions(Input.input_params.muS_begin, Input.input_params.muS_end, Input.input_params.muS_step);
    muQ0_divisions =
        calculate_divisions(Input.input_params.muQ_begin, Input.input_params.muQ_end, Input.input_params.muQ_step);

    sigma0_divisions = calculate_divisions(Input.input_params.sigma0_begin, Input.input_params.sigma0_end,
                                           Input.input_params.sigma0_step);
    zeta0_divisions = calculate_divisions(Input.input_params.zeta0_begin, Input.input_params.zeta0_end,
                                          Input.input_params.zeta0_step);
    delta0_divisions = calculate_divisions(Input.input_params.delta0_begin, Input.input_params.delta0_end,
                                           Input.input_params.delta0_step);
    omega0_divisions = calculate_divisions(Input.input_params.omega0_begin, Input.input_params.omega0_end,
                                           Input.input_params.omega0_step);
    phi0_divisions =
        calculate_divisions(Input.input_params.phi0_begin, Input.input_params.phi0_end, Input.input_params.phi0_step);
    rho0_divisions =
        calculate_divisions(Input.input_params.rho0_begin, Input.input_params.rho0_end, Input.input_params.rho0_step);
    Phi_order0_divisions = calculate_divisions(Input.input_params.Phi_order0_begin, Input.input_params.Phi_order0_end,
                                               Input.input_params.Phi_order0_step);

    display_initial_point_divisions();

    Total_points = calculate_total_points();

    cout << "\nTotal initial points: \t" << Total_points << endl;
}

void cmf::iterate_over_chemical_potentials() {
    Iteration_counter = 0;

    initialize_coupling_constants();

    initialize_particle_sectors();

    // loop over muB, muS, muQ
    for (int muB0_i = 0; muB0_i <= muB0_divisions; muB0_i++) {
        double muB = Input.input_params.muB_begin + muB0_i * Input.input_params.muB_step;

        CMF_Constant_Parameters.muB4 = muB * muB * muB * muB;

        for (int muS0_i = 0; muS0_i <= muS0_divisions; muS0_i++) {
            double muS = Input.input_params.muS_begin + muS0_i * Input.input_params.muS_step;

            for (int muQ0_i = 0; muQ0_i <= muQ0_divisions; muQ0_i++) {
                double muQ = Input.input_params.muQ_begin + muQ0_i * Input.input_params.muQ_step;

                vector<solution> Solutions_per_chemical_potential;
                solution Current_Solution;

                update_particles_chemical_potentials(muB, muS, muQ);

                Current_Solution.update_chemical_potentials(muB, muS, muQ);

                iterate_over_fields(Current_Solution, Solutions_per_chemical_potential);
            }
        }
    }
}

void cmf::write_solution_to_output(FILE* outFile, const solution& Sol) {
    fprintf(outFile,
            "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
            "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
            "%0.12E,%0.12E,%0.12E,%0.12E,"
            "%0.12E,%0.12E,%0.12E,%0.12E,"
            "%0.12E,%0.12E,%0.12E\n",
            0.0, Sol.chemical_potentials.Baryon, Sol.chemical_potentials.Strange, Sol.chemical_potentials.Charge,
            Sol.observables.vector_density, Sol.observables.strange_density, Sol.observables.charge_density,
            Sol.observables.energy_density, Sol.observables.pressure, 0.0, Sol.mean_fields.sigma, Sol.mean_fields.zeta,
            Sol.mean_fields.delta, Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho,
            Sol.mean_fields.Phi_order, Sol.observables.vector_density_without_Phi_order,
            Sol.observables.total_quark_vector_density / Input.input_params.hbarc3,
            Sol.observables.total_baryon_octet_vector_density / Input.input_params.hbarc3,
            Sol.observables.total_baryon_decuplet_vector_density / Input.input_params.hbarc3);
}

void cmf::write_solution_to_particle_properties(FILE* outFile, const solution& Sol) {
    fprintf(
        outFile,
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,"
        "%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E,%0.12E\n",
        0.0, Sol.chemical_potentials.Baryon, Sol.chemical_potentials.Strange, Sol.chemical_potentials.Charge,
        Sol.observables.vector_density, Sol.observables.strange_density, Sol.observables.charge_density,
        Sol.observables.energy_density, Sol.observables.pressure, 0.0, Sol.mean_fields.sigma, Sol.mean_fields.zeta,
        Sol.mean_fields.delta, Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho,
        Sol.mean_fields.Phi_order, Sol.observables.vector_density_without_Phi_order,
        Sol.observables.total_quark_vector_density / Input.input_params.hbarc3,
        Sol.observables.total_baryon_octet_vector_density / Input.input_params.hbarc3,
        Sol.observables.total_baryon_decuplet_vector_density / Input.input_params.hbarc3,
        Input.input_params.use_quarks ? Quarks.particles[0].Mass : 0.,
        Input.input_params.use_quarks ? Quarks.particles[1].Mass : 0.,
        Input.input_params.use_quarks ? Quarks.particles[2].Mass : 0.,
        Input.input_params.use_quarks ? Quarks.particles[0].effective_mass : 0.,
        Input.input_params.use_quarks ? Quarks.particles[1].effective_mass : 0.,
        Input.input_params.use_quarks ? Quarks.particles[2].effective_mass : 0.,
        Input.input_params.use_quarks ? Quarks.particles[0].chemical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[1].chemical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[2].chemical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[0].effective_chemical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[1].effective_chemical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[2].effective_chemical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[0].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_quarks ? Quarks.particles[1].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_quarks ? Quarks.particles[2].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_quarks ? Quarks.particles[0].optical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[1].optical_potential : 0.,
        Input.input_params.use_quarks ? Quarks.particles[2].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[0].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[1].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[2].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[3].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[4].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[5].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[6].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[7].Mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[0].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[1].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[2].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[3].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[4].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[5].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[6].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[7].effective_mass : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[0].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[1].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[2].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[3].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[4].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[5].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[6].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[7].chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[0].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[1].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[2].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[3].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[4].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[5].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[6].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[7].effective_chemical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[0].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[1].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[2].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[3].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[4].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[5].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[6].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[7].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[0].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[1].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[2].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[3].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[4].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[5].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[6].optical_potential : 0.,
        Input.input_params.use_octet ? Baryon_Octet.particles[7].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[0].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[1].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[2].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[3].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[4].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[5].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[6].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[7].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[8].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[9].Mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[0].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[1].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[2].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[3].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[4].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[5].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[6].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[7].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[8].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[9].effective_mass : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[0].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[1].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[2].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[3].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[4].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[5].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[6].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[7].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[8].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[9].chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[0].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[1].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[2].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[3].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[4].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[5].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[6].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[7].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[8].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[9].effective_chemical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[0].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[1].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[2].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[3].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[4].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[5].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[6].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[7].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[8].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[9].vector_density / Input.input_params.hbarc3 : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[0].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[1].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[2].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[3].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[4].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[5].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[6].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[7].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[8].optical_potential : 0.,
        Input.input_params.use_decuplet ? Baryon_Decuplet.particles[9].optical_potential : 0.);
}
