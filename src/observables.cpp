#include "observables.hpp"

/**
 * @file observables.cpp
 * @brief (implementation) observables class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

observables::observables() {}

void observables::compute_observables(quark_sector &Quarks, baryon_sector &Baryon_Octet, baryon_sector &Baryon_Decuplet,
                                      solution &Sol) {
    double hf2 = sqrt(2) * input_params.kaon_vacuum_mass * input_params.kaon_vacuum_mass * input_params.f_K -
                 input_params.pion_vacuum_mass * input_params.pion_vacuum_mass * input_params.f_pi / sqrt(2);

    total_omega_source_term = 0;
    total_phi_source_term = 0;
    total_rho_source_term = 0;
    total_fermionic_pressure = 0;
    total_fermionic_energy = 0;
    total_vector_density = 0;
    total_strange_density = 0;
    total_charge_density = 0;

    if (input_params.use_quarks) {
        Quarks.compute_source_terms(Sol.mean_fields.sigma, Sol.mean_fields.zeta, Sol.mean_fields.delta,
                                    Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho,
                                    Sol.mean_fields.Phi_order);

        Quarks.compute_total_vector_density();
        Quarks.compute_total_strangeness();
        Quarks.compute_total_charge();

        for (size_t i = 0; i < static_cast<size_t>(Quarks.particles.size()); i++) {
            Quarks.particles[i].compute_fermionic_energy();
            Quarks.particles[i].compute_fermionic_pressure();

            Quarks.particles[i].compute_potential(Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho);
        }

        Quarks.compute_total_fermionic_energy();
        Quarks.compute_total_fermionic_pressure();

        total_omega_source_term += Quarks.omega_source_term;
        total_phi_source_term += Quarks.phi_source_term;
        total_rho_source_term += Quarks.rho_source_term;

        total_fermionic_pressure += Quarks.total_fermionic_pressure;
        total_fermionic_energy += Quarks.total_fermionic_energy;

        total_vector_density += Quarks.total_vector_density;
        total_strange_density += Quarks.total_strangeness;
        total_charge_density += Quarks.total_charge;

        Sol.observables.total_quark_vector_density = Quarks.total_vector_density;
        Sol.observables.total_quark_strangeness_density = Quarks.total_strangeness;
        Sol.observables.total_quark_charge_density = Quarks.total_charge;
    } else {
        Sol.observables.total_quark_vector_density = 0;
        Sol.observables.total_quark_strangeness_density = 0;
        Sol.observables.total_quark_charge_density = 0;
    }

    if (input_params.use_octet) {
        Baryon_Octet.compute_source_terms(Sol.mean_fields.sigma, Sol.mean_fields.zeta, Sol.mean_fields.delta,
                                          Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho,
                                          Sol.mean_fields.Phi_order);

        Baryon_Octet.compute_total_vector_density();
        Baryon_Octet.compute_total_strangeness();
        Baryon_Octet.compute_total_charge();

        for (size_t i = 0; i < static_cast<size_t>(Baryon_Octet.particles.size()); i++) {
            Baryon_Octet.particles[i].compute_fermionic_energy();
            Baryon_Octet.particles[i].compute_fermionic_pressure();

            Baryon_Octet.particles[i].compute_potential(
                Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho);
        }

        Baryon_Octet.compute_total_fermionic_energy();
        Baryon_Octet.compute_total_fermionic_pressure();

        total_omega_source_term += Baryon_Octet.omega_source_term;
        total_phi_source_term += Baryon_Octet.phi_source_term;
        total_rho_source_term += Baryon_Octet.rho_source_term;

        total_fermionic_pressure += Baryon_Octet.total_fermionic_pressure;
        total_fermionic_energy += Baryon_Octet.total_fermionic_energy;

        total_vector_density += Baryon_Octet.total_vector_density;
        total_strange_density += Baryon_Octet.total_strangeness;
        total_charge_density += Baryon_Octet.total_charge;

        Sol.observables.total_baryon_octet_vector_density = Baryon_Octet.total_vector_density;
        Sol.observables.total_baryon_octet_strangeness_density = Baryon_Octet.total_strangeness;
        Sol.observables.total_baryon_octet_charge_density = Baryon_Octet.total_charge;
    } else {
        Sol.observables.total_baryon_octet_vector_density = 0;
        Sol.observables.total_baryon_octet_strangeness_density = 0;
        Sol.observables.total_baryon_octet_charge_density = 0;
    }

    if (input_params.use_decuplet) {
        Baryon_Decuplet.compute_source_terms(Sol.mean_fields.sigma, Sol.mean_fields.zeta, Sol.mean_fields.delta,
                                             Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho,
                                             Sol.mean_fields.Phi_order);

        Baryon_Decuplet.compute_total_vector_density();
        Baryon_Decuplet.compute_total_strangeness();
        Baryon_Decuplet.compute_total_charge();

        for (size_t i = 0; i < static_cast<size_t>(Baryon_Decuplet.particles.size()); i++) {
            Baryon_Decuplet.particles[i].compute_fermionic_energy();
            Baryon_Decuplet.particles[i].compute_fermionic_pressure();

            Baryon_Decuplet.particles[i].compute_potential(
                Sol.mean_fields.omega, Sol.mean_fields.phi, Sol.mean_fields.rho);
        }

        Baryon_Decuplet.compute_total_fermionic_energy();
        Baryon_Decuplet.compute_total_fermionic_pressure();

        total_omega_source_term += Baryon_Decuplet.omega_source_term;
        total_phi_source_term += Baryon_Decuplet.phi_source_term;
        total_rho_source_term += Baryon_Decuplet.rho_source_term;

        total_fermionic_pressure += Baryon_Decuplet.total_fermionic_pressure;
        total_fermionic_energy += Baryon_Decuplet.total_fermionic_energy;

        total_vector_density += Baryon_Decuplet.total_vector_density;
        total_strange_density += Baryon_Decuplet.total_strangeness;
        total_charge_density += Baryon_Decuplet.total_charge;

        Sol.observables.total_baryon_decuplet_vector_density = Baryon_Decuplet.total_vector_density;
        Sol.observables.total_baryon_decuplet_strangeness_density = Baryon_Decuplet.total_strangeness;
        Sol.observables.total_baryon_decuplet_charge_density = Baryon_Decuplet.total_charge;
    } else {
        Sol.observables.total_baryon_decuplet_vector_density = 0;
        Sol.observables.total_baryon_decuplet_strangeness_density = 0;
        Sol.observables.total_baryon_decuplet_charge_density = 0;
    }

    vector_contribution_to_pressure =
        1. / 2. * input_params.omega_mean_field_vacuum_mass * input_params.omega_mean_field_vacuum_mass *
            Sol.mean_fields.omega * Sol.mean_fields.omega +
        1. / 2. * input_params.phi_mean_field_vacuum_mass * input_params.phi_mean_field_vacuum_mass *
            Sol.mean_fields.phi * Sol.mean_fields.phi +
        1. / 2. * input_params.rho_mean_field_vacuum_mass * input_params.rho_mean_field_vacuum_mass *
            Sol.mean_fields.rho * Sol.mean_fields.rho;

    switch (input_params.vector_potential) {
        case 1:
            vector_contribution_to_pressure +=
                input_params.g_4 *
                (Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega +
                 6. * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.rho * Sol.mean_fields.rho +
                 Sol.mean_fields.rho * Sol.mean_fields.rho * Sol.mean_fields.rho * Sol.mean_fields.rho +
                 2. * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi);
            break;

        case 2:
            vector_contribution_to_pressure +=
                input_params.g_4 *
                (Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega +
                 Sol.mean_fields.rho * Sol.mean_fields.rho * Sol.mean_fields.rho * Sol.mean_fields.rho +
                 1. / 2. * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi +
                 3. * (Sol.mean_fields.rho * Sol.mean_fields.rho + Sol.mean_fields.omega * Sol.mean_fields.omega) *
                     Sol.mean_fields.phi * Sol.mean_fields.phi);
            break;

        case 3:
            vector_contribution_to_pressure +=
                input_params.g_4 *
                (Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega +
                 2. * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.rho * Sol.mean_fields.rho +
                 Sol.mean_fields.rho * Sol.mean_fields.rho * Sol.mean_fields.rho * Sol.mean_fields.rho +
                 Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi +
                 2. * (Sol.mean_fields.rho * Sol.mean_fields.rho + Sol.mean_fields.omega * Sol.mean_fields.omega) *
                     Sol.mean_fields.phi * Sol.mean_fields.phi);
            break;

        case 4:
            vector_contribution_to_pressure +=
                input_params.g_4 *
                (Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega +
                 1. / 4. * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi +
                 3. * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.phi * Sol.mean_fields.phi +
                 2. * sqrt(2.) * Sol.mean_fields.omega * Sol.mean_fields.omega * Sol.mean_fields.omega *
                     Sol.mean_fields.phi +
                 sqrt(2.) * Sol.mean_fields.omega * Sol.mean_fields.phi * Sol.mean_fields.phi * Sol.mean_fields.phi);
            break;
    }

    vector_contribution_to_energy = -vector_contribution_to_pressure + total_omega_source_term * Sol.mean_fields.omega +
                                    total_phi_source_term * Sol.mean_fields.phi +
                                    total_rho_source_term * Sol.mean_fields.rho;

    scalar_contribution_to_energy =
        1. / 2. * input_params.k_0 *
            (input_params.chi_mean_field_vacuum_value * input_params.chi_mean_field_vacuum_value *
                 (Sol.mean_fields.sigma * Sol.mean_fields.sigma + Sol.mean_fields.zeta * Sol.mean_fields.zeta +
                  Sol.mean_fields.delta * Sol.mean_fields.delta) -
             input_params.chi_mean_field_vacuum_value * input_params.chi_mean_field_vacuum_value *
                 (input_params.sigma_mean_field_vacuum_mass * input_params.sigma_mean_field_vacuum_mass +
                  input_params.zeta_mean_field_vacuum_mass * input_params.zeta_mean_field_vacuum_mass)) -
        input_params.k_2 *
            (Sol.mean_fields.sigma * Sol.mean_fields.sigma * Sol.mean_fields.sigma * Sol.mean_fields.sigma / 2. +
             Sol.mean_fields.delta * Sol.mean_fields.delta * Sol.mean_fields.delta * Sol.mean_fields.delta / 2. +
             Sol.mean_fields.zeta * Sol.mean_fields.zeta * Sol.mean_fields.zeta * Sol.mean_fields.zeta +
             3. * Sol.mean_fields.sigma * Sol.mean_fields.sigma * Sol.mean_fields.delta * Sol.mean_fields.delta -
             input_params.sigma_mean_field_vacuum_mass * input_params.sigma_mean_field_vacuum_mass *
                 input_params.sigma_mean_field_vacuum_mass * input_params.sigma_mean_field_vacuum_mass / 2. -
             input_params.zeta_mean_field_vacuum_mass * input_params.zeta_mean_field_vacuum_mass *
                 input_params.zeta_mean_field_vacuum_mass * input_params.zeta_mean_field_vacuum_mass) -
        input_params.k_1 *
            (Sol.mean_fields.sigma * Sol.mean_fields.sigma + Sol.mean_fields.zeta * Sol.mean_fields.zeta +
             Sol.mean_fields.delta * Sol.mean_fields.delta) *
            (Sol.mean_fields.sigma * Sol.mean_fields.sigma + Sol.mean_fields.zeta * Sol.mean_fields.zeta +
             Sol.mean_fields.delta * Sol.mean_fields.delta) +
        input_params.k_1 *
            (input_params.sigma_mean_field_vacuum_mass * input_params.sigma_mean_field_vacuum_mass +
             input_params.zeta_mean_field_vacuum_mass * input_params.zeta_mean_field_vacuum_mass) *
            (input_params.sigma_mean_field_vacuum_mass * input_params.sigma_mean_field_vacuum_mass +
             input_params.zeta_mean_field_vacuum_mass * input_params.zeta_mean_field_vacuum_mass) -
        input_params.k_3 *
            (input_params.chi_mean_field_vacuum_value *
                 (Sol.mean_fields.sigma * Sol.mean_fields.sigma - Sol.mean_fields.delta * Sol.mean_fields.delta) *
                 Sol.mean_fields.zeta -
             input_params.chi_mean_field_vacuum_value * input_params.sigma_mean_field_vacuum_mass *
                 input_params.sigma_mean_field_vacuum_mass * input_params.zeta_mean_field_vacuum_mass) -
        input_params.d_betaQCD * input_params.chi_mean_field_vacuum_value * input_params.chi_mean_field_vacuum_value *
            input_params.chi_mean_field_vacuum_value * input_params.chi_mean_field_vacuum_value *
            log(((Sol.mean_fields.sigma * Sol.mean_fields.sigma - Sol.mean_fields.delta * Sol.mean_fields.delta) *
                 Sol.mean_fields.zeta) /
                (input_params.sigma_mean_field_vacuum_mass * input_params.sigma_mean_field_vacuum_mass *
                 input_params.zeta_mean_field_vacuum_mass)) /
            3.;

    sym_breaking_contribution_to_energy = input_params.pion_vacuum_mass * input_params.pion_vacuum_mass *
                                              input_params.f_pi * (Sol.mean_fields.sigma + input_params.f_pi) +
                                          hf2 * (Sol.mean_fields.zeta - input_params.zeta_mean_field_vacuum_mass);

    if (input_params.use_Phi_order) {
        Phi_order_contribution_to_pressure =
            -input_params.T0 * input_params.T0 * input_params.T0 * input_params.T0 * input_params.a_3 *
                log((3. * Sol.mean_fields.Phi_order + 1.) * (1. - Sol.mean_fields.Phi_order) *
                    (1. - Sol.mean_fields.Phi_order) * (1. - Sol.mean_fields.Phi_order)) -
            Sol.chemical_potentials.Baryon * Sol.chemical_potentials.Baryon * Sol.chemical_potentials.Baryon *
                Sol.chemical_potentials.Baryon * input_params.a_1 * Sol.mean_fields.Phi_order *
                Sol.mean_fields.Phi_order;

        Phi_order_contribution_to_energy = -Phi_order_contribution_to_pressure -
                                           2. * Sol.chemical_potentials.Baryon * Sol.chemical_potentials.Baryon *
                                               Sol.chemical_potentials.Baryon * Sol.chemical_potentials.Baryon * 2. *
                                               input_params.a_1 * Sol.mean_fields.Phi_order * Sol.mean_fields.Phi_order;
    } else {
        Phi_order_contribution_to_pressure = 0;
        Phi_order_contribution_to_energy = 0;
    }

    Sol.observables.vector_density_without_Phi_order = total_vector_density / input_params.hbarc3;

    // only for T=0
    if (input_params.use_Phi_order) {
        Sol.observables.vector_density =
            Sol.observables.vector_density_without_Phi_order -
            (4. * input_params.a_1 * Sol.chemical_potentials.Baryon * Sol.chemical_potentials.Baryon *
             Sol.chemical_potentials.Baryon * Sol.mean_fields.Phi_order * Sol.mean_fields.Phi_order) /
                input_params.hbarc3;
    } else {
        Sol.observables.vector_density = Sol.observables.vector_density_without_Phi_order;
    }

    // units conversion to MeV
    Sol.observables.strange_density = total_strange_density / input_params.hbarc3;
    Sol.observables.charge_density = total_charge_density / input_params.hbarc3;

    // units conversion to MeV/fm^3
    Sol.observables.pressure =
        (vector_contribution_to_pressure - scalar_contribution_to_energy - sym_breaking_contribution_to_energy +
         Phi_order_contribution_to_pressure + total_fermionic_pressure) /
        input_params.hbarc3;
    Sol.observables.energy_density =
        (vector_contribution_to_energy + scalar_contribution_to_energy + sym_breaking_contribution_to_energy +
         Phi_order_contribution_to_energy + total_fermionic_energy) /
        input_params.hbarc3;
}