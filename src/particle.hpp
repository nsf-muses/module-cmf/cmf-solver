#ifndef PARTICLE_HPP
#define PARTICLE_HPP

/**
 * @file particle.hpp
 * @brief (header) Particle class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <iostream>

#include "input.hpp"

class particle : public input {
   public:
    int ID;                 // PDG ID
    string Name;            // name of the particle
    double Mass;            // mass of the particle in GeV
    short int Degeneracy;   // Spin degeneracy not color degeneracy
    double BaryonNumber;    // Baryon number
    short int Strangeness;  // Strangeness
    double Isospin_Z;       // Z projection of the isospin
    double ElectricCharge;  // Electric charge

    // coupling constants and bare mass
    double g_sigma;      // sigma coupling
    double g_zeta;       // zeta coupling
    double g_delta;      // delta coupling
    double g_omega;      // omega coupling
    double g_phi;        // phi coupling
    double g_rho;        // rho coupling
    double g_Phi_order;  // Phi order coupling
    double bare_mass;    // bare mass

    // computed quantities
    double vacuum_mass;
    void compute_vacuum_mass();
    double effective_mass;
    void compute_effective_mass(double sigma, double zeta, double delta);
    double chemical_potential;
    void compute_chemical_potential(double muB, double muS, double muQ);
    double effective_chemical_potential;
    void compute_effective_chemical_potential(double omega, double phi, double rho);

    double Fermi_momentum;
    void compute_Fermi_momentum();
    double energy;
    void compute_energy();

    double vector_density;
    void compute_vector_density();
    double scalar_density;
    void compute_scalar_density();

    double fermionic_energy;
    void compute_fermionic_energy();
    double fermionic_pressure;
    void compute_fermionic_pressure();

    double optical_potential;
    void compute_potential(double omega, double phi, double rho);

    // print methods
    inline void print_basic_info() {
        cout << "ID\t" << ID << '\n'
             << "Name\t" << Name << '\n'
             << "Mass\t" << Mass << '\n'
             << "Degeneracy\t" << Degeneracy << '\n'
             << "BaryonNumber\t" << BaryonNumber << '\n'
             << "Strangeness\t" << Strangeness << '\n'
             << "Isospin_Z\t" << Isospin_Z << '\n'
             << "ElectricCharge\t" << ElectricCharge << endl;
    }

    inline void print_couplings_info() {
        cout << scientific << "g_sigma\t" << g_sigma << '\n'
             << "g_zeta\t" << g_zeta << '\n'
             << "g_delta\t" << g_delta << '\n'
             << "g_omega\t" << g_omega << '\n'
             << "g_phi\t" << g_phi << '\n'
             << "g_rho\t" << g_rho << '\n'
             << "g_Phi_order\t" << g_Phi_order << '\n'
             << endl;
    }

    inline void print_masses_info() {
        cout << scientific << "bare_mass\t" << bare_mass << '\n'
             << "vacuum_mass\t" << vacuum_mass << '\n'
             << "effective_mass\t" << effective_mass << '\n'
             << endl;
    }

    inline void print_chemical_potential_info() {
        cout << scientific << "chemical_potential\t" << chemical_potential << '\n'
             << "effective_chemical_potential\t" << effective_chemical_potential << '\n'
             << endl;
    }

    inline void print_fermi_momentum_info() {
        cout << scientific << "Fermi_momentum\t" << Fermi_momentum << '\n' << endl;
    }

    inline void print_energy_info() {
        cout << scientific << "vector_density\t" << vector_density << '\n' << "energy\t" << energy << '\n' << endl;
    }

    inline void print_densities_info() {
        cout << scientific << "vector_density\t" << vector_density << '\n'
             << "scalar_density\t" << scalar_density << '\n'
             << endl;
    }

    inline void print_fermionic_info() {
        cout << scientific << "fermionic_energy\t" << fermionic_energy << '\n'
             << "fermionic_pressure\t" << fermionic_pressure << '\n'
             << endl;
    }

    inline void print_potential_info() { cout << "optical_potential\t" << optical_potential << '\n' << endl; }
};

class quark : public particle {
   public:
    quark();

   public:
    void compute_effective_mass(double sigma, double zeta, double delta, double Phi_order);
};

class baryon : public particle {
   public:
    baryon();

   public:
    void compute_effective_mass(double sigma, double zeta, double delta, double Phi_order);
};

#endif
