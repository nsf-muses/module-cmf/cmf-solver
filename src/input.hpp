#ifndef INPUT_HPP
#define INPUT_HPP

/**
 * @file input.hpp
 * @brief (header) Input class for managing and processing input parameters from YAML configuration files.
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <algorithm>
#include <iostream>

#include "utils.hpp"
#include "yaml-cpp/yaml.h"

/**
 * @brief Structure to store various input parameters read from a configuration YAML file.
 */
struct input_params_struct {
    // General configuration parameters
    std::string run_name;
    bool use_ideal_gas;
    bool use_quarks;
    bool use_octet;
    bool use_decuplet;
    bool use_pure_glue;
    bool use_hyperons;
    bool use_constant_sigma_mean_field;
    bool use_constant_zeta_mean_field;
    bool use_constant_delta_mean_field;
    bool use_constant_omega_mean_field;
    bool use_constant_phi_mean_field;
    bool use_constant_rho_mean_field;
    bool use_Phi_order;
    bool use_constant_Phi_order_field;
    bool use_default_vector_couplings;
    bool output_debug;
    bool output_flavor_equilibration;
    bool output_particle_properties;

    // Coupling and potential parameters
    int vector_potential;
    int baryon_mass_coupling;
    double V_Delta;

    // Resolution parameters
    double solution_resolution;
    double maximum_for_residues;

    // Chemical potential ranges
    double muB_begin, muB_end, muB_step;
    double muQ_begin, muQ_end, muQ_step;
    double muS_begin, muS_end, muS_step;

    // Sigma, zeta, delta, omega, phi, rho field ranges
    double sigma0_begin, sigma0_end, sigma0_step;
    double zeta0_begin, zeta0_end, zeta0_step;
    double delta0_begin, delta0_end, delta0_step;
    double omega0_begin, omega0_end, omega0_step;
    double phi0_begin, phi0_end, phi0_step;
    double rho0_begin, rho0_end, rho0_step;
    double Phi_order0_begin, Phi_order0_end, Phi_order0_step;

    // Couplings
    double d_betaQCD, f_pi, f_K;
    double gqu_sigma, gqu_zeta, gqu_delta, gqu_omega, gqu_phi, gqu_rho;
    double gqd_sigma, gqd_zeta, gqd_delta, gqd_omega, gqd_phi, gqd_rho;
    double gqs_sigma, gqs_zeta, gqs_delta, gqs_omega, gqs_phi, gqs_rho;
    double gQ_Phi_order, gB_Phi_order;

    // Mass parameters
    double pion_vacuum_mass, kaon_vacuum_mass, nucleon_vacuum_mass;
    double Lambda_vacuum_mass, Sigma_vacuum_mass, Delta_vacuum_mass;
    double Sigma_star_vacuum_mass, Omega_vacuum_mass;

    // Quark mass parameters
    double up_quark_bare_mass, down_quark_bare_mass, strange_quark_bare_mass;

    // Model-specific parameters
    double m_1, m_2, m_3H, m_3D;
    double k_0, k_1, k_2, k_3;
    double a_1, a_3, T0, alpha_X;

    // Nucleon couplings
    double gN_sigma, gN_zeta, g_4, gN_omega, gN_phi, gN_rho;

    // Mean field vacuum masses and other constants
    double mass0;
    double sigma_mean_field_vacuum_mass, zeta_mean_field_vacuum_mass;
    double omega_mean_field_vacuum_mass, phi_mean_field_vacuum_mass;
    double rho_mean_field_vacuum_mass;
    double chi_mean_field_vacuum_value;
    double hbarc3;
};

/**
 * @brief Class for managing and processing input parameters from a configuration YAML file.
 */
class input {
   public:
    /**
     * @brief Default constructor for the input class.
     */
    input();

    /**
     * @brief Sets the path to the configuration YAML file.
     * @param config_path The file path to the YAML configuration file.
     */
    void set_config_path(const std::string &config_path);

    /**
     * @brief Gets the current path of the configuration YAML file.
     * @return The file path as a string.
     */
    std::string get_config_path();

    /**
     * @brief Structure holding the input parameters parsed from the configuration YAML file.
     */
    static input_params_struct input_params;

    /**
     * @brief Initializes input parameters by reading and validating the YAML configuration file.
     */
    void create_input_params();

   private:
    /**
     * @brief Reads and parses a YAML configuration file.
     * @return YAML::Node representing the parsed YAML file contents.
     * @throws std::runtime_error if the file cannot be read or parsed.
     */
    YAML::Node read_YAML_file();

    /**
     * @brief Prints the contents of a YAML::Node to the console.
     */
    void print_YAML_node();

    /**
     * @brief Prints the input parameters parsed from the configuration YAML file.
     */
    void print_input_params();

    YAML::Node config_node;
    std::string config_path_;
};

#endif
