#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
file: yaml_preprocess.py
author: Nikolas Cruz-Camacho <cnc6@illinois.edu>
Purpose: Generate and validate a YAML configuration file for CMF++ using OpenAPI specifications.

This script automates the creation of a YAML configuration file for the CMF++ (Chiral Mean Field) module 
based on user input. The generated configuration is validated against the provided OpenAPI specification to 
ensure compliance. After validation, the configuration file is flattened to a simplified format suitable 
for further processing.

Functionality includes:
- Parsing command line arguments to specify paths for API specifications and configuration files.
- Validating the user-provided configuration file against the OpenAPI specification.
- Extracting the deepest key-value pairs from a nested YAML configuration.
- Flattening the configuration into a simplified format.
- Saving the simplified configuration to a new YAML file.

Usage:
    python yaml_preprocess.py --api_file_path <path_to_api_spec> --config_file_path <path_to_config> --validated_config_file_path <path_to_validated_config>

Arguments:
    --api_file_path: Path to the OpenAPI specification file (optional, defaults to ../api/OpenAPI_Specifications_CMF.yaml).
    --config_file_path: Path to the initial configuration file (optional, defaults to ../input/config.yaml).
    --validated_config_file_path: Path to the final validated and flattened configuration file (optional, defaults to ../input/validated_config.yaml).

Example:
    python yaml_preprocess.py --api_file_path ../api/OpenAPI_Specifications_CMF.yaml --config_file_path ../input/config.yaml --validated_config_file_path ../input/validated_config.yaml

"""

import argparse
import sys
import os
import yaml
import logging

from yaml_validation import validate_unmarshal_file

# Constants
BASE_DIR = os.path.dirname(__file__)
DEFAULT_API_FILE_PATH = os.path.join(
    BASE_DIR, "..", "api", "OpenAPI_Specifications_CMF.yaml"
)
DEFAULT_CONFIG_FILE_PATH = os.path.join(BASE_DIR, "..", "input", "config.yaml")
DEFAULT_VALIDATED_CONFIG_FILE_PATH = os.path.join(
    BASE_DIR, "..", "input", "validated_config.yaml"
)

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def extract_deepest_pairs(data: dict, prefix: str = "") -> list:
    """
    Recursively extracts the deepest key-value pairs from a nested dictionary.

    Parameters:
        data (dict): The nested dictionary.
        prefix (str): A prefix to be added to the keys.

    Returns:
        list: A list of tuples representing the deepest key-value pairs.
    """
    pairs = []
    for key, value in data.items():
        if isinstance(value, dict):
            pairs.extend(extract_deepest_pairs(value, prefix + key + "###"))
        else:
            pairs.append((prefix + key, value))
    return pairs


def flatten_config_yaml(input_filename: str, output_filename: str) -> None:
    """
    Flattens a nested YAML configuration file to a simplified format.

    Parameters:
        input_filename (str): The path to the original configuration file.
        output_filename (str): The path to the simplified configuration file.
    """
    try:
        with open(input_filename, "r") as file:
            original_data = yaml.safe_load(file)

        deepest_pairs = extract_deepest_pairs(original_data)
        simplified_data = {key.split("###")[-1]: value for key, value in deepest_pairs}

        simplified_data["run_name"] = (
            "C"
            + str(simplified_data["vector_potential"])
            + "_"
            + simplified_data["run_name"].replace(" ", "_")
        )

        # Don't debug in production runs!
        if simplified_data["production_run"] == True:
            simplified_data["output_debug"] = False

        with open(output_filename, "w") as file:
            yaml.dump(simplified_data, file)

        logging.info(f"Simplified YAML configuration saved to {output_filename}")

    except Exception as e:
        logging.error(f"Failed to flatten YAML configuration: {e}")
        sys.exit(1)


def main() -> None:
    """
    Entry point for the YAML preprocessing script.
    """
    parser = argparse.ArgumentParser(
        description="Create a YAML configuration file for CMF++ based on user input validated by the OpenAPI specification"
    )
    parser.add_argument(
        "--api_file_path",
        type=str,
        default=DEFAULT_API_FILE_PATH,
        help="Path to the OpenAPI specification file",
    )
    parser.add_argument(
        "--config_file_path",
        type=str,
        default=DEFAULT_CONFIG_FILE_PATH,
        help="Path to the initial configuration file",
    )
    parser.add_argument(
        "--validated_config_file_path",
        type=str,
        default=DEFAULT_VALIDATED_CONFIG_FILE_PATH,
        help="Path to the final configuration file",
    )

    args = parser.parse_args()

    if validate_unmarshal_file(
        spec_file_path=args.api_file_path,
        input_file_path=args.config_file_path,
        valid_file_path=args.config_file_path,
    ):
        flatten_config_yaml(args.config_file_path, args.validated_config_file_path)
    else:
        logging.error("Validation of the configuration file failed.")
        sys.exit(1)


if __name__ == "__main__":
    logging.info(f"Starting execution of {__file__}...")
    main()
    logging.info(f"Execution end of {__file__}.")
