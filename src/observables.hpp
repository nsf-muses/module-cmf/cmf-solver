#ifndef OBSERVABLES_HPP
#define OBSERVABLES_HPP

/**
 * @file observables.hpp
 * @brief (header) observables class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <iostream>

#include "input.hpp"
#include "particle_types.hpp"
#include "solution.hpp"

class observables : public input {
   public:
    observables();
    void compute_observables(quark_sector &Quarks, baryon_sector &Baryon_Octet, baryon_sector &Baryon_Decuplet,
                             solution &Sol);

   private:
    double vector_contribution_to_pressure;
    double Phi_order_contribution_to_pressure;
    double vector_contribution_to_energy;
    double scalar_contribution_to_energy;
    double sym_breaking_contribution_to_energy;
    double Phi_order_contribution_to_energy;

    double total_fermionic_pressure;
    double total_fermionic_energy;

    double total_omega_source_term;
    double total_phi_source_term;
    double total_rho_source_term;

    double total_vector_density;
    double total_strange_density;
    double total_charge_density;
};

#endif