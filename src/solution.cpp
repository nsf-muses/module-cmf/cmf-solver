#include "solution.hpp"

/**
 * @file solution.cpp
 * @brief (implementation) solution class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

/**
 * @brief Constructor for the solution class.
 */
solution::solution() {}

/**
 * @brief Update the mean field values with the provided parameters.
 *
 * @param sigma    The new value for sigma.
 * @param zeta     The new value for zeta.
 * @param delta    The new value for delta.
 * @param omega    The new value for omega.
 * @param phi      The new value for phi.
 * @param rho      The new value for rho.
 * @param Phi_order new value for Phi order.
 */
void solution::update_mean_fields(double sigma, double zeta, double delta, double omega, double phi, double rho,
                                  double Phi_order) {
    mean_fields = {sigma, zeta, delta, omega, phi, rho, Phi_order};
}

/**
 * @brief Update the residues with the provided parameters.
 *
 * @param sigma    The new value for sigma.
 * @param zeta     The new value for zeta.
 * @param delta    The new value for delta.
 * @param omega    The new value for omega.
 * @param phi      The new value for phi.
 * @param rho      The new value for rho.
 * @param Phi_order new value for Phi order.
 */
void solution::update_residues(double sigma, double zeta, double delta, double omega, double phi, double rho,
                               double Phi_order) {
    residues = {sigma, zeta, delta, omega, phi, rho, Phi_order};
}

/**
 * @brief Update the chemical potentials with Baryon, Strange, and Charge values.
 *
 * @param Baryon  The new Baryon value.
 * @param Strange The new Strange value.
 * @param Charge  The new Charge value.
 */
void solution::update_chemical_potentials(double Baryon, double Strange, double Charge) {
    chemical_potentials = {Baryon, Strange, Charge};
}

/**
 * @brief Print the mean field values to the provided output stream (default is cout).
 *
 * @param os The output stream to which the mean field values will be printed.
 */
void solution::print(ostream& os) {
    os << chemical_potentials.Baryon << '\t' << chemical_potentials.Strange << '\t' << chemical_potentials.Charge
       << '\t' << mean_fields.sigma << '\t' << mean_fields.zeta << '\t' << mean_fields.delta << '\t'
       << mean_fields.omega << '\t' << mean_fields.phi << '\t' << mean_fields.rho << '\t' << mean_fields.Phi_order
       << endl;
}

/**
 * @brief Calculate the mean squared difference between this solution and another solution.
 *
 * @param other The other solution for comparison.
 * @return The mean squared difference between the two solutions.
 */
double solution::calculate_mean_squared_difference(const solution& other) const {
    double squaredDiff = 0.0;

    squaredDiff += pow(mean_fields.sigma - other.mean_fields.sigma, 2);
    squaredDiff += pow(mean_fields.zeta - other.mean_fields.zeta, 2);
    squaredDiff += pow(mean_fields.delta - other.mean_fields.delta, 2);
    squaredDiff += pow(mean_fields.omega - other.mean_fields.omega, 2);
    squaredDiff += pow(mean_fields.phi - other.mean_fields.phi, 2);
    squaredDiff += pow(mean_fields.rho - other.mean_fields.rho, 2);
    squaredDiff += pow(mean_fields.Phi_order - other.mean_fields.Phi_order, 2);

    return squaredDiff / 7.0;
}
