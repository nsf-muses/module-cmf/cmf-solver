#ifndef PARTICLE_TYPES_HPP
#define PARTICLE_TYPES_HPP

/**
 * @file particle_types.hpp
 * @brief (header) Particle types classes for the Chiral Mean Field model.
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <cmath>
#include <iostream>
#include <string>

#include "PDG_reader.hpp"
#include "coupling_constants.hpp"
#include "particle.hpp"

class quark_sector {
   public:
    quark_sector();  // regular constructor, requires prior initialization
    void initialize(PDG_reader infile, coupling_constants& Coupling_Constants);

   public:
    vector<quark> particles;

    void compute_source_terms(double sigma, double zeta, double delta, double omega, double phi, double rho,
                              double Phi_order);

    void compute_total_vector_density();
    void compute_total_charge();
    void compute_total_strangeness();

    void compute_total_fermionic_energy();
    void compute_total_fermionic_pressure();

    void print();
    void reset();

    double sigma_source_term;
    double zeta_source_term;
    double delta_source_term;
    double omega_source_term;
    double phi_source_term;
    double rho_source_term;
    double Phi_order_source_term;

    double total_vector_density;
    double total_charge;
    double total_strangeness;

    double total_fermionic_pressure;
    double total_fermionic_energy;
};

class baryon_sector {
   public:
    baryon_sector();  // regular constructor, requires prior initialization
    void initialize(PDG_reader infile, coupling_constants& Coupling_Constants);

   public:
    vector<baryon> particles;

    void compute_source_terms(double sigma, double zeta, double delta, double omega, double phi, double rho,
                              double Phi_order);

    void compute_total_vector_density();
    void compute_total_charge();
    void compute_total_strangeness();

    void compute_total_fermionic_energy();
    void compute_total_fermionic_pressure();

    void print();
    void reset();

    double sigma_source_term;
    double zeta_source_term;
    double delta_source_term;
    double omega_source_term;
    double phi_source_term;
    double rho_source_term;
    double Phi_order_source_term;

    double total_fermionic_pressure;
    double total_fermionic_energy;

    double total_vector_density;
    double total_charge;
    double total_strangeness;
};

#endif