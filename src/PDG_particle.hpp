#ifndef PDG_PARTICLE_HPP
#define PDG_PARTICLE_HPP

/**
 * @file PDG_particle.hpp
 * @brief (header) PDG Particle class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <iostream>
#include <string>

#include "utils.hpp"

class PDG_Particle {
   public:
    int ID;
    string Name;
    double Mass;
    double Width;
    short int Degeneracy;
    double BaryonNumber;
    short int Strangeness;
    short int Charm;
    short int Bottomness;
    double Isospin;
    double Isospin_Z;
    double ElectricCharge;
    short int Decays;

    // constructor
    PDG_Particle() = delete;

    PDG_Particle(int _ID, string _Name, double _Mass, double _Width, short int _Degeneracy, double _BaryonNumber,
                 short int _Strangeness, short int _Charm, short int _Bottomness, double _Isospin, double _Isospin_Z,
                 double _ElectricCharge, short int _Decays)
        : ID(_ID),
          Name(_Name),
          Mass(_Mass),
          Width(_Width),
          Degeneracy(_Degeneracy),
          BaryonNumber(_BaryonNumber),
          Strangeness(_Strangeness),
          Charm(_Charm),
          Bottomness(_Bottomness),
          Isospin(_Isospin),
          Isospin_Z(_Isospin_Z),
          ElectricCharge(_ElectricCharge),
          Decays(_Decays) {}

    // print methods
    inline void printInfo() {
        cout << "\nID :\t" << ID << "\nName :\t" << Name << "\nMass :\t" << Mass << "\nWidth :\t" << Width
             << "\nDegeneracy :\t" << Degeneracy << "\nBaryon Number :\t" << BaryonNumber << "\nStrangeness :\t"
             << Strangeness << "\nCharm :\t" << Charm << "\nBottomness :\t" << Bottomness << "\nIsospin :\t" << Isospin
             << "\nIsospin_Z :\t" << Isospin_Z << "\nElectric Charge :\t" << ElectricCharge << "\nDecays :\t" << Decays
             << endl;
    }

    inline void printInfo_line() {
        cout << to_string(ID) + ';' + Name + ';' + to_string(Mass) + ';' + to_string(Degeneracy) + ';' +
                    to_string(BaryonNumber) + ';' + to_string(Strangeness) + ';' + to_string(Isospin) + ';' +
                    to_string(Isospin_Z) + ';' + to_string(ElectricCharge)
             << endl;
    }
};

#endif