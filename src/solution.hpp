#ifndef SOLUTION_HPP
#define SOLUTION_HPP

/**
 * @file solution.hpp
 * @brief (header) solution class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <cmath>
#include <iostream>

#include "utils.hpp"

/**
 * @file solution.hpp
 * @brief (header) solution class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

/**
 * @brief Structure to store mean field values.
 */
struct solution_struct {
    double sigma;
    double zeta;
    double delta;
    double omega;
    double phi;
    double rho;
    double Phi_order;
};

/**
 * @brief Structure to store chemical potentials.
 */
struct chemical_potential_struct {
    double Baryon;
    double Strange;
    double Charge;
};

/**
 * @brief Structure to store observables.

 */
struct observables_struct {
    double energy_density;
    double pressure;
    double vector_density;
    double vector_density_without_Phi_order;
    double strange_density;
    double charge_density;
    double total_quark_vector_density;
    double total_quark_strangeness_density;
    double total_quark_charge_density;
    double total_baryon_octet_vector_density;
    double total_baryon_octet_strangeness_density;
    double total_baryon_octet_charge_density;
    double total_baryon_decuplet_vector_density;
    double total_baryon_decuplet_strangeness_density;
    double total_baryon_decuplet_charge_density;
};

/**
 * @class solution
 * @brief Represents a solution in the Chiral Mean Field model.
 */
class solution {
   public:
    /**
     * @brief Constructor for the solution class.
     *
     * Regular constructor, requires prior initialization.
     */
    solution();

    /**
     * @brief Update the mean field values with the provided parameters.
     *
     * @param sigma    The new value for sigma.
     * @param zeta     The new value for zeta.
     * @param delta    The new value for delta.
     * @param omega    The new value for omega.
     * @param phi      The new value for phi.
     * @param rho      The new value for rho.
     * @param Phi_order The new value for Phi order.
     */
    void update_mean_fields(double sigma, double zeta, double delta, double omega, double phi, double rho,
                            double Phi_order);

    /**
     * @brief Update the residues with the provided parameters.
     *
     * @param sigma    The new value for sigma.
     * @param zeta     The new value for zeta.
     * @param delta    The new value for delta.
     * @param omega    The new value for omega.
     * @param phi      The new value for phi.
     * @param rho      The new value for rho.
     * @param Phi_order The new value for Phi order.
     */
    void update_residues(double sigma, double zeta, double delta, double omega, double phi, double rho,
                         double Phi_order);

    /**
     * @brief Update the chemical potentials with Baryon, Strange, and Charge values.
     *
     * @param Baryon  The new Baryon value.
     * @param Strange The new Strange value.
     * @param Charge  The new Charge value.
     */
    void update_chemical_potentials(double Baryon, double Strange, double Charge);

    /**
     * @brief Print the mean field values to the provided output stream (default is cout).
     *
     * @param os The output stream to which the mean field values will be printed.
     */
    void print(ostream& os = cout);

    /**
     * @brief Calculate the mean squared difference between this solution and another solution.
     *
     * @param other The other solution for comparison.
     * @return The mean squared difference between the two solutions.
     */
    double calculate_mean_squared_difference(const solution& other) const;

   public:
    solution_struct mean_fields;
    solution_struct residues;
    chemical_potential_struct chemical_potentials;
    observables_struct observables;
};

#endif
