#include "utils.hpp"

/**
 * @file utils.cpp
 * @brief (implementation) utility functions for the Chiral Mean Field model.
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

/**
 * @brief Check the existence of a file.
 *
 * @param[in] fname Name of the file to check for existence.
 * @return true if the file exists, false if it does not.
 */
bool file_exist(const string &fname) { return std::filesystem::exists(fname); }

/**
 * @brief Create the status file for the Calculation Engine.
 *
 * @param[in] status_value Numerical code error for the Calculation Engine
 * (200 for success, 500 for failure).
 * @param[in] file_path Path where the status file will be created.
 * @param[in] message Descriptive message to include in the status file.
 * @return void. Status file is created in the provided file_path.
 */
void create_status_file(const int status_value, const string &file_path, const string &message) {
    YAML::Emitter status;
    status << YAML::BeginMap;
    status << YAML::Key << "code";
    status << YAML::Value << status_value;
    status << YAML::Key << "message";
    status << YAML::Value << message;
    status << YAML::EndMap;

    ofstream fout(file_path);
    if (!fout.is_open()) {
        throw runtime_error("Error: Cannot open file.");
    }
    fout << status.c_str();
    fout.close();
}

/**
 * @brief Copy a file from the source path to the destination path with a new name.
 *
 * @param[in] source_path Path of the source file.
 * @param[in] destination_path Path of the destination file with the new name.
 * @return true if the file was copied successfully, false otherwise.
 */
bool copy_file(const string &source_path, const string &destination_path) {
    // Open source file for reading
    ifstream source_file(source_path, std::ios::binary);
    if (!source_file) {
        cerr << "Error opening source file" << endl;
        return false;
    }

    // Open destination file for writing
    ofstream destination_file(destination_path, std::ios::binary);
    if (!destination_file) {
        cerr << "Error opening destination file" << endl;
        return false;
    }

    // Copy contents of source file to destination file
    destination_file << source_file.rdbuf();

    source_file.close();
    destination_file.close();

    return true;
}

/**
 * @brief Open a file for writing.
 *
 * @param[in] filePath Path of the file to open.
 * @param[out] outFile Pointer to the opened file.
 * @return void. Exits with error if the file could not be opened.
 */
void open_file(const string &filePath, FILE *&outFile) {
    outFile = fopen(filePath.c_str(), "w");
    if (outFile == nullptr) {
        cerr << "Error: Could not open file: " << filePath << endl;
        exit(1);
    }
}

/**
 * @brief Print a progress bar in the console.
 *
 * @param[in] counter Current progress count.
 * @param[in] Initial_points Total number of points for the progress calculation.
 * @return void. Prints the progress bar to the console.
 */
void print_progress_bar(int counter, int Initial_points) {
    int barWidth = 50;  // Width of the progress bar
    if (counter % (Initial_points / 100) == 0 || counter == Initial_points) {
        // Calculate the progress percentage
        float progress = static_cast<float>(counter) / static_cast<float>(Initial_points);
        int filledWidth = static_cast<int>(round(progress * static_cast<float>(barWidth)));

        // Ensure that filledWidth doesn't exceed barWidth
        if (filledWidth > barWidth) {
            filledWidth = barWidth;
        }

        // Use a carriage return to move to the start of the line
        cout << "\r";

        // Print the progress bar
        cout << "[";
        for (int i = 0; i < barWidth; i++) {
            if (i < filledWidth)
                cout << "=";
            else
                cout << ".";
        }

        // Format the percentage and progress output to maintain consistent width
        cout << "] " << std::setw(3) << static_cast<int>(round(progress * 100)) << "%  Progress: " << std::setw(8)
             << counter << " / " << Initial_points;

        // Add enough spaces to clear any previous longer lines
        cout << "            ";

        // Flush the output
        cout.flush();
    }
}
