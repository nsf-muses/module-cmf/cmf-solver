#ifndef CMF_HPP
#define CMF_HPP

/**
 * @file cmf.hpp
 * @brief (header) Chiral Mean Field model class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <sys/stat.h>
#include <sys/types.h>

#include <cerrno>
#include <cstring>
#include <iostream>
#include <stdexcept>

#include "fsolve.hpp"
#include "input.hpp"
#include "observables.hpp"
#include "particle_types.hpp"
#include "solution.hpp"
#include "utils.hpp"

struct cmf_constant_parameters {
    double pi_mass_squared_f_pi;
    double k_0_chi2;
    double k3_chi;
    double eps_chi4;
    double omega_mass_squared;
    double k_mass_fk_minus_pi_mass_f_pi;
    double phi_mass_squared;
    double rho_mass_squared;
    double muB4;
    double tnull4;
};

void CMF_system(int n, double x[], double fvec[]);

class cmf {
   public:
    // Constructor
    cmf() = delete;

    cmf(string cfg_file_path, string out_path, string pdg_table, string pdg_quark_table)
        : config_file_path(move(cfg_file_path)),
          root_output_path(move(out_path)),
          PDG_table_path(move(pdg_table)),
          PDG_quark_table_path(move(pdg_quark_table)) {}

    // Objects
    input Input;
    quark_sector Quarks;
    baryon_sector Baryon_Octet;
    baryon_sector Baryon_Decuplet;

    // Structures
    cmf_constant_parameters CMF_Constant_Parameters;

    // Methods
    void read_config_file();
    void compute_fields_and_chemical_potential_divisions();
    void calculate_CMF_constant_parameters();
    void iterate_over_chemical_potentials();

    void open_output_files();
    void close_output_files();

   private:
    // Objects
    coupling_constants Couplings;

    // Command line arguments
    string config_file_path;
    string root_output_path;
    string PDG_table_path;
    string PDG_quark_table_path;

    // Constants
    int Iteration_counter = 0;
    int Total_points = 1;

    // Output files
    FILE* OutFile_EoS = nullptr;
    FILE* OutFile_debug = nullptr;
    FILE* OutFile_particle_properties = nullptr;

    // Chemical potentials divisions
    double muB0_divisions;
    double muS0_divisions;
    double muQ0_divisions;

    // Fields divisions
    double sigma0_divisions;
    double zeta0_divisions;
    double delta0_divisions;
    double omega0_divisions;
    double phi0_divisions;
    double rho0_divisions;
    double Phi_order0_divisions;

    // PDG particle ids
    vector<size_t> particle_ids_quarks = {1, 2, 3};
    vector<size_t> particle_ids_octet = {2212, 2112, 3122, 3222, 3212, 3112, 3322, 3312};
    vector<size_t> particle_ids_decuplet = {2224, 2214, 2114, 1114, 3224, 3214, 3114, 3324, 3314, 3334};

    // Methods
    void initialize_coupling_constants();

    void initialize_particle_sectors();

    void remove_hyerons_from_particle_ids();

    void update_mean_fields_and_residues(double x_init[], double f_vec_init[], solution& Current_Solution);

    int calculate_divisions(double begin, double end, double step);
    void display_initial_point_divisions();
    int calculate_total_points();

    void update_particles_chemical_potentials(double muB, double muS, double muQ);
    void set_initial_guess(double* x_init, int sigma0_i, int zeta0_i, int delta0_i, int omega0_i, int phi0_i,
                           int rho0_i, int Phi_order0_i);

    void iterate_over_fields(solution& Current_Solution, vector<solution>& Solutions_per_chemical_potential);

    void handle_solution(const solution& Current_Solution, vector<solution>& Solutions_per_chemical_potential);
    bool is_solution_valid_in_domain(const solution& Current_Solution);
    bool is_solution_unique(const solution& Current_Solution, const vector<solution>& Solutions_per_chemical_potential);
    bool is_solution_physical(const solution& Current_Solution);

    void write_solution_to_output(FILE* outFile, const solution& Sol);
    void write_solution_to_particle_properties(FILE* outFile, const solution& Sol);
};

#endif