#ifndef FSOLVE_HPP
#define FSOLVE_HPP

#pragma once

#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <limits>

void dogleg(int n, double r[], double diag[], double qtb[], double delta, double x[], double wa1[], double wa2[]);
double enorm(int n, double x[]);
void fdjac1(void fcn(int n, double x[], double f[], void *params), int n, double x[], double fvec[], void *params,
            double fjac[], int ldfjac, int ml, int mu, double epsfcn, double wa1[], double wa2[]);
int fsolve(void fcn(int n, double x[], double fvec[], void *params), int n, double x[], double fvec[], void *params,
           double tol, double wa[], int lwa);
int hybrd(void fcn(int n, double x[], double fvec[], void *params), int n, double x[], double fvec[], void *params,
          double xtol, int maxfev, int ml, int mu, double epsfcn, double diag[], int mode, double factor, int nfev,
          double fjac[], int ldfjac, double r[], int lr, double qtf[], double wa1[], double wa2[], double wa3[],
          double wa4[]);
void qform(int m, int n, double q[], int ldq);
void qrfac(int m, int n, double a[], int lda, bool pivot, int ipvt[], double rdiag[], double acnorm[]);
void r1mpyq(int m, int n, double a[], int lda, double v[], double w[]);
bool r1updt(int m, int n, double s[], double u[], double v[], double w[]);

#endif