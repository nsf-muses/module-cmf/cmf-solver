#ifndef BASE_COUPLINGS_HPP
#define BASE_COUPLINGS_HPP

/**
 * @file coupling_constants.hpp
 * @brief (header) coupling constants class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <unordered_map>

#include "input.hpp"
#include "yaml-cpp/yaml.h"

struct couplings {
    double g_sigma;
    double g_zeta;
    double g_delta;
    double g_omega;
    double g_phi;
    double g_rho;
    double g_Phi_order;
};

class coupling_constants : public input {
   public:
    coupling_constants();
    void create_couplings_and_bare_masses();

    typedef unordered_map<int, couplings> couplings_map_t;
    typedef unordered_map<int, double> bare_mass_map_t;
    couplings_map_t couplings_map;
    bare_mass_map_t bare_masses_map;
};

#endif