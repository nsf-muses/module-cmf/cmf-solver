#include "PDG_reader.hpp"

/**
 * @file PDG_reader.cpp
 * @brief (implementation) PDG particle reader class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

PDG_reader::PDG_reader(const string &filename, const vector<size_t> &desired_particles_IDs) {
    vector<PDG_Particle> temp_read_particles;
    vector<int> temp_particle_IDs;

    if (file_exist(filename)) {
        ifstream infile;
        string line;
        int line_counter = 0;

        infile.open(filename);

        while (getline(infile, line)) {
            stringstream ss(line);
            int ID;
            string Name;
            double Mass;
            double Width;
            short int Degeneracy;
            double BaryonNumber;
            short int Strangeness;
            short int Charm;
            short int Bottomness;
            double Isospin;
            double Isospin_Z;
            double ElectricCharge;
            short int Decays;

            ++line_counter;

            // Check that line is valid in all its entries
            if (!(ss >> ID >> Name >> Mass >> Width >> Degeneracy >> BaryonNumber >> Strangeness >> Charm >>
                  Bottomness >> Isospin >> Isospin_Z >> ElectricCharge >> Decays)) {
                cout << "Invalid entry in PGD table, line " << line_counter << endl;
                exit(EXIT_FAILURE);
            }

            // Split stringstream
            ss >> ID >> Name >> Mass >> Width >> Degeneracy >> BaryonNumber >> Strangeness >> Charm >> Bottomness >>
                Isospin >> Isospin_Z >> ElectricCharge >> Decays;

            // Transform 0.333 and 0.666 into 1/3 and 2/3
            if (fabs(BaryonNumber - 1. / 3.) < 1.e-2) BaryonNumber = 1. / 3.;
            if (fabs(BaryonNumber + 1. / 3.) < 1.e-2) BaryonNumber = -1. / 3.;
            if (fabs(ElectricCharge - 1. / 3.) < 1.e-2) ElectricCharge = 1. / 3.;
            if (fabs(ElectricCharge + 1. / 3.) < 1.e-2) ElectricCharge = -1. / 3.;
            if (fabs(ElectricCharge - 2. / 3.) < 1.e-2) ElectricCharge = 2. / 3.;
            if (fabs(ElectricCharge + 2. / 3.) < 1.e-2) ElectricCharge = -2. / 3.;

            if (find(desired_particles_IDs.begin(), desired_particles_IDs.end(), ID) != desired_particles_IDs.end()) {
                // If ID is also in desired_particles_IDs then add it

                // Initialize a Particle object with the fields read from the line and
                // add the object to the vector, converting GeV to MeV
                temp_read_particles.push_back(PDG_Particle(ID, Name, 1000 * Mass, 1000 * Width, Degeneracy,
                                                           BaryonNumber, Strangeness, Charm, Bottomness, Isospin,
                                                           Isospin_Z, ElectricCharge, Decays));
                temp_particle_IDs.push_back(ID);
            }
        }

        infile.close();

        // Sort temp_read_particles which uses temp_particle_IDs order into
        // desired_particles_IDs order
        for (vector<size_t>::size_type i = 0; i < desired_particles_IDs.size(); i++) {
            vector<int>::size_type j = get_position_from_ID(temp_particle_IDs, desired_particles_IDs[i]);

            if (j < temp_particle_IDs.size()) {  // Check if j is valid
                read_particles.push_back(temp_read_particles[j]);
                particle_IDs.push_back(temp_read_particles[j].ID);
            } else {
                cout << "WARNING: Desired particle ID " << desired_particles_IDs[i] << " not found." << endl;
            }
        }

        // Check that all particles inside desired_particles_IDs were read and
        // report with a message the ones that do not
        if (desired_particles_IDs.size() != read_particles.size()) {
            for (vector<size_t>::size_type i = 0; i < desired_particles_IDs.size(); i++) {
                if (find(particle_IDs.begin(), particle_IDs.end(), desired_particles_IDs[i]) == particle_IDs.end()) {
                    cout << "Particle " << desired_particles_IDs[i] << " not found in PGD " << filename << endl;
                }
            }
        }

    } else {
        cout << "Unexistent PGD table file: " << filename << endl;
        exit(EXIT_FAILURE);
    }
}

void PDG_reader::print_read_particles() {
    for (vector<PDG_Particle>::size_type i = 0; i < read_particles.size(); i++) {
        read_particles[i].printInfo_line();
    }
}

vector<int>::size_type PDG_reader::get_particle_position_from_ID(const int _ID) {
    auto it = find(particle_IDs.begin(), particle_IDs.end(), _ID);

    if (it != particle_IDs.end()) {
        return static_cast<vector<int>::size_type>(it - particle_IDs.begin());  // Cast to the correct type
    } else {
        cout << "WARNING: value " << _ID << " _ID not found" << endl;
        return particle_IDs.size();  // Indicate failure
    }
}

vector<int>::size_type get_position_from_ID(const vector<int> &P_IDs, const size_t _ID) {
    auto it = find(P_IDs.begin(), P_IDs.end(), _ID);

    if (it != P_IDs.end()) {
        return static_cast<vector<int>::size_type>(it - P_IDs.begin());  // Cast to the correct type
    } else {
        cout << "WARNING: value " << _ID << " _ID not found" << endl;
        return P_IDs.size();  // Indicate failure
    }
}
