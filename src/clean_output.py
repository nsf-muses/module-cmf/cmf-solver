#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
file: clean_output.py
author: Nikolas Cruz-Camacho <cnc6@illinois.edu>
Purpose: Clean and process CMF++ output files.

This script reads CMF++ output data, performs several cleaning and filtering operations, 
and saves the processed data into separate CSV files. The script processes both the 
main CMF++ output file and the CMF++ output particle properties file, if it exists. 

The script supports the following functionalities:
- Reading configuration from a YAML file.
- Loading and reading CMF++ output CSV files into pandas DataFrames.
- Filtering data based on specific criteria (e.g., removing data points with negative pressure gradients).
- Splitting data into stable, metastable, and unstable categories.
- Saving processed data into separate CSV files.

Usage:
    python clean_output.py --validated_config_file_path <path_to_config> --root_output_path <path_to_output_folder>

Arguments:
    --validated_config_file_path: Path to the validated configuration file (optional). If not provided, 
                                  it defaults to a predefined path.
    --root_output_path: Path to the root output folder where the CMF++ output files are stored.

Example:
    python clean_output.py --validated_config_file_path ../input/validated_config.yaml --root_output_path ../output

Note:
    The script assumes the existence of specific output files and directories as configured in the YAML file.

"""

import argparse
import os
import sys
import yaml
import pandas as pd
import numpy as np
import logging
import warnings

# Constants
SEPARATOR = ","
CMF_VARIABLES = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "nS",
    "nQ",
    "energy_density",
    "pressure",
    "entropy",
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
    "vector_density_without_Phi_order",
    "quark_vector_density",
    "octet_vector_density",
    "decuplet_vector_density",
]

PARTICLE_PROPERTIES_VARIABLES = [
    "temperature",
    "muB",
    "muS",
    "muQ",
    "nB",
    "nS",
    "nQ",
    "energy_density",
    "pressure",
    "entropy",
    "sigma_mean_field",
    "zeta_mean_field",
    "delta_mean_field",
    "omega_mean_field",
    "phi_mean_field",
    "rho_mean_field",
    "Phi_order_field",
    "vector_density_without_Phi_order",
    "quark_vector_density",
    "octet_vector_density",
    "decuplet_vector_density",
    "up_quark_mass",
    "down_quark_mass",
    "strange_quark_mass",
    "up_quark_effective_mass",
    "down_quark_effective_mass",
    "strange_quark_effective_mass",
    "up_quark_chemical_potential",
    "down_quark_chemical_potential",
    "strange_quark_chemical_potential",
    "up_quark_effective_chemical_potential",
    "down_quark_effective_chemical_potential",
    "strange_quark_effective_chemical_potential",
    "up_quark_vector_density",
    "down_quark_vector_density",
    "strange_quark_vector_density",
    "up_quark_potential",
    "down_quark_potential",
    "strange_quark_potential",
    "proton_mass",
    "neutron_mass",
    "Lambda_mass",
    "Sigma+_mass",
    "Sigma0_mass",
    "Sigma-_mass",
    "Xi0_mass",
    "Xi-_mass",
    "proton_effective_mass",
    "neutron_effective_mass",
    "Lambda_effective_mass",
    "Sigma+_effective_mass",
    "Sigma0_effective_mass",
    "Sigma-_effective_mass",
    "Xi0_effective_mass",
    "Xi-_effective_mass",
    "proton_chemical_potential",
    "neutron_chemical_potential",
    "Lambda_chemical_potential",
    "Sigma+_chemical_potential",
    "Sigma0_chemical_potential",
    "Sigma-_chemical_potential",
    "Xi0_chemical_potential",
    "Xi-_chemical_potential",
    "proton_effective_chemical_potential",
    "neutron_effective_chemical_potential",
    "Lambda_effective_chemical_potential",
    "Sigma+_effective_chemical_potential",
    "Sigma0_effective_chemical_potential",
    "Sigma-_effective_chemical_potential",
    "Xi0_effective_chemical_potential",
    "Xi-_effective_chemical_potential",
    "proton_vector_density",
    "neutron_vector_density",
    "Lambda_vector_density",
    "Sigma+_vector_density",
    "Sigma0_vector_density",
    "Sigma-_vector_density",
    "Xi0_vector_density",
    "Xi-_vector_density",
    "proton_potential",
    "neutron_potential",
    "Lambda_potential",
    "Sigma+_potential",
    "Sigma0_potential",
    "Sigma-_potential",
    "Xi0_potential",
    "Xi-_potential",
    "Delta++_mass",
    "Delta+_mass",
    "Delta0_mass",
    "Delta-_mass",
    "Sigma*+_mass",
    "Sigma*0_mass",
    "Sigma*-_mass",
    "Xi*0_mass",
    "Xi*-mass",
    "Omega_mass",
    "Delta++_effective_mass",
    "Delta+_effective_mass",
    "Delta0_effective_mass",
    "Delta-_effective_mass",
    "Sigma*+_effective_mass",
    "Sigma*0_effective_mass",
    "Sigma*-_effective_mass",
    "Xi*0_effective_mass",
    "Xi*-effective_mass",
    "Omega_effective_mass",
    "Delta++_chemical_potential",
    "Delta+_chemical_potential",
    "Delta0_chemical_potential",
    "Delta-_chemical_potential",
    "Sigma*+_chemical_potential",
    "Sigma*0_chemical_potential",
    "Sigma*-_chemical_potential",
    "Xi*0_chemical_potential",
    "Xi*-chemical_potential",
    "Omega_chemical_potential",
    "Delta++_effective_chemical_potential",
    "Delta+_effective_chemical_potential",
    "Delta0_effective_chemical_potential",
    "Delta-_effective_chemical_potential",
    "Sigma*+_effective_chemical_potential",
    "Sigma*0_effective_chemical_potential",
    "Sigma*-_effective_chemical_potential",
    "Xi*0_effective_chemical_potential",
    "Xi*-effective_chemical_potential",
    "Omega_effective_chemical_potential",
    "Delta++_vector_density",
    "Delta+_vector_density",
    "Delta0_vector_density",
    "Delta-_vector_density",
    "Sigma*+_vector_density",
    "Sigma*0_vector_density",
    "Sigma*-_vector_density",
    "Xi*0_vector_density",
    "Xi*-vector_density",
    "Omega_vector_density",
    "Delta++_potential",
    "Delta+_potential",
    "Delta0_potential",
    "Delta-_potential",
    "Sigma*+_potential",
    "Sigma*0_potential",
    "Sigma*-_potential",
    "Xi*0_potential",
    "Xi*-potential",
    "Omega_potential",
]

HBARC = 197.3269804  # [MeV fm]

DEFAULT_VALIDATED_CONFIG_FILE_PATH = os.path.join(
    os.path.dirname(__file__), "..", "input", "validated_config.yaml"
)
DEFAULT_ROOT_OUTPUT_PATH = os.path.join(os.path.dirname(__file__), "..", "output")

# Configure logging
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def compute_derivative(y, x, order):
    """
    Recursive function to calculate the nth derivative of a function y(x) using np.gradient
    input:
        y: function to be differentiated
        x: independent variable
        order: order of the derivative
    output:
        order nth derivative of y(x)
    """

    # Suppress specific warnings
    warnings.filterwarnings("ignore", category=RuntimeWarning)

    try:
        if len(y) == 0 or len(x) == 0:
            return np.zeros_like(x)

        for _ in range(order):
            y = np.gradient(y, x, edge_order=2)

        return y
    except Exception as e:
        logging.warning(f"Error computing derivative: {e}")
        return np.zeros_like(x)


def load_configuration(
    config_file_path: str = DEFAULT_VALIDATED_CONFIG_FILE_PATH,
) -> dict:
    """
    Load configuration from a config.yaml file.

    Args:
        config_file_path (str): Path to the configuration file.

    Returns:
        dict: Loaded configuration data.
    """
    try:
        with open(config_file_path, "r") as file:
            yaml_data = yaml.load(file, Loader=yaml.FullLoader)
        return yaml_data
    except FileNotFoundError:
        logging.error(f"Configuration file {config_file_path} not found.")
        sys.exit(1)
    except yaml.YAMLError as e:
        logging.error(f"Error parsing YAML file {config_file_path}: {e}")
        sys.exit(1)


def save_DataFrame(df: pd.DataFrame, file_path: str) -> None:
    """
    Save a DataFrame to a CSV file.

    Args:
        df (pd.DataFrame): The DataFrame to save.
        file_path (str): The path to the output CSV file.
    """
    df.to_csv(
        file_path, index=False, header=False, float_format="%0.12E", sep=SEPARATOR
    )
    if os.path.exists(file_path):
        logging.info(f"Saved {file_path}")


def remove_non_positive_monotonic(
    df: pd.DataFrame, x="muB", y="pressure"
) -> pd.DataFrame:

    df = df.sort_values(by=x)

    while True:

        # Create a boolean mask for rows where 'pressure' is increasing or constant
        pressure_monotonic = df[y].diff().fillna(0)

        if np.any(pressure_monotonic < 0):
            df = df[pressure_monotonic >= 0]
        else:
            break

    return df


def remove_non_positive_chi2(df: pd.DataFrame, X="B") -> pd.DataFrame:

    while True:

        df = compute_chi2_for_X(df, X=X)

        if np.any(df["chi2_" + X] < 0):
            df = df[df["chi2_" + X] >= 0]
        else:
            break

    return df


def compute_d_y_d_x(df, y="pressure", x="nB"):

    # sort by x
    df = df.sort_values(by=x)

    # compute derivative of y with respect to x
    df["d_" + y + "_d_" + x] = compute_derivative(df[y], df[x], 1)

    return df


def compute_chi2_for_X(df, X="B"):
    """
    Compute the second derivative (chi2) of a quantity nX with respect to muX.
    Assumes the DataFrame has columns 'nX' and 'muX'.
    """
    # Sort values by 'muX'
    df = df.sort_values(by="mu" + X).copy()

    # Compute chi2_X derivative
    df["chi2_" + X] = compute_derivative(df["n" + X], df["mu" + X], 1)

    return df


def compute_all_chi2(df, X):
    """
    Generalized function to compute chi2_X for constant group columns.

    Args:
        df (DataFrame): Input DataFrame.
        X (str): The variable to compute chi2 for ('B', 'S', 'Q').

    Returns:
        DataFrame: DataFrame with chi2_X computed.
    """

    if X == "B":
        group_cols = ["muS", "muQ"]
    elif X == "S":
        group_cols = ["muB", "muQ"]
    elif X == "Q":
        group_cols = ["muB", "muS"]

    # Group by specified columns, compute chi2_X, and concatenate results
    df_with_chi2 = (
        df.groupby(group_cols, group_keys=False)
        .apply(lambda group: compute_chi2_for_X(group, X=X))
        .reset_index(drop=True)
    )

    return df_with_chi2


def compute_chi11_for_XY(df, X="B", Y="Q"):
    """
    Compute the second derivative (chi11) of a quantity nX with respect to muY.
    Assumes the DataFrame has columns 'nX' and 'muY'.
    """
    # Sort values by 'muY'
    df = df.sort_values(by="mu" + Y).copy()

    # Compute chi11_XY derivative
    df["chi11_" + X + Y] = compute_derivative(df["n" + X], df["mu" + Y], 1)

    return df


def compute_all_chi11(df, X, Y):
    """
    Generalized function to compute chi11_XY for constant group columns.

    Args:
        df (DataFrame): Input DataFrame.
        X (str): The first variable to compute chi11 for ('B', 'S', 'Q').
        Y (str): The second variable to compute chi11 for ('B', 'S', 'Q').

    Returns:
        DataFrame: DataFrame with chi11_XY computed.
    """

    if Y == "B":
        group_cols = ["muS", "muQ"]
    elif Y == "S":
        group_cols = ["muB", "muQ"]
    elif Y == "Q":
        group_cols = ["muB", "muS"]

    # Group by specified columns, compute chi11_XY, and concatenate results
    df_with_chi11 = (
        df.groupby(group_cols, group_keys=False)
        .apply(lambda group: compute_chi11_for_XY(group, X=X, Y=Y))
        .reset_index(drop=True)
    )

    return df_with_chi11


def divide_by_sign(df, x=None):

    if x not in df.columns:
        logging.error(f"Column {x} not found in DataFrame.")
        return df

    return df[df[x] > 0], df[df[x] <= 0]


def get_highest_and_second_highest(*dataframes, X="B", y="pressure"):
    highest_points = []
    second_highest_points = []

    # Combine the 'muB' columns from all dataframes to find unique values
    unique_muXs = pd.concat([df["mu" + X] for df in dataframes]).unique()

    for muX in unique_muXs:
        # Combine rows with the same 'muB' value from all dataframes
        combined = pd.concat([df[df["mu" + X] == muX] for df in dataframes])

        if len(combined) >= 2:
            highest_point = combined.nlargest(1, y)
            second_highest_point = combined.nlargest(2, y).iloc[1:2]

            highest_points.append(highest_point)
            second_highest_points.append(second_highest_point)
        elif len(combined) == 1:
            highest_point = combined.nlargest(1, y)
            highest_points.append(highest_point)

    if len(highest_points) == 0:
        highest_points_df = pd.DataFrame(columns=dataframes[0].columns)
    else:
        highest_points_df = pd.concat(highest_points).reset_index(drop=True)

    if len(second_highest_points) == 0:
        second_highest_points_df = pd.DataFrame(columns=highest_points_df.columns)
    else:
        second_highest_points_df = pd.concat(second_highest_points).reset_index(
            drop=True
        )

    return highest_points_df, second_highest_points_df


def separate_stable_from_metastable(df_baryons, df_quarks, df_vacuum, X="B"):

    highest_points = []
    second_highest_points = []

    highest, second_highest = get_highest_and_second_highest(
        df_baryons, df_quarks, df_vacuum, X=X
    )

    highest_points.append(highest)
    second_highest_points.append(second_highest)

    if len(highest_points) == 0:
        highest_points_df = pd.DataFrame(columns=df_baryons.columns)
    else:
        highest_points_df = pd.concat(highest_points).reset_index(drop=True)

    if len(second_highest_points) == 0:
        second_highest_points_df = pd.DataFrame(columns=df_baryons.columns)
    else:
        second_highest_points_df = pd.concat(second_highest_points).reset_index(
            drop=True
        )

    return highest_points_df, second_highest_points_df


def split_df_into_baryons_quarks_vacuum(df):

    df_baryons = df[
        ((df["octet_vector_density"] > 0) & (df["quark_vector_density"] == 0))
        | ((df["decuplet_vector_density"] > 0) & (df["quark_vector_density"] == 0))
    ]

    df_quarks = df[
        (df["quark_vector_density"] > 0)
        & (df["octet_vector_density"] == 0)
        & (df["decuplet_vector_density"] == 0)
    ]

    df_vacuum = df[
        (df["quark_vector_density"] == 0)
        & (df["octet_vector_density"] == 0)
        & (df["decuplet_vector_density"] == 0)
    ]

    return df_baryons, df_quarks, df_vacuum


def filter_vacuum_to_matter_transitions(df):

    df_baryons, df_quarks, df_vacuum = split_df_into_baryons_quarks_vacuum(df)

    # Add a 'state' column to indicate the type of each row
    if not df_baryons.empty:
        df_baryons = df_baryons.copy()
        df_baryons.loc[:, "state"] = "baryon"

    if not df_quarks.empty:
        df_quarks = df_quarks.copy()
        df_quarks.loc[:, "state"] = "quark"

    if not df_vacuum.empty:
        df_vacuum = df_vacuum.copy()
        df_vacuum.loc[:, "state"] = "vacuum"

    # Combine the dataframes back together
    dfs_to_concat = []
    if not df_baryons.empty:
        dfs_to_concat.append(df_baryons)
    if not df_quarks.empty:
        dfs_to_concat.append(df_quarks)
    if not df_vacuum.empty:
        dfs_to_concat.append(df_vacuum)

    if len(dfs_to_concat) == 0:
        return pd.DataFrame(columns=df.columns)

    df_combined = pd.concat(dfs_to_concat)
    df_combined = df_combined.sort_values(by="muB").reset_index(drop=True)

    # Initialize the filtered dataframe with the first row
    filtered_rows = [df_combined.iloc[0]]

    # Iterate over the rows starting from the second one
    for i in range(1, len(df_combined)):
        current_row = df_combined.iloc[i]
        previous_row = filtered_rows[-1]

        if previous_row["state"] == "vacuum":
            if current_row["state"] in ["vacuum", "baryon", "quark"]:
                filtered_rows.append(current_row)

        elif previous_row["state"] == "baryon":
            if current_row["state"] in ["baryon", "quark"]:
                filtered_rows.append(current_row)

        elif previous_row["state"] == "quark":
            if current_row["state"] == "quark":
                filtered_rows.append(current_row)

    # Create the filtered dataframe
    df_filtered = pd.DataFrame(filtered_rows).reset_index(drop=True)
    return df_filtered


def subset_difference(A: pd.DataFrame, B: pd.DataFrame) -> pd.DataFrame:
    """
    Returns the subset of points in DataFrame A minus the subset B.

    Parameters:
    A (pd.DataFrame): The main DataFrame.
    B (pd.DataFrame): The subset DataFrame to be subtracted from A.

    Returns:
    pd.DataFrame: The resulting DataFrame after subtracting B from A.
    """

    # Check if B is a subset of A by comparing rows
    subset_mask = B.apply(lambda row: row.to_numpy() in A.to_numpy(), axis=1)
    if not subset_mask.all():
        rows_not_in_A = B[~subset_mask]
        raise ValueError(
            f"The following rows in B are not present in A:\n{rows_not_in_A}"
        )

    # Perform the difference operation
    C = pd.concat([A, B]).drop_duplicates(keep=False)

    return C


def read_file(filename_in: str, variables: list) -> pd.DataFrame:

    logging.info(f"Starting cleaning of {filename_in}...")

    try:
        df = pd.read_csv(
            filename_in, sep=SEPARATOR, header=None, names=variables, engine="python"
        )

        # if dataframe is empty, print warning
        if df.empty:
            logging.warning(f"No data in {filename_in}")
            sys.exit(1)

        if variables is not None:
            df = df[variables]
        return df
    except FileNotFoundError:
        logging.error(f"File {filename_in} not found.")
        sys.exit(1)
    except pd.errors.ParserError as e:
        logging.error(f"Error parsing CSV file {filename_in}: {e}")
        sys.exit(1)

    return df


def compare_pressure_difference(
    df_a, df_b, key_column="muB", comparison_column="pressure"
):
    # Identify overlapping muB values
    # overlapping_muBs = pd.merge(df_a[[key_column]], df_b[[key_column]], on=key_column)

    # Merge the two DataFrames on the overlapping muB values
    merged_df = pd.merge(df_a, df_b, on=key_column, suffixes=("_A", "_B"))

    # Compute the difference for pressure
    merged_df["pressure_diff"] = (
        merged_df[f"{comparison_column}_A"] - merged_df[f"{comparison_column}_B"]
    )

    # Detect sign changes (positive to negative or vice versa)
    sign_change = (merged_df["pressure_diff"] * merged_df["pressure_diff"].shift(1)) < 0
    sign_change_muB = merged_df.loc[sign_change, key_column].tolist()

    if sign_change.any():
        # "Sign changes detected in pressure difference"
        merged_df["sign_change"] = sign_change
        return True, sign_change_muB, merged_df["pressure_diff"]
    else:
        # "No sign changes detected in pressure difference"
        merged_df["sign_change"] = False
        return False, [], merged_df["pressure_diff"]


def clean_df_along_muX_dimension(df: pd.DataFrame, X="B") -> pd.DataFrame:
    """
    Process a CMF++ output file, split the data into stable, metastable, and unstable categories.

    Args:
        df (pd.DataFrame): The DataFrame containing the data to process.
        output_directory (str): The directory to save the cleaned data files.
        filename_in (str): The input file to process.
        X (str): The dimension to process (either 'B' or 'S' or 'Q').
    """

    df_baryons, df_quarks, df_vacuum = split_df_into_baryons_quarks_vacuum(df)

    # compute dP/dnX
    df_baryons = compute_d_y_d_x(df_baryons, y="pressure", x="n" + X)
    df_quarks = compute_d_y_d_x(df_quarks, y="pressure", x="n" + X)
    # df_vacuum = compute_d_y_d_x(df_vacuum, y='pressure', x='n'+X)

    df_baryons_stable_and_metastable, __ = divide_by_sign(
        df_baryons, x="d_pressure_d_n" + X
    )
    df_quarks_stable_and_metastable, __ = divide_by_sign(
        df_quarks, x="d_pressure_d_n" + X
    )
    # df_vacuum, df_vacuum = divide_by_sign(df_vacuum, x='d_pressure_d_nB') # vacuum d_pressure_d_nB doesn't make sense because nB = 0

    # remove non-monotonic points x=muB, y=pressure
    df_baryons_stable_and_metastable = remove_non_positive_monotonic(
        df_baryons_stable_and_metastable, x="mu" + X, y="pressure"
    )
    df_quarks_stable_and_metastable = remove_non_positive_monotonic(
        df_quarks_stable_and_metastable, x="mu" + X, y="pressure"
    )
    # df_vacuum = remove_non_positive_monotonic(df_vacuum)

    df_stable_and_metastable = pd.concat(
        [df_baryons_stable_and_metastable, df_quarks_stable_and_metastable, df_vacuum]
    )

    # test vaccum to baryon transition
    test1, value1, __ = compare_pressure_difference(
        df_baryons_stable_and_metastable,
        df_vacuum,
        key_column="mu" + X,
        comparison_column="pressure",
    )
    if test1:
        # if there is crossing, keep vaccum until value1, and keep df_baryons after value1
        # keep vaccum until value1
        df_vacuum = df_vacuum[df_vacuum["mu" + X] < value1[0]]
        # keep df_baryons after value1
        df_baryons_stable_and_metastable = df_baryons_stable_and_metastable[
            df_baryons_stable_and_metastable["mu" + X] >= value1[0]
        ]

    # test vaccum to quark transition
    test2, value2, __ = compare_pressure_difference(
        df_quarks_stable_and_metastable,
        df_vacuum,
        key_column="mu" + X,
        comparison_column="pressure",
    )
    if test2:
        # if there is crossing, keep vaccum until value2, and keep df_quarks after value2
        # keep vaccum until value2
        df_vacuum = df_vacuum[df_vacuum["mu" + X] < value2[0]]

        # keep df_quarks after value2
        df_quarks_stable_and_metastable = df_quarks_stable_and_metastable[
            df_quarks_stable_and_metastable["mu" + X] >= value2[0]
        ]

    # test baryon to quark transition
    test3, value3, pressure_diff3 = compare_pressure_difference(
        df_baryons_stable_and_metastable,
        df_quarks_stable_and_metastable,
        key_column="mu" + X,
        comparison_column="pressure",
    )
    if test3:
        # if there is crossing, keep df_baryons until value3, and keep df_quarks after value3
        # keep df_barons until value3
        df_baryons_stable_and_metastable = df_baryons_stable_and_metastable[
            df_baryons_stable_and_metastable["mu" + X] < value3[0]
        ]

        # keep df_quarks after value3
        df_quarks_stable_and_metastable = df_quarks_stable_and_metastable[
            df_quarks_stable_and_metastable["mu" + X] >= value3[0]
        ]
    else:
        # if there is no crossing, then check if all pressure diff are positive (all baryon pressures above quark pressures)
        # if all pressure diff are positive then empy df_quarks_stable_and_metastable

        if len(pressure_diff3) != 0 and all(pressure_diff3 > 0):
            df_quarks_stable_and_metastable = pd.DataFrame(
                columns=df_quarks_stable_and_metastable.columns
            )

    # separate stable from metastable
    df_stable, df_metastable = separate_stable_from_metastable(
        df_baryons_stable_and_metastable,
        df_quarks_stable_and_metastable,
        df_vacuum,
        X=X,
    )

    # remove non-monotonic points x='muB', y='pressure'
    (
        df_baryons_stable,
        df_quarks_stable,
        df_vacuum_stable,
    ) = split_df_into_baryons_quarks_vacuum(df_stable)

    df_temp = pd.concat([df_baryons_stable, df_quarks_stable])

    df_temp = remove_non_positive_monotonic(df_temp, x="mu" + X, y="pressure")

    df_stable = pd.concat([df_temp, df_vacuum_stable])

    df_stable = filter_vacuum_to_matter_transitions(df_stable)

    # if chi2_X is negative, then the point is unstable so remove it
    (
        df_baryons_stable,
        df_quarks_stable,
        df_vacuum_stable,
    ) = split_df_into_baryons_quarks_vacuum(df_stable)

    df_temp = pd.concat([df_baryons_stable, df_quarks_stable])

    df_temp = remove_non_positive_chi2(df_temp, X=X)

    df_stable = pd.concat([df_temp, df_vacuum_stable])

    df_stable = df_stable[df_stable_and_metastable.columns]

    # if a row from df_stable is not in df_stable_and_metastable, then it is df_metastable
    df_metastable = subset_difference(df_stable_and_metastable, df_stable)

    df_stable = df_stable[df.columns]
    df_metastable = df_metastable[df.columns]

    # if a row from df is not in df_stable or df_metastable, then it is df_unstable
    # bar = subset_difference(df, df_stable) = df - df_stable = df_unstable + df_metastable
    # subset_difference(bar, df_metastable) = df - df_stable - df_metastable = df_unstable + df_metastable - df_metastable = df_unstable
    df_unstable = subset_difference(subset_difference(df, df_stable), df_metastable)

    # remove extra columns from df_stable, df_metastable, df_unstable
    df_stable = df_stable[df.columns]
    df_metastable = df_metastable[df.columns]
    df_unstable = df_unstable[df.columns]

    return df_stable, df_metastable, df_unstable


def save_all_dataframes(
    df_stable, df_metastable, df_unstable, output_directory, output_prefix
):

    # split stable into baryons, quarks and vacuum
    (
        df_baryons_stable,
        df_quarks_stable,
        df_vacuum_stable,
    ) = split_df_into_baryons_quarks_vacuum(df_stable)
    (
        df_baryons_metastable,
        df_quarks_metastable,
        df_vacuum_metastable,
    ) = split_df_into_baryons_quarks_vacuum(df_metastable)
    # df_baryons_unstable, df_quarks_unstable, df_vacuum_unstable = split_df_into_baryons_quarks_vacuum(df_unstable)

    # save the DataFrames
    filename_stable = os.path.join(output_directory, f"{output_prefix}_stable.csv")
    save_DataFrame(df_stable, filename_stable)

    filename_metastable = os.path.join(
        output_directory, f"{output_prefix}_metastable.csv"
    )
    save_DataFrame(df_metastable, filename_metastable)

    filename_unstable = os.path.join(output_directory, f"{output_prefix}_unstable.csv")
    save_DataFrame(df_unstable, filename_unstable)

    df_quarks = (
        pd.concat([df_quarks_stable, df_quarks_metastable], ignore_index=True)
        if not df_quarks_stable.empty or not df_quarks_metastable.empty
        else pd.DataFrame(columns=df_stable.columns)
    )
    df_baryons = (
        pd.concat([df_baryons_stable, df_baryons_metastable], ignore_index=True)
        if not df_baryons_stable.empty or not df_baryons_metastable.empty
        else pd.DataFrame(columns=df_stable.columns)
    )
    df_vacuum = (
        pd.concat([df_vacuum_stable, df_vacuum_metastable], ignore_index=True)
        if not df_vacuum_stable.empty or not df_vacuum_metastable.empty
        else pd.DataFrame(columns=df_stable.columns)
    )

    save_DataFrame(
        df_quarks, os.path.join(output_directory, f"{output_prefix}_quarks.csv")
    )
    save_DataFrame(
        df_baryons, os.path.join(output_directory, f"{output_prefix}_baryons.csv")
    )
    save_DataFrame(
        df_vacuum, os.path.join(output_directory, f"{output_prefix}_vacuum.csv")
    )


def determine_varying_dimensions(df):
    unique_muB = df["muB"].nunique()
    unique_muS = df["muS"].nunique()
    unique_muQ = df["muQ"].nunique()

    varying_dims = {"B": unique_muB > 1, "S": unique_muS > 1, "Q": unique_muQ > 1}

    varying_columns = [dim for dim, varies in varying_dims.items() if varies]

    if len(varying_columns) == 0:
        logging.error("No B, S, or Q dimensions vary in the data.")
        sys.exit(1)
    else:
        return varying_columns


def process_intermediate_file(
    output_directory: str,
    input_file: str,
    variables: list,
    output_prefix: str = "CMF_output",
) -> None:
    """
    Process the main CMF++ output file and save cleaned data into separate CSV files.

    Args:
        output_directory (str): The directory to save the cleaned data files.
        input_file (str): The input file name to process.
        variables (list): List of variables for the DataFrame.
        output_prefix (str): Prefix for the output file names.
    """

    if not os.path.exists(os.path.join(output_directory, input_file)):
        logging.error(f"File {os.path.join(output_directory, input_file)} not found.")

        # list contents of output directory
        logging.info(f"Contents of {output_directory}:")
        for file in os.listdir(output_directory):
            logging.info(file)
        sys.exit(1)

    filename_in = os.path.join(output_directory, input_file)

    df = read_file(filename_in, variables)

    dimensions = determine_varying_dimensions(df)

    logging.info(f"Dimensions: {dimensions}")

    if "B" in dimensions:
        logging.info(f"Number of unique muB: {df['muB'].nunique()}")

    if "S" in dimensions:
        logging.info(f"Number of unique muS: {df['muS'].nunique()}")

    if "Q" in dimensions:
        logging.info(f"Number of unique muQ: {df['muQ'].nunique()}")

    df_original_columns = df.columns

    # Initialize empty DataFrames to store combined results
    combined_stable_muB = pd.DataFrame(columns=df_original_columns)
    combined_metastable_muB = pd.DataFrame(columns=df_original_columns)
    combined_unstable_muB = pd.DataFrame(columns=df_original_columns)

    # 1D filtering

    logging.info("Filtering stable points in 1D...")

    if "B" in dimensions:

        logging.info("Processing along muB...")

        combined_stable = list()
        combined_metastable = list()
        combined_unstable = list()

        for group, df_group in df.groupby(["muS", "muQ"]):

            df_stable, df_metastable, df_unstable = clean_df_along_muX_dimension(
                df_group, X="B"
            )

            # Add 'muS' and 'muQ' back to the DataFrames
            df_stable["muS"] = group[0]
            df_stable["muQ"] = group[1]
            df_metastable["muS"] = group[0]
            df_metastable["muQ"] = group[1]
            df_unstable["muS"] = group[0]
            df_unstable["muQ"] = group[1]

            # Concatenate the results of each group into the combined DataFrames
            combined_stable.append(df_stable)
            combined_metastable.append(df_metastable)
            combined_unstable.append(df_unstable)

        if len(combined_stable) > 0:
            combined_stable_muB = pd.concat(combined_stable)

        if len(combined_metastable) > 0:
            combined_metastable_muB = pd.concat(combined_metastable)

        if len(combined_unstable) > 0:
            combined_unstable_muB = pd.concat(combined_unstable)

    df = combined_stable_muB if combined_stable_muB is not df.empty else df

    # Initialize empty DataFrames to store combined results
    combined_stable_muS = pd.DataFrame(columns=df.columns)
    combined_metastable_muS = pd.DataFrame(columns=df.columns)
    combined_unstable_muS = pd.DataFrame(columns=df.columns)

    if "S" in dimensions:

        logging.info("Processing along muS...")

        combined_stable = list()
        combined_metastable = list()
        combined_unstable = list()

        for group, df_group in df.groupby(["muB", "muQ"]):

            df_stable, df_metastable, df_unstable = clean_df_along_muX_dimension(
                df_group, X="S"
            )

            # Add 'muS' and 'muQ' back to the DataFrames
            df_stable["muB"] = group[0]
            df_stable["muQ"] = group[1]
            df_metastable["muB"] = group[0]
            df_metastable["muQ"] = group[1]
            df_unstable["muB"] = group[0]
            df_unstable["muQ"] = group[1]

            # Concatenate the results of each group into the combined DataFrames
            combined_stable.append(df_stable)
            combined_metastable.append(df_metastable)
            combined_unstable.append(df_unstable)

        if len(combined_stable) > 0:
            combined_stable_muS = pd.concat(combined_stable)

        if len(combined_metastable) > 0:
            combined_metastable_muS = pd.concat(combined_metastable)

        if len(combined_unstable) > 0:
            combined_unstable_muS = pd.concat(combined_unstable)

    df = combined_stable_muS if combined_stable_muS is not df.empty else df

    # Initialize empty DataFrames to store combined results
    combined_stable_muQ = pd.DataFrame(columns=df.columns)
    combined_metastable_muQ = pd.DataFrame(columns=df.columns)
    combined_unstable_muQ = pd.DataFrame(columns=df.columns)

    if "Q" in dimensions:

        logging.info("Processing along muQ...")

        combined_stable = list()
        combined_metastable = list()
        combined_unstable = list()

        for group, df_group in df.groupby(["muB", "muS"]):

            df_stable, df_metastable, df_unstable = clean_df_along_muX_dimension(
                df_group, X="Q"
            )

            # Add 'muS' and 'muQ' back to the DataFrames
            df_stable["muB"] = group[0]
            df_stable["muS"] = group[1]
            df_metastable["muB"] = group[0]
            df_metastable["muS"] = group[1]
            df_unstable["muB"] = group[0]
            df_unstable["muS"] = group[1]

            # Concatenate the results of each group into the combined DataFrames
            combined_stable.append(df_stable)
            combined_metastable.append(df_metastable)
            combined_unstable.append(df_unstable)

        if len(combined_stable) > 0:
            combined_stable_muQ = pd.concat(combined_stable)

        if len(combined_metastable) > 0:
            combined_metastable_muQ = pd.concat(combined_metastable)

        if len(combined_unstable) > 0:
            combined_unstable_muQ = pd.concat(combined_unstable)

    if (
        combined_stable_muB.empty
        and combined_stable_muS.empty
        and combined_stable_muQ.empty
    ):
        logging.error("No stable data found.")
        sys.exit(1)

    combined_stable_temp = [
        df
        for df in [combined_stable_muB, combined_stable_muS, combined_stable_muQ]
        if not df.empty
    ]
    combined_metastable_temp = [
        df
        for df in [
            combined_metastable_muB,
            combined_metastable_muS,
            combined_metastable_muQ,
        ]
        if not df.empty
    ]
    combined_unstable_temp = [
        df
        for df in [combined_unstable_muB, combined_unstable_muS, combined_unstable_muQ]
        if not df.empty
    ]

    if len(combined_stable_temp) > 0:
        combined_stable = pd.concat(combined_stable_temp)
    else:
        logging.error("No stable data found.")
        sys.exit(1)

    if len(combined_metastable_temp) > 0:
        combined_metastable = pd.concat(combined_metastable_temp)
    else:
        combined_metastable = pd.DataFrame(columns=df.columns)

    if len(combined_unstable_temp) > 0:
        combined_unstable = pd.concat(combined_unstable_temp)
    else:
        combined_unstable = pd.DataFrame(columns=df.columns)

    # Remove duplicates
    combined_stable = combined_stable.drop_duplicates()
    combined_metastable = combined_metastable.drop_duplicates()
    combined_unstable = combined_unstable.drop_duplicates()

    # separate stable by vacuum, baryons, and quarks
    (
        combined_stable_baryons,
        combined_stable_quarks,
        combined_stable_vacuum,
    ) = split_df_into_baryons_quarks_vacuum(combined_stable)

    # 2D filter

    if len(dimensions) >= 2:

        logging.info("Filtering stable points in 2D...")

        if "B" in dimensions:
            logging.info("computing chi2_B")
            # Compute chi2_B
            combined_stable_baryons = compute_all_chi2(combined_stable_baryons, "B")
            combined_stable_quarks = compute_all_chi2(combined_stable_quarks, "B")

        if "S" in dimensions:
            logging.info("computing chi2_S")
            # Compute chi2_S
            combined_stable_baryons = compute_all_chi2(combined_stable_baryons, "S")
            combined_stable_quarks = compute_all_chi2(combined_stable_quarks, "S")

        if "Q" in dimensions:
            logging.info("computing chi2_Q")
            # Compute chi2_Q
            combined_stable_baryons = compute_all_chi2(combined_stable_baryons, "Q")
            combined_stable_quarks = compute_all_chi2(combined_stable_quarks, "Q")

        # if chi2_B is not present in the columns then add it
        if "chi2_B" not in combined_stable_baryons.columns:
            combined_stable_baryons["chi2_B"] = 0

        if "chi2_B" not in combined_stable_quarks.columns:
            combined_stable_quarks["chi2_B"] = 0

        # if chi2_S is not present in the columns then add it
        if "chi2_S" not in combined_stable_baryons.columns:
            combined_stable_baryons["chi2_S"] = 0

        if "chi2_S" not in combined_stable_quarks.columns:
            combined_stable_quarks["chi2_S"] = 0

        # if chi2_Q is not present in the columns then add it
        if "chi2_Q" not in combined_stable_baryons.columns:
            combined_stable_baryons["chi2_Q"] = 0

        if "chi2_Q" not in combined_stable_quarks.columns:
            combined_stable_quarks["chi2_Q"] = 0

        # check stability conditions
        if ("B" in dimensions) and ("S" in dimensions):

            logging.info("computing chi11_BS")

            # compute chi11_BS
            combined_stable_baryons = compute_all_chi11(
                combined_stable_baryons, "B", "S"
            )
            combined_stable_quarks = compute_all_chi11(combined_stable_quarks, "B", "S")

            # if chi11_BS is not present in the columns then add it
            if "chi11_BS" not in combined_stable_baryons.columns:
                combined_stable_baryons["chi11_BS"] = 0

            if "chi11_BS" not in combined_stable_quarks.columns:
                combined_stable_quarks["chi11_BS"] = 0

            # allow only chi2_B * chi2 >= chi11_BS^2 for stable
            combined_stable_baryons = combined_stable_baryons[
                combined_stable_baryons["chi2_B"] * combined_stable_baryons["chi2_S"]
                >= combined_stable_baryons["chi11_BS"] ** 2 - 1e-12
            ]

            combined_stable_quarks = combined_stable_quarks[
                combined_stable_quarks["chi2_B"] * combined_stable_quarks["chi2_S"]
                >= combined_stable_quarks["chi11_BS"] ** 2 - 1e-12
            ]

            # if chi2_B * chi2 < chi11_BS^2, then the point is unstable so add it to the unstable dataframe
            combined_unstable = pd.concat(
                [
                    combined_unstable,
                    combined_stable_baryons[
                        combined_stable_baryons["chi2_B"]
                        * combined_stable_baryons["chi2_S"]
                        < combined_stable_baryons["chi11_BS"] ** 2 - 1e-12
                    ],
                    combined_stable_quarks[
                        combined_stable_quarks["chi2_B"]
                        * combined_stable_quarks["chi2_S"]
                        < combined_stable_quarks["chi11_BS"] ** 2 - 1e-12
                    ],
                ]
            )

        if ("S" in dimensions) and ("Q" in dimensions):

            logging.info("computing chi11_SQ")

            # compute chi11_SQ
            combined_stable_baryons = compute_all_chi11(
                combined_stable_baryons, "S", "Q"
            )
            combined_stable_quarks = compute_all_chi11(combined_stable_quarks, "S", "Q")

            # if chi11_SQ is not present in the columns then add it
            if "chi11_SQ" not in combined_stable_baryons.columns:
                combined_stable_baryons["chi11_SQ"] = 0

            if "chi11_SQ" not in combined_stable_quarks.columns:
                combined_stable_quarks["chi11_SQ"] = 0

            # allow only chi2_S * chi2_Q >= chi11_SQ^2 for stable
            combined_stable_baryons = combined_stable_baryons[
                combined_stable_baryons["chi2_S"] * combined_stable_baryons["chi2_Q"]
                >= combined_stable_baryons["chi11_SQ"] ** 2 - 1e-12
            ]

            combined_stable_quarks = combined_stable_quarks[
                combined_stable_quarks["chi2_S"] * combined_stable_quarks["chi2_Q"]
                >= combined_stable_quarks["chi11_SQ"] ** 2 - 1e-12
            ]

            # if chi2_S * chi2_Q < chi11_SQ^2, then the point is unstable so add it to the unstable dataframe
            combined_unstable = pd.concat(
                [
                    combined_unstable,
                    combined_stable_baryons[
                        combined_stable_baryons["chi2_S"]
                        * combined_stable_baryons["chi2_Q"]
                        < combined_stable_baryons["chi11_SQ"] ** 2 - 1e-12
                    ],
                    combined_stable_quarks[
                        combined_stable_quarks["chi2_S"]
                        * combined_stable_quarks["chi2_Q"]
                        < combined_stable_quarks["chi11_SQ"] ** 2 - 1e-12
                    ],
                ]
            )

        if "Q" in dimensions and "B" in dimensions:

            logging.info("computing chi11_QB")

            # compute chi11_QB
            combined_stable_baryons = compute_all_chi11(
                combined_stable_baryons, "Q", "B"
            )
            combined_stable_quarks = compute_all_chi11(combined_stable_quarks, "Q", "B")

            # if chi11_QB is not present in the columns then add it
            if "chi11_QB" not in combined_stable_baryons.columns:
                combined_stable_baryons["chi11_QB"] = 0

            if "chi11_QB" not in combined_stable_quarks.columns:
                combined_stable_quarks["chi11_QB"] = 0

            # allow only chi2_Q * chi2_B >= chi11_QB^2 for stable
            combined_stable_baryons = combined_stable_baryons[
                combined_stable_baryons["chi2_Q"] * combined_stable_baryons["chi2_B"]
                >= combined_stable_baryons["chi11_QB"] ** 2 - 1e-12
            ]

            combined_stable_quarks = combined_stable_quarks[
                combined_stable_quarks["chi2_Q"] * combined_stable_quarks["chi2_B"]
                >= combined_stable_quarks["chi11_QB"] ** 2 - 1e-12
            ]

            # if chi2_Q * chi2_B < chi11_QB^2, then the point is unstable so add it to the unstable dataframe
            combined_unstable = pd.concat(
                [
                    combined_unstable,
                    combined_stable_baryons[
                        combined_stable_baryons["chi2_Q"]
                        * combined_stable_baryons["chi2_B"]
                        < combined_stable_baryons["chi11_QB"] ** 2 - 1e-12
                    ],
                    combined_stable_quarks[
                        combined_stable_quarks["chi2_Q"]
                        * combined_stable_quarks["chi2_B"]
                        < combined_stable_quarks["chi11_QB"] ** 2 - 1e-12
                    ],
                ]
            )

    # 3D filter

    if len(dimensions) == 3:

        logging.info("Filtering stable points in 3D...")

        # allow only chi_2B * chi_2S * chi_2Q + 2 * chi11_BS * chi11_SQ * chi11_QB >= chi_2B * chi11_SQ^2 + chi_2S * chi11_QB^2 + chi_2Q * chi11_BS^2  for stable
        combined_stable_baryons = combined_stable_baryons[
            combined_stable_baryons["chi2_B"]
            * combined_stable_baryons["chi2_S"]
            * combined_stable_baryons["chi2_Q"]
            + 2
            * combined_stable_baryons["chi11_BS"]
            * combined_stable_baryons["chi11_SQ"]
            * combined_stable_baryons["chi11_QB"]
            >= combined_stable_baryons["chi2_B"]
            * combined_stable_baryons["chi11_SQ"] ** 2
            + combined_stable_baryons["chi2_S"]
            * combined_stable_baryons["chi11_QB"] ** 2
            + combined_stable_baryons["chi2_Q"]
            * combined_stable_baryons["chi11_BS"] ** 2
            - 1e-12
        ]

        combined_stable_quarks = combined_stable_quarks[
            combined_stable_quarks["chi2_B"]
            * combined_stable_quarks["chi2_S"]
            * combined_stable_quarks["chi2_Q"]
            + 2
            * combined_stable_quarks["chi11_BS"]
            * combined_stable_quarks["chi11_SQ"]
            * combined_stable_quarks["chi11_QB"]
            >= combined_stable_quarks["chi2_B"]
            * combined_stable_quarks["chi11_SQ"] ** 2
            + combined_stable_quarks["chi2_S"] * combined_stable_quarks["chi11_QB"] ** 2
            + combined_stable_quarks["chi2_Q"] * combined_stable_quarks["chi11_BS"] ** 2
            - 1e-12
        ]

        # if chi_2B * chi_2S * chi_2Q + 2 * chi11_BS * chi11_SQ * chi11_QB < chi_2B * chi11_SQ^2 + chi_2S * chi11_QB^2 + chi_2Q * chi11_BS^2, then the point is unstable so add it to the unstable dataframe
        combined_unstable = pd.concat(
            [
                combined_unstable,
                combined_stable_baryons[
                    combined_stable_baryons["chi2_B"]
                    * combined_stable_baryons["chi2_S"]
                    * combined_stable_baryons["chi2_Q"]
                    + 2
                    * combined_stable_baryons["chi11_BS"]
                    * combined_stable_baryons["chi11_SQ"]
                    * combined_stable_baryons["chi11_QB"]
                    < combined_stable_baryons["chi2_B"]
                    * combined_stable_baryons["chi11_SQ"] ** 2
                    + combined_stable_baryons["chi2_S"]
                    * combined_stable_baryons["chi11_QB"] ** 2
                    + combined_stable_baryons["chi2_Q"]
                    * combined_stable_baryons["chi11_BS"] ** 2
                    - 1e-12
                ],
                combined_stable_quarks[
                    combined_stable_quarks["chi2_B"]
                    * combined_stable_quarks["chi2_S"]
                    * combined_stable_quarks["chi2_Q"]
                    + 2
                    * combined_stable_quarks["chi11_BS"]
                    * combined_stable_quarks["chi11_SQ"]
                    * combined_stable_quarks["chi11_QB"]
                    < combined_stable_quarks["chi2_B"]
                    * combined_stable_quarks["chi11_SQ"] ** 2
                    + combined_stable_quarks["chi2_S"]
                    * combined_stable_quarks["chi11_QB"] ** 2
                    + combined_stable_quarks["chi2_Q"]
                    * combined_stable_quarks["chi11_BS"] ** 2
                    - 1e-12
                ],
            ]
        )

    if len(dimensions) >= 2:
        # conatenate combined_stable_baryons and combined_stable_quarks with combined_stable_vacuum
        combined_stable = pd.concat(
            [
                combined_stable_baryons,
                combined_stable_quarks,
                combined_stable_vacuum,
            ]
        )

    # reinstate to df column order
    combined_stable = combined_stable[df_original_columns]
    combined_metastable = combined_metastable[df_original_columns]
    combined_unstable = combined_unstable[df_original_columns]

    # Save the combined DataFrames
    save_all_dataframes(
        combined_stable,
        combined_metastable,
        combined_unstable,
        output_directory,
        output_prefix,
    )


def main() -> None:

    parser = argparse.ArgumentParser(description="Clean CMF++ output files")
    parser.add_argument(
        "--validated_config_file_path",
        type=str,
        default=DEFAULT_VALIDATED_CONFIG_FILE_PATH,
        help="Path to the validated configuration file (optional)",
    )
    parser.add_argument(
        "--root_output_path",
        type=str,
        default=DEFAULT_ROOT_OUTPUT_PATH,
        help="Path to the root output folder where the CMF_output files are stored",
    )

    args = parser.parse_args()

    # check existence of config file
    if not os.path.exists(args.validated_config_file_path):
        logging.error(f"File {args.validated_config_file_path} not found.")
        sys.exit(1)

    yaml_input = load_configuration(args.validated_config_file_path)
    logging.info(f"Loaded configuration from {args.validated_config_file_path}")

    # check existence of root output directory
    if not os.path.exists(args.root_output_path):
        logging.error(f"Directory {args.root_output_path} not found")
        sys.exit(1)

    output_directory = os.path.join(args.root_output_path, yaml_input["run_name"])
    logging.info(f"Running for folder: {output_directory}")

    # check existence of output directory
    if not os.path.exists(output_directory):
        logging.error(f"Directory {output_directory} not found")
        sys.exit(1)

    process_intermediate_file(
        output_directory,
        "CMF_intermediate_output.csv",
        CMF_VARIABLES,
        output_prefix="CMF_output",
    )

    if yaml_input["output_particle_properties"]:

        process_intermediate_file(
            output_directory,
            "CMF_intermediate_output_particle_properties.csv",
            PARTICLE_PROPERTIES_VARIABLES,
            output_prefix="CMF_output_particle_properties",
        )


if __name__ == "__main__":
    logging.info(f"Starting execution of {__file__}...")
    main()
    logging.info(f"Execution end of {__file__}.")
