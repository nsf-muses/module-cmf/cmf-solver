#include "particle.hpp"

/**
 * @file particle.cpp
 * @brief (implementation) Particle class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

void particle::compute_chemical_potential(double muB, double muS, double muQ) {
    chemical_potential = muB * BaryonNumber + muS * Strangeness + muQ * ElectricCharge;
}

void particle::compute_vacuum_mass() {
    vacuum_mass = bare_mass + g_sigma * input_params.sigma_mean_field_vacuum_mass +
                  g_zeta * input_params.zeta_mean_field_vacuum_mass;
    if (input_params.use_Phi_order) {
        vacuum_mass += g_Phi_order;
    }
}

void particle::compute_potential(double omega, double phi, double rho) {
    optical_potential = g_omega * omega + g_phi * phi + g_rho * rho;
}

void particle::compute_effective_mass(double sigma, double zeta, double delta) {
    effective_mass = bare_mass + g_sigma * sigma + g_zeta * zeta + g_delta * delta;
}

void particle::compute_effective_chemical_potential(double omega, double phi, double rho) {
    effective_chemical_potential = chemical_potential - g_omega * omega - g_rho * rho - g_phi * phi;
}

void particle::compute_Fermi_momentum() {
    double Fermi_momentum_squared;

    Fermi_momentum_squared =
        effective_chemical_potential * effective_chemical_potential - effective_mass * effective_mass;
    if (Fermi_momentum_squared <= 0 || effective_chemical_potential <= 0) {
        Fermi_momentum = 0.;
    } else {
        Fermi_momentum = sqrt(Fermi_momentum_squared);
    }
}

void particle::compute_energy() {
    if (Fermi_momentum * Fermi_momentum <= 0 || effective_chemical_potential <= 0) {
        energy = effective_mass;
    } else {
        energy = sqrt(Fermi_momentum * Fermi_momentum + effective_mass * effective_mass);
    }
}

void particle::compute_vector_density() {
    vector_density = Degeneracy * Fermi_momentum * Fermi_momentum * Fermi_momentum / 6. / M_PI / M_PI;
}

void particle::compute_fermionic_energy() {
    if (Fermi_momentum * Fermi_momentum <= 0 || effective_chemical_potential <= 0) {
        fermionic_energy = 0;
    } else {
        if (fabs(effective_mass) < 1E-6) {
            fermionic_energy = Degeneracy * Fermi_momentum * Fermi_momentum * Fermi_momentum * Fermi_momentum / 4.;
        } else {
            fermionic_energy =
                Degeneracy * (.25 * Fermi_momentum * energy * energy * energy -
                              (effective_mass / 8.) * (effective_mass * Fermi_momentum * energy +
                                                       effective_mass * effective_mass * effective_mass *
                                                           log(fabs((Fermi_momentum + energy) / effective_mass))));
        }
    }
}

void particle::compute_fermionic_pressure() {
    if (Fermi_momentum * Fermi_momentum <= 0 || effective_chemical_potential <= 0) {
        fermionic_pressure = 0;
    } else {
        if (fabs(effective_mass) < 1E-6) {
            fermionic_pressure =
                Degeneracy * Fermi_momentum * Fermi_momentum * Fermi_momentum * Fermi_momentum / 4. / 3.;
        } else {
            fermionic_pressure =
                Degeneracy * (Fermi_momentum * energy * energy * energy / 12. -
                              (effective_mass * 5. / 24.) * (effective_mass * Fermi_momentum * energy) +
                              (effective_mass / 8.) * effective_mass * effective_mass * effective_mass *
                                  log(fabs((Fermi_momentum + energy) / effective_mass)));
        }
    }
}

void particle::compute_scalar_density() {
    if (Fermi_momentum * Fermi_momentum <= 0 || effective_chemical_potential <= 0) {
        scalar_density = 0;

    } else {
        if (fabs(effective_mass) < 1E-6) {
            scalar_density = Degeneracy * effective_mass * Fermi_momentum * Fermi_momentum / 4.;

        } else {
            scalar_density = Degeneracy * effective_mass *
                             (Fermi_momentum * energy -
                              effective_mass * effective_mass * log(fabs((Fermi_momentum + energy) / effective_mass))) /
                             2.;
        }

        scalar_density /= 2. * M_PI * M_PI;
    }
}

quark::quark() {}

void quark::compute_effective_mass(double sigma, double zeta, double delta, double Phi_order) {
    particle::compute_effective_mass(sigma, zeta, delta);

    if (input_params.use_Phi_order) {
        effective_mass += g_Phi_order * (1. - Phi_order);
    }
}

baryon::baryon() {}

void baryon::compute_effective_mass(double sigma, double zeta, double delta, double Phi_order) {
    particle::compute_effective_mass(sigma, zeta, delta);

    if (input_params.use_Phi_order) {
        effective_mass += g_Phi_order * Phi_order * Phi_order;
    }
}