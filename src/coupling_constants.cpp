#include "coupling_constants.hpp"

/**
 * @file coupling_constants.cpp
 * @brief (implementation) coupling constants class
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

coupling_constants::coupling_constants() {}

void coupling_constants::create_couplings_and_bare_masses() {
    cout << "\nCreating coupling constants \n";

    double m_1 = input_params.m_1;
    double m_2 = input_params.m_2;
    double xiesb3 = 0.;
    double mresb = 0.;

    // order: g_sigma;g_zeta;g_delta;g_omega;g_phi;g_rho;g_Phi_order ;
    const couplings empty = {0, 0, 0, 0, 0, 0, 0};

    if (input_params.use_ideal_gas) {
        // quarks
        couplings_map[1] = empty;
        couplings_map[2] = empty;
        couplings_map[3] = empty;

        // Baryon octet
        couplings_map[2212] = empty;
        couplings_map[2112] = empty;
        couplings_map[3122] = empty;
        couplings_map[3222] = empty;
        couplings_map[3212] = empty;
        couplings_map[3112] = empty;
        couplings_map[3322] = empty;
        couplings_map[3312] = empty;

        // Baryon decuplet
        couplings_map[2224] = empty;
        couplings_map[2214] = empty;
        couplings_map[2114] = empty;
        couplings_map[1114] = empty;
        couplings_map[3224] = empty;
        couplings_map[3214] = empty;
        couplings_map[3114] = empty;
        couplings_map[3314] = empty;
        couplings_map[3324] = empty;
        couplings_map[3334] = empty;
    } else {
        double gBpol = 0.;
        double gQpol = 0.;

        double alpha_X = input_params.alpha_X;
        double g_X1 = 0.;
        double g_X8 = 0.;
        double g_DX = 0.;
        double alpha_DX = 0.;

        // precompute constant quantities
        const double hw =
            sqrt(2.) * input_params.zeta_mean_field_vacuum_mass - input_params.sigma_mean_field_vacuum_mass;

        xiesb3 = 1.;

        if (input_params.use_Phi_order) {
            gBpol = input_params.gB_Phi_order;
            gQpol = input_params.gQ_Phi_order;
        }

        if (input_params.baryon_mass_coupling == 1) {
            g_X1 =
                sqrt(6.) / 2. *
                (input_params.Lambda_vacuum_mass + input_params.Sigma_vacuum_mass - 2 * input_params.mass0) /
                (2. * input_params.sigma_mean_field_vacuum_mass + sqrt(2.) * input_params.zeta_mean_field_vacuum_mass);

            alpha_X = (-input_params.Sigma_vacuum_mass / 2. - 3. / 2. * input_params.Lambda_vacuum_mass +
                       2. * input_params.nucleon_vacuum_mass) /
                      (input_params.Sigma_vacuum_mass - 3. * input_params.Lambda_vacuum_mass +
                       2. * input_params.nucleon_vacuum_mass);

            g_X8 = 3. *
                   ((input_params.Lambda_vacuum_mass + input_params.Sigma_vacuum_mass) / 2. -
                    input_params.nucleon_vacuum_mass) /
                   (4. * alpha_X - 1.) / hw;

            alpha_DX =
                (-input_params.Omega_vacuum_mass * input_params.sigma_mean_field_vacuum_mass +
                 input_params.Delta_vacuum_mass * sqrt(2.) * input_params.zeta_mean_field_vacuum_mass) /
                (-input_params.sigma_mean_field_vacuum_mass + sqrt(2.) * input_params.zeta_mean_field_vacuum_mass) /
                input_params.Sigma_star_vacuum_mass;
            g_DX = input_params.Delta_vacuum_mass / ((3. - alpha_DX) * input_params.sigma_mean_field_vacuum_mass +
                                                     alpha_DX * sqrt(2.) * input_params.zeta_mean_field_vacuum_mass);

        } else if (input_params.baryon_mass_coupling == 2) {
            const double gns =
                -(input_params.nucleon_vacuum_mass - input_params.mass0) / input_params.sigma_mean_field_vacuum_mass;

            g_X1 = -sqrt(2. / 3.) * gns;

            const double m_8 = g_X1 *
                                   (2. * input_params.sigma_mean_field_vacuum_mass +
                                    sqrt(2.) * input_params.zeta_mean_field_vacuum_mass) /
                                   sqrt(6.) +
                               input_params.mass0;

            g_X8 = -gns / (4. * alpha_X - 1.);

            g_DX = input_params.Delta_vacuum_mass / (3. * input_params.sigma_mean_field_vacuum_mass);

            m_1 = input_params.Sigma_vacuum_mass - m_8 - 2. * g_X8 / 3. * (alpha_X - 1.) * hw;
            m_2 = ((input_params.Lambda_vacuum_mass - m_8 + 2. * g_X8 / 3. * (alpha_X - 1.) * hw) * 3. - m_1) / 2.;

            mresb = -104.45;

        } else if (input_params.baryon_mass_coupling == 3) {
            const double gns = input_params.gN_sigma;

            const double mn = gns * input_params.sigma_mean_field_vacuum_mass +
                              input_params.gN_zeta * input_params.zeta_mean_field_vacuum_mass;
            const double m_8 =
                g_X1 *
                (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass + input_params.zeta_mean_field_vacuum_mass) /
                sqrt(3.);

            alpha_X = (1. - 2 * (m_8 - mn) / (m_8 - input_params.Lambda_vacuum_mass)) /
                      (4. - 2. * (m_8 - mn) / (m_8 - input_params.Lambda_vacuum_mass));
            g_X1 = (sqrt(2.) * gns + input_params.gN_zeta) / sqrt(3.);
            g_X8 = 1.5 * (m_8 - input_params.Lambda_vacuum_mass) / (hw * (alpha_X - 1.));

            alpha_DX =
                (-input_params.Omega_vacuum_mass * input_params.sigma_mean_field_vacuum_mass +
                 input_params.Delta_vacuum_mass * sqrt(2.) * input_params.zeta_mean_field_vacuum_mass) /
                (-input_params.sigma_mean_field_vacuum_mass + sqrt(2.) * input_params.zeta_mean_field_vacuum_mass) /
                input_params.Sigma_star_vacuum_mass;
            g_DX = input_params.Delta_vacuum_mass / ((3. - alpha_DX) * input_params.sigma_mean_field_vacuum_mass +
                                                     alpha_DX * sqrt(2.) * input_params.zeta_mean_field_vacuum_mass);
        }

        const double g8 = input_params.gN_omega / 3.;
        const double gsir = 2. * g8;
        const double gxir = g8;
        const double gdr = 3. * g8 * input_params.V_Delta;
        const double gssr = 2. * g8 * input_params.V_Delta;
        const double gxsr = g8 * input_params.V_Delta;

        const double glphi = -sqrt(2.) / 3. * input_params.gN_omega;
        const double gsiphi = -sqrt(2.) / 3. * input_params.gN_omega;
        const double gxiphi = -2. * sqrt(2.) / 3. * input_params.gN_omega;
        const double gssphi = -sqrt(2.) / 3. * input_params.V_Delta * input_params.gN_omega;
        const double gxsphi = -2. * sqrt(2.) / 3. * input_params.V_Delta * input_params.gN_omega;
        const double gophi = -3. * sqrt(2.) / 3. * input_params.V_Delta * input_params.gN_omega;

        const double glo = 2. * g8;
        const double gsio = 2. * g8;
        const double gxio = g8;
        const double gdo = 3. * g8 * input_params.V_Delta;
        const double gsso = 2. * g8 * input_params.V_Delta;
        const double gxso = g8 * input_params.V_Delta;
        const double goo = 0;

        const double gsp = g_X1 * sqrt(2. / 3.) + g_X8 / 3. * (4. * alpha_X - 1.);
        const double gzp = g_X1 * sqrt(1. / 3.) - g_X8 / 3. * (4. * alpha_X - 1.) * sqrt(2.);
        const double gsl = g_X1 * sqrt(2. / 3.) + 2. * g_X8 / 3. * (alpha_X - 1.) + sqrt(2.) * input_params.m_3H;
        const double gss = g_X1 * sqrt(2. / 3.) - 2. * g_X8 / 3. * (alpha_X - 1.) + sqrt(2.) * input_params.m_3H;
        const double gsxi =
            g_X1 * sqrt(2. / 3.) - g_X8 / 3. * (2. * alpha_X + 1.) + sqrt(2.) * input_params.m_3H * xiesb3;

        const double gzl = g_X1 * sqrt(1. / 3.) - 2. * g_X8 / 3. * (alpha_X - 1.) * sqrt(2.) + input_params.m_3H;
        const double gzs = g_X1 * sqrt(1. / 3.) + 2. * g_X8 / 3. * (alpha_X - 1.) * sqrt(2.) + input_params.m_3H;

        const double gzxi =
            g_X1 * sqrt(1. / 3.) + g_X8 / 3. * (2. * alpha_X + 1.) * sqrt(2.) + input_params.m_3H * xiesb3;
        const double gdxi = g_X8 * (2. * alpha_X - 1.);

        const double gds = 2. * g_X8 * alpha_X;

        const double gsd = g_DX * (3. - alpha_DX);
        const double gss2 = g_DX * 2. + input_params.m_3D * sqrt(2.);
        const double gsk = g_DX * (1. + alpha_DX) + xiesb3 * input_params.m_3D * sqrt(2.);
        const double gso = g_DX * alpha_DX * 2. + xiesb3 * 1.5 * input_params.m_3D * sqrt(2.);

        const double gzd = g_DX * alpha_DX * sqrt(2.);
        const double gzs2 = g_DX * sqrt(2.) + input_params.m_3D;
        const double gzk = g_DX * (2. - alpha_DX) * sqrt(2.) + xiesb3 * input_params.m_3D;
        const double gzo = g_DX * (3. - 2. * alpha_DX) * sqrt(2.) + xiesb3 * 1.5 * input_params.m_3D;

        // create map of couplings

        // Quarks
        /*
        1	Quark up	1
        2	Quark down	2
        3	Quark strange	3
        */

        couplings_map[1] = {input_params.gqu_sigma,
                            input_params.gqu_zeta,
                            input_params.gqu_delta,
                            input_params.gqu_omega,
                            input_params.gqu_phi,
                            input_params.gqu_rho,
                            gQpol};
        couplings_map[2] = {input_params.gqd_sigma,
                            input_params.gqd_zeta,
                            input_params.gqd_delta,
                            input_params.gqd_omega,
                            input_params.gqd_phi,
                            input_params.gqd_rho,
                            gQpol};
        couplings_map[3] = {input_params.gqs_sigma,
                            input_params.gqs_zeta,
                            input_params.gqs_delta,
                            input_params.gqs_omega,
                            input_params.gqs_phi,
                            input_params.gqs_rho,
                            gQpol};

        // Baryon octet
        /*
        2212	p	7
        2112	n	8
        3122	Lambda	9
        3222	Sigma+	10
        3212	Sigma0	11
        3112	Sigma-	12
        3322	Ksi0	13
        3312	Ksi-	14
        */

        couplings_map[2212] = {gsp, gzp, g_X8, input_params.gN_omega, input_params.gN_phi, input_params.gN_rho, gBpol};
        couplings_map[2112] = {gsp,  gzp, -g_X8, input_params.gN_omega, input_params.gN_phi, -input_params.gN_rho,
                               gBpol};
        couplings_map[3122] = {gsl, gzl, 0, glo, glphi, 0, gBpol};
        couplings_map[3222] = {gss, gzs, gds, gsio, gsiphi, gsir, gBpol};
        couplings_map[3212] = {gss, gzs, 0, gsio, gsiphi, 0, gBpol};
        couplings_map[3112] = {gss, gzs, -gds, gsio, gsiphi, -gsir, gBpol};
        couplings_map[3322] = {gsxi, gzxi, gdxi, gxio, gxiphi, gxir, gBpol};
        couplings_map[3312] = {gsxi, gzxi, -gdxi, gxio, gxiphi, -gxir, gBpol};

        // Baryon decuplet
        /*
        2224	Delta(1232)++	15
        2214	Delta(1232)+	16
        2114	Delta(1232)0	17
        1114	Delta(1232)-	18
        3224	Sigma(1385)+	19
        3214	Sigma(1385)0	20
        3114	Sigma(1385)-	21
        3324	Ksi(1530)0	22
        3314	Ksi(1530)-	23
        3334	Omega	24
        */

        couplings_map[2224] = {gsd, gzd, 0, gdo, 0, gdr, gBpol};
        couplings_map[2214] = {gsd, gzd, 0, gdo, 0, gdr / 3., gBpol};
        couplings_map[2114] = {gsd, gzd, 0, gdo, 0, -gdr / 3., gBpol};
        couplings_map[1114] = {gsd, gzd, 0, gdo, 0, -gdr, gBpol};
        couplings_map[3224] = {gss2, gzs2, 0, gsso, gssphi, gssr, gBpol};
        couplings_map[3214] = {gss2, gzs2, 0, gsso, gssphi, 0, gBpol};
        couplings_map[3114] = {gss2, gzs2, 0, gsso, gssphi, -gssr, gBpol};
        couplings_map[3324] = {gsk, gzk, 0, gxso, gxsphi, gxsr, gBpol};
        couplings_map[3314] = {gsk, gzk, 0, gxso, gxsphi, -gxsr, gBpol};
        couplings_map[3334] = {gso, gzo, 0, goo, gophi, 0, gBpol};
    }

    cout << "\nCreating bare masses \n";

    // quarks
    bare_masses_map[1] = input_params.up_quark_bare_mass;
    bare_masses_map[2] = input_params.down_quark_bare_mass;
    bare_masses_map[3] = input_params.strange_quark_bare_mass;

    // Baryon octet
    bare_masses_map[2212] = input_params.mass0;
    bare_masses_map[2112] = input_params.mass0;
    bare_masses_map[3122] = -input_params.m_3H * (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                                  input_params.zeta_mean_field_vacuum_mass) +
                            (m_1 + 2. * m_2) / 3. + input_params.mass0;
    bare_masses_map[3222] = -input_params.m_3H * (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                                  input_params.zeta_mean_field_vacuum_mass) +
                            m_1 + input_params.mass0;
    bare_masses_map[3212] = -input_params.m_3H * (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                                  input_params.zeta_mean_field_vacuum_mass) +
                            m_1 + input_params.mass0;
    bare_masses_map[3112] = -input_params.m_3H * (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                                  input_params.zeta_mean_field_vacuum_mass) +
                            m_1 + input_params.mass0;
    bare_masses_map[3322] =
        -input_params.m_3H *
            (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass + input_params.zeta_mean_field_vacuum_mass) * xiesb3 +
        m_1 + m_2 + input_params.mass0;
    bare_masses_map[3312] =
        -input_params.m_3H *
            (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass + input_params.zeta_mean_field_vacuum_mass) * xiesb3 +
        m_1 + m_2 + input_params.mass0;

    // Baryon decuplet
    bare_masses_map[2224] = 0.;
    bare_masses_map[2214] = 0.;
    bare_masses_map[2114] = 0.;
    bare_masses_map[1114] = 0.;
    bare_masses_map[3224] = mresb - input_params.m_3D * (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                                         input_params.zeta_mean_field_vacuum_mass);
    bare_masses_map[3214] = mresb - input_params.m_3D * (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                                         input_params.zeta_mean_field_vacuum_mass);
    bare_masses_map[3114] = mresb - input_params.m_3D * (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                                         input_params.zeta_mean_field_vacuum_mass);
    bare_masses_map[3314] = 2. * mresb - xiesb3 * input_params.m_3D *
                                             (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                              input_params.zeta_mean_field_vacuum_mass);
    bare_masses_map[3324] = 2. * mresb - xiesb3 * input_params.m_3D *
                                             (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                              input_params.zeta_mean_field_vacuum_mass);
    bare_masses_map[3334] = 3. * mresb - xiesb3 * 1.5 * input_params.m_3D *
                                             (sqrt(2.) * input_params.sigma_mean_field_vacuum_mass +
                                              input_params.zeta_mean_field_vacuum_mass);
}