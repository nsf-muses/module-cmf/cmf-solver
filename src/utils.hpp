#ifndef UTILS_HPP
#define UTILS_HPP

/**
 * @file utils.hpp
 * @brief (header) utility functions for the Chiral Mean Field model.
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

#pragma once

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "yaml-cpp/yaml.h"

using std::begin;
using std::cerr;
using std::cout;
using std::end;
using std::endl;
using std::exception;
using std::fabs;
using std::find;
using std::ifstream;
using std::isfinite;
using std::ofstream;
using std::ostream;
using std::pow;
using std::remove;
using std::round;
using std::runtime_error;
using std::scientific;
using std::sqrt;
using std::string;
using std::stringstream;
using std::to_string;
using std::unordered_map;
using std::vector;
using std::move;

bool file_exist(const string &fname);
void create_status_file(const int status_value, const string &file_path = "../output/status.yaml",
                        const string &message = "none");
bool copy_file(const string &source_path, const string &destination_path);
void open_file(const string &filePath, FILE *&outFile);
void print_progress_bar(int counter, int Initial_points);

/**
 * @brief check if a and b are equal within 1e-14 difference.
 *
 * @param[in] a first value
 * @param[in] b second value
 * @return true if a and b are closer than 1e-14, false if they are not.
 */
template <typename T>
T AreEqual(T a, T b) {
    const double epsilon = 1e-14;
    return fabs(a - b) < epsilon;
}

/**
 * @brief check if the value e is inside class object c
 *
 * @param[in] c class object
 * @param[in] e value to check
 * @return true if e is inside c, false if e is not inside c.
 */
template <class C, typename T>
bool contains(C &&c, T e) {
    return find(begin(c), end(c), e) != end(c);
}

#endif