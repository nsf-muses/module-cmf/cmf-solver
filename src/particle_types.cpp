#include "particle_types.hpp"

/**
 * @file particle_types.cpp
 * @brief (implementation) Particle types classes for the Chiral Mean Field model.
 *
 * @author Nikolas Cruz-Camacho <cnc6@illinois.edu>
 */

quark_sector::quark_sector() {}

void quark_sector::initialize(PDG_reader infile, coupling_constants& Coupling_Constants) {
    // loop over PDG particles
    for (size_t i = 0; i < static_cast<size_t>(infile.read_particles.size()); i++) {
        quark temp;

        // move data from PDG particles[i] to temp
        temp.ID = infile.read_particles[i].ID;
        temp.Name = infile.read_particles[i].Name;
        temp.Mass = infile.read_particles[i].Mass;
        temp.Degeneracy = infile.read_particles[i].Degeneracy;
        temp.BaryonNumber = infile.read_particles[i].BaryonNumber;
        temp.Strangeness = infile.read_particles[i].Strangeness;
        temp.Isospin_Z = infile.read_particles[i].Isospin_Z;
        temp.ElectricCharge = infile.read_particles[i].ElectricCharge;

        // add coupling constants information
        temp.g_sigma = Coupling_Constants.couplings_map.at(temp.ID).g_sigma;
        temp.g_zeta = Coupling_Constants.couplings_map.at(temp.ID).g_zeta;
        temp.g_delta = Coupling_Constants.couplings_map.at(temp.ID).g_delta;
        temp.g_omega = Coupling_Constants.couplings_map.at(temp.ID).g_omega;
        temp.g_phi = Coupling_Constants.couplings_map.at(temp.ID).g_phi;
        temp.g_rho = Coupling_Constants.couplings_map.at(temp.ID).g_rho;
        temp.g_Phi_order = Coupling_Constants.couplings_map.at(temp.ID).g_Phi_order;

        temp.bare_mass = Coupling_Constants.bare_masses_map.at(temp.ID);

        particles.push_back(temp);
    }
}

void quark_sector::compute_total_charge() {
    total_charge = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_charge += particles[i].ElectricCharge * particles[i].vector_density;
    }
}

void quark_sector::compute_total_strangeness() {
    total_strangeness = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_strangeness += particles[i].Strangeness * particles[i].vector_density;
    }
}

void quark_sector::compute_total_vector_density() {
    total_vector_density = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_vector_density += particles[i].BaryonNumber * particles[i].vector_density;
    }
}

void quark_sector::compute_source_terms(double sigma, double zeta, double delta, double omega, double phi, double rho,
                                        double Phi_order) {
    sigma_source_term = 0;
    zeta_source_term = 0;
    delta_source_term = 0;
    omega_source_term = 0;
    phi_source_term = 0;
    rho_source_term = 0;
    Phi_order_source_term = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        particles[i].compute_effective_mass(sigma, zeta, delta, Phi_order);
        particles[i].compute_effective_chemical_potential(omega, phi, rho);
        particles[i].compute_Fermi_momentum();
        particles[i].compute_energy();

        particles[i].compute_scalar_density();
        particles[i].compute_vector_density();

        omega_source_term += particles[i].g_omega * particles[i].vector_density * particles[i].BaryonNumber;
        phi_source_term += particles[i].g_phi * particles[i].vector_density * particles[i].BaryonNumber;
        rho_source_term += particles[i].g_rho * particles[i].vector_density * particles[i].BaryonNumber;

        sigma_source_term += particles[i].scalar_density * particles[i].g_sigma;
        zeta_source_term += particles[i].scalar_density * particles[i].g_zeta;
        delta_source_term += particles[i].scalar_density * particles[i].g_delta;
        Phi_order_source_term += particles[i].scalar_density * particles[i].g_Phi_order;
    }
}

void quark_sector::compute_total_fermionic_energy() {
    total_fermionic_energy = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_fermionic_energy += particles[i].fermionic_energy;
    }

    total_fermionic_energy = total_fermionic_energy / 2. / M_PI / M_PI;
}

void quark_sector::compute_total_fermionic_pressure() {
    total_fermionic_pressure = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_fermionic_pressure += particles[i].fermionic_pressure;
    }

    total_fermionic_pressure = total_fermionic_pressure / 2. / M_PI / M_PI;
}

void quark_sector::print() {
    cout << "\n quark_sector \n";
    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        cout << "\n basic info \n";
        particles[i].print_basic_info();

        cout << "\n coupling info \n";
        particles[i].print_couplings_info();

        cout << "\n masses info \n";
        particles[i].print_masses_info();
        cout << endl;

        cout << "\n chemical potential info \n";
        particles[i].print_chemical_potential_info();
        cout << endl;

        cout << "\n densities info \n";
        particles[i].print_densities_info();
        cout << endl;
    }
    cout << "\n hz \n";
    cout << "omega_source_term \t" << omega_source_term << endl;
    cout << "phi_source_term  \t" << phi_source_term << endl;
    cout << "rho_source_term  \t" << rho_source_term << endl;

    cout << "\n ib \n";
    cout << "sigma_source_term \t" << sigma_source_term << endl;
    cout << "zeta_source_term  \t" << zeta_source_term << endl;
    cout << "delta_source_term  \t" << delta_source_term << endl;
    cout << "Phi_order_source_term  \t" << Phi_order_source_term << endl;
}

void quark_sector::reset() {
    sigma_source_term = 0;
    zeta_source_term = 0;
    delta_source_term = 0;
    omega_source_term = 0;
    phi_source_term = 0;
    rho_source_term = 0;
    Phi_order_source_term = 0;
    total_fermionic_pressure = 0;
    total_fermionic_energy = 0;
    total_vector_density = 0;
    total_charge = 0;
    total_strangeness = 0;
}

baryon_sector::baryon_sector() {}

void baryon_sector::initialize(PDG_reader infile, coupling_constants& Coupling_Constants) {
    // loop over PDG particles
    for (size_t i = 0; i < static_cast<size_t>(infile.read_particles.size()); i++) {
        baryon temp;

        // move data from PDG particles[i] to temp
        temp.ID = infile.read_particles[i].ID;
        temp.Name = infile.read_particles[i].Name;
        temp.Mass = infile.read_particles[i].Mass;
        temp.Degeneracy = infile.read_particles[i].Degeneracy;
        temp.BaryonNumber = infile.read_particles[i].BaryonNumber;
        temp.Strangeness = infile.read_particles[i].Strangeness;
        temp.Isospin_Z = infile.read_particles[i].Isospin_Z;
        temp.ElectricCharge = infile.read_particles[i].ElectricCharge;

        // add coupling constants information
        temp.g_sigma = Coupling_Constants.couplings_map.at(temp.ID).g_sigma;
        temp.g_zeta = Coupling_Constants.couplings_map.at(temp.ID).g_zeta;
        temp.g_delta = Coupling_Constants.couplings_map.at(temp.ID).g_delta;
        temp.g_omega = Coupling_Constants.couplings_map.at(temp.ID).g_omega;
        temp.g_phi = Coupling_Constants.couplings_map.at(temp.ID).g_phi;
        temp.g_rho = Coupling_Constants.couplings_map.at(temp.ID).g_rho;
        temp.g_Phi_order = Coupling_Constants.couplings_map.at(temp.ID).g_Phi_order;

        temp.bare_mass = Coupling_Constants.bare_masses_map.at(temp.ID);

        particles.push_back(temp);
    }
}

void baryon_sector::compute_total_vector_density() {
    total_vector_density = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_vector_density += particles[i].BaryonNumber * particles[i].vector_density;
    }
}

void baryon_sector::compute_total_strangeness() {
    total_strangeness = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_strangeness += particles[i].Strangeness * particles[i].vector_density;
    }
}

void baryon_sector::compute_total_charge() {
    total_charge = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_charge += particles[i].ElectricCharge * particles[i].vector_density;
    }
}

void baryon_sector::compute_source_terms(double sigma, double zeta, double delta, double omega, double phi, double rho,
                                         double Phi_order) {
    sigma_source_term = 0;
    zeta_source_term = 0;
    delta_source_term = 0;
    omega_source_term = 0;
    phi_source_term = 0;
    rho_source_term = 0;
    Phi_order_source_term = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        particles[i].compute_effective_mass(sigma, zeta, delta, Phi_order);
        particles[i].compute_effective_chemical_potential(omega, phi, rho);
        particles[i].compute_Fermi_momentum();
        particles[i].compute_energy();

        particles[i].compute_scalar_density();
        particles[i].compute_vector_density();

        omega_source_term += particles[i].g_omega * particles[i].vector_density * particles[i].BaryonNumber;
        phi_source_term += particles[i].g_phi * particles[i].vector_density * particles[i].BaryonNumber;
        rho_source_term += particles[i].g_rho * particles[i].vector_density * particles[i].BaryonNumber;

        sigma_source_term += particles[i].scalar_density * particles[i].g_sigma;
        zeta_source_term += particles[i].scalar_density * particles[i].g_zeta;
        delta_source_term += particles[i].scalar_density * particles[i].g_delta;
        Phi_order_source_term += -6. * particles[i].scalar_density * particles[i].g_Phi_order * Phi_order;
    }
}

void baryon_sector::compute_total_fermionic_energy() {
    total_fermionic_energy = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_fermionic_energy += particles[i].fermionic_energy;
    }

    total_fermionic_energy = total_fermionic_energy / 2. / M_PI / M_PI;
}

void baryon_sector::compute_total_fermionic_pressure() {
    total_fermionic_pressure = 0;

    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        total_fermionic_pressure += particles[i].fermionic_pressure;
    }

    total_fermionic_pressure = total_fermionic_pressure / 2. / M_PI / M_PI;
}

void baryon_sector::print() {
    cout << "\n Baryon Octet \n";
    for (size_t i = 0; i < static_cast<size_t>(particles.size()); i++) {
        cout << "\n basic info \n";
        particles[i].print_basic_info();

        cout << "\n coupling info \n";
        particles[i].print_couplings_info();

        cout << "\n masses info \n";
        particles[i].print_masses_info();
        cout << endl;

        cout << "\n chemical potential info \n";
        particles[i].print_chemical_potential_info();
        cout << endl;

        cout << "\n densities info \n";
        particles[i].print_densities_info();
        cout << endl;
    }
    cout << "\n hz \n";
    cout << "omega_source_term \t" << omega_source_term << endl;
    cout << "phi_source_term  \t" << phi_source_term << endl;
    cout << "rho_source_term  \t" << rho_source_term << endl;

    cout << "\n ib \n";
    cout << "sigma_source_term \t" << sigma_source_term << endl;
    cout << "zeta_source_term  \t" << zeta_source_term << endl;
    cout << "delta_source_term  \t" << delta_source_term << endl;
    cout << "Phi_order_source_term  \t" << Phi_order_source_term << endl;
}

void baryon_sector::reset() {
    sigma_source_term = 0;
    zeta_source_term = 0;
    delta_source_term = 0;
    omega_source_term = 0;
    phi_source_term = 0;
    rho_source_term = 0;
    Phi_order_source_term = 0;
    total_fermionic_pressure = 0;
    total_fermionic_energy = 0;
    total_vector_density = 0;
    total_charge = 0;
    total_strangeness = 0;
}