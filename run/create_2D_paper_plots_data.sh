#!/bin/bash

# Function to run the Python command and make
run_CMF++() {
    cd ../src/
    python create_config.py --production_run=False --use_decuplet=True --use_hyperons=True --output_flavor_equilibration=False --output_particle_properties=True --output_Lepton=False --output_debug=True --vector_potential=$1 --muB_step=$2 --muS_begin=$3 --muS_end=$4 --muS_step=$5 --muQ_begin=$6 --muQ_end=$7 --muQ_step=$8 --run_name=$9 --Phi_order0_step=0.333
    time make run
}

# Run the commands with different parameters

# 2D
# vector_potential muB_step muS_begin muS_end muS_step muQ_begin muQ_end muQ_step run_name

# C1
run_CMF++ 1 1 0 999 10000 0 999 10000 "muS_0_muQ_0"
run_CMF++ 1 1 0 999 10000 -50 999 10000 "muS_0_muQ_-50"
run_CMF++ 1 1 -50 999 10000 0 999 10000 "muS_-50_muQ_0"
run_CMF++ 1 1 -50 999 10000 -50 999 10000 "muS_-50_muQ_-50"

# C2
run_CMF++ 2 1 0 999 10000 0 999 10000 "muS_0_muQ_0"
run_CMF++ 2 1 0 999 10000 -50 999 10000 "muS_0_muQ_-50"
run_CMF++ 2 1 -50 999 10000 0 999 10000 "muS_-50_muQ_0"
run_CMF++ 2 1 -50 999 10000 -50 999 10000 "muS_-50_muQ_-50"

# C3
run_CMF++ 3 1 0 999 10000 0 999 10000 "muS_0_muQ_0"
run_CMF++ 3 1 0 999 10000 -50 999 10000 "muS_0_muQ_-50"
run_CMF++ 3 1 -50 999 10000 0 999 10000 "muS_-50_muQ_0"
run_CMF++ 3 1 -50 999 10000 -50 999 10000 "muS_-50_muQ_-50"

# C4
run_CMF++ 4 1 0 999 10000 0 999 10000 "muS_0_muQ_0"
run_CMF++ 4 1 0 999 10000 -50 999 10000 "muS_0_muQ_-50"
run_CMF++ 4 1 -50 999 10000 0 999 10000 "muS_-50_muQ_0"
run_CMF++ 4 1 -50 999 10000 -50 999 10000 "muS_-50_muQ_-50"
