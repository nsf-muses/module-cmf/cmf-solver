#!/bin/bash

# Define parameter combinations
vector_potentials=(1 2 3 4)
use_decuplet_options=("True" "False")
use_hyperons_options=("True")
use_quarks_options=("True" "False")

# Loop over parameter combinations
for vector_potential in "${vector_potentials[@]}"; do
  for use_decuplet in "${use_decuplet_options[@]}"; do
    for use_hyperons in "${use_hyperons_options[@]}"; do
      for use_quarks in "${use_quarks_options[@]}"; do
        # Construct run_name based on use_decuplet, use_hyperons, and use_quarks
        run_name="default"
        if [ "$use_decuplet" = "True" ]; then
          run_name="${run_name}_decuplet"
        fi
        if [ "$use_hyperons" = "False" ]; then
          run_name="${run_name}_nohyper"
        fi
        if [ "$use_quarks" = "False" ]; then
          run_name="${run_name}_noquarks"
        fi

        # Run the commands
        echo "Running CMF++ simulation with parameters:"
        echo "  Vector Potential: $vector_potential"
        echo "  Use Decuplet: $use_decuplet"
        echo "  Use Hyperons: $use_hyperons"
        echo "  Use Quarks: $use_quarks"
        echo "  Run Name: $run_name"

        cd ../src/

        python create_config.py \
          --run_name="$run_name" \
          --use_decuplet="$use_decuplet" \
          --use_quarks="$use_quarks" \
          --use_hyperons="$use_hyperons" \
          --vector_potential="$vector_potential" \
          --production_run=False \
          --output_flavor_equilibration=False \
          --output_Lepton=False \
          --output_debug=True \
          --output_particle_properties=True \
          --use_Phi_order=True \
          --muB_step=1

        time make run

        echo "CMF++ execution complete."
      
      done
    done
  done
done
