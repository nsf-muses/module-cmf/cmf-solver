FROM registry.gitlab.com/nsf-muses/common/muses-common:1.1.0 AS build
############################################################################################
## Copy source code
COPY src/ /tmp/src
WORKDIR /tmp/src

## Build CMF
RUN make clean && make

## Copy unit tests
COPY test/ /tmp/test
WORKDIR /tmp/test/unit_tests

## Build unit tests
RUN bash build_unit_tests.sh


FROM python:3.11-slim AS deps
#############################
# Install git for pip install
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
        git \
    && rm -rf /var/lib/apt/lists/*
## Install dependencies to /usr/local/lib/python3.11/site-packages/
COPY requirements.txt /opt/
RUN pip --no-cache-dir install -r /opt/requirements.txt --no-compile


FROM python:3.11-slim
#####################
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -yq \
        make \
    && rm -rf /var/lib/apt/lists/*

ARG USERNAME=cmf
ARG UID=1000

## Create user with desired UID and set "home" to /opt
RUN useradd --uid $UID --no-log-init --no-create-home --home-dir /opt --shell /bin/bash $USERNAME
RUN chown -R $UID:$UID /opt
USER $USERNAME


## Install compiled executable and add to PATH
ENV PATH="/opt/src:/opt/.local/bin:${PATH}"
## Install Python dependencies
COPY --from=deps --chown=$UID:$UID /usr/local/lib/python3.11/site-packages/ /opt/.local/lib/python3.11/site-packages/

## Install source code with compiled output
WORKDIR /opt
COPY --from=build --chown=$UID:$UID /tmp/src src
COPY --from=build --chown=$UID:$UID /tmp/test test
COPY --chown=$UID:$UID api api
COPY --chown=$UID:$UID input input

## remove .cpp and .hpp from container image
RUN rm -rf src/*.cpp src/*.hpp

## remove build from container image
RUN rm -rf src/build

## Copy manifest, Dockerfile, Readme.md, LICENSE, and *.sh files
COPY --chown=$UID:$UID manifest.yaml ./
COPY --chown=$UID:$UID Dockerfile ./
COPY --chown=$UID:$UID LICENSE ./
COPY --chown=$UID:$UID Readme.md ./

## create input and output directories
RUN mkdir -p /opt/input /opt/output

# change to source code directory
WORKDIR /opt/src
