#!/bin/bash

# This script builds and runs the CMF++ project using Docker
# author: Nikolas Cruz-Camacho <cnc6@illinois.edu> 

# Set options to exit the script on any error, treat unset variables as errors, and fail if any command in a pipeline fails.
set -euo pipefail

# Print a message indicating the start of the CMF++ docker run.
echo "CMF++ docker run started"

# Function to display help message
usage() {
  echo "Usage: $0 [options]"
  echo "Options:"
  echo "  -r ROOT_OUTPUT_PATH           Path to the root output (default: $DEFAULT_ROOT_OUTPUT_PATH)"
  echo "  -t DOCKER_IMAGE_TAG           Tag of the Docker image (default: $DEFAULT_DOCKER_IMAGE_TAG)"
  echo "  -h                            Display this help message"
  exit 1
}

# Default values
DEFAULT_ROOT_OUTPUT_PATH="./output/"
DEFAULT_IMAGE_NAME="cmf"
DEFAULT_DOCKER_IMAGE_TAG="local"

# Initialize variables with default values
ROOT_OUTPUT_PATH="$DEFAULT_ROOT_OUTPUT_PATH"
DOCKER_IMAGE_TAG="$DEFAULT_DOCKER_IMAGE_TAG"

# Parse options
while getopts ":r:t:h" opt; do
  case $opt in
    r) ROOT_OUTPUT_PATH="$OPTARG"
       ;;
    t) DOCKER_IMAGE_TAG="$OPTARG"
       ;;
    h) usage
       ;;
    \?) echo "Invalid option: -$OPTARG" >&2
        usage
        ;;
    :) echo "Option -$OPTARG requires an argument." >&2
       usage
       ;;
  esac
done

# Display parsed values (for debugging purposes)
echo "ROOT_OUTPUT_PATH: $ROOT_OUTPUT_PATH"
echo "DOCKER_IMAGE_TAG: $DOCKER_IMAGE_TAG"

# Convert the user ths to an absolute path
ROOT_OUTPUT_PATH=$(realpath "$ROOT_OUTPUT_PATH")

# check that the Docker image exists; exit with an error if not.
if ! docker image inspect $DEFAULT_IMAGE_NAME:$DOCKER_IMAGE_TAG &> /dev/null; then
    echo "Error: CMF docker image does not exist: $DEFAULT_IMAGE_NAME:$DOCKER_IMAGE_TAG"
    exit 1
fi

# Check if the output folder exists; if not create it
if [ ! -d "$ROOT_OUTPUT_PATH" ]; then
    echo "Warning: output directory does not exist: $ROOT_OUTPUT_PATH"
    echo "Warning: creating output directory: $ROOT_OUTPUT_PATH"
    mkdir -p "$ROOT_OUTPUT_PATH"
fi

# Run the CMF++ Docker container, mapping output directories, and executing 'make run'.
docker run -it --rm --name cmf \
  -v "$ROOT_OUTPUT_PATH:/opt/output" \
  $DEFAULT_IMAGE_NAME:$DOCKER_IMAGE_TAG make run_default

# Check the exit status of the last command (Docker container); print success or failure message accordingly.
if [ $? -eq 0 ]; then
  echo -e "\n\tCMF++ docker default run: OK\n"
else
  echo -e "\n\tCMF++ docker default run: Failed\n"
fi

# Print a message indicating the completion of the CMF++ run and exit with status 0.
echo "CMF++ run done"
exit 0
