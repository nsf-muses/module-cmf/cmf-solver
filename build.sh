#!/bin/bash

# This script builds the CMF++ project locally
# author: Nikolas Cruz-Camacho <cnc6@illinois.edu> 

# Set shell options for strict error handling
set -euo pipefail

# Display a message indicating the start of the CMF++ build
echo "CMF++ build started"

# Ensure the 'input' directory exists or create it
mkdir -p input

# Ensure the 'output' directory exists or create it
mkdir -p output

# Change the current working directory to 'src'
cd src

# Build CMF++ using the 'make' command
make clean && make

# Check the exit status of the 'make' command
if [ $? -eq 0 ]; then
  # If the build is successful, print a success message
  echo -e "\n\tCMF++ build: OK\n"
else
  # If the build fails, print an error message and exit with status 1
  echo -e "\n\tCMF++ build: Failed\n"
  exit 1
fi

# Display a message indicating the completion of the CMF++ build
echo "CMF++ build done"

# Exit the script with a status of 0 (success)
exit 0

